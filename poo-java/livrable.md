# Livrable
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Le point de départ
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* .java
* packages 

... et c'est tout

----

## Rappel : compilation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](img/compile.png)

----

## Compilation : démo
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Démo
```bash
javac Main.java
java Main Main.class
```

/!\ A reproduire chez vous /!\
<!-- .element: class="fragment" -->

----

## Le livrable : JAR
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Java ARchive
* Un zip avec des .class  
* JAR exécutable (contient le nom du main)

----

## Générer un JAR : démo
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Démo

/!\ A ne pas reproduire chez vous /!\
<!-- .element: class="fragment" -->

----

## Exécuter un JAR : démo
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```bash
java -jar jeanmichel.jar
java Main -jar jeanmichel.jar
```

----

## Conclusion : livrable Java
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Pour livrer du code exécutable : JAR

