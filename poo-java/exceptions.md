# Les exceptions
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Principe
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Des trucs peuvent mal se passer
* Hors cas nominal = Exception
* Typage fort = exceptions typées
* Les méthodes peuvent lancer des exceptions
* On gère ou on propage

----

## Un exemple : la classe [Files](https://docs.oracle.com/javase/8/docs/api/java/nio/file/Files.html)
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
public static List<String> readAllLines(Path path) throws IOException

```Java
public void lireFichier() {
    List<String> lignes = Files.readAllLines(Paths.get("/home/user/toto.txt"));
}
```
Et si ça se passe mal ?

----

## Option 1 : catch
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public void lireFichier() {
    try {
        List<String> lignes = Files.readAllLines(Paths.get("/home/user/toto.txt"));
    }
    catch (IOException e) {
        e.printStackTrace();
    }
}
```
* Catch = on gère l'exception

----

## Option 2 : on propage
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public void lireFichier() throws IOException {
    List<String> lignes = Files.readAllLines(Paths.get("/home/user/toto.txt"));
}
```
* throws = attention, je peux lancer cette exception  
* différence throws / throw

----

## L'analogie du chantier
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## RuntimeException
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Quid des NullPointerException ?
* Runtime = exécution
* "Unchecked"
* Pas de throws
* Try / catch possible

![](img/exceptions.png)

----

## Error
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Game over
* "Unchecked"
* Pas de try / catch
* Ex : java.lang.OutOfMemoryError

----

## Mes propres exceptions
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class PokemonFatigueException extends Exception {

} 
```

```Java
public class PointsDeVieInvalides extends RuntimeException {
    
} 
```

----

## Mes propres exceptions : throw
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Pokemon {

    public void attaquer(Pokemon cible) throws PokemonFatigueException {
        if (this.fatigue > MAX_FATIGUE) {
            throw new PokemonFatigueException();
        }

        cible.perdrePV(this.force);
    }
}
```

----

## Gestion des exceptions
```Java
public void attaquerToutLeMonde(List<Pokemon> cibles) {
    for (Pokemon cible : cibles) {
        try {
            attaquer(cible);
        }
        catch (PokemonFatigueException e) {
            System.out.println("Le pokémon est fatigué, on arrête les attaques");
            return;
        }
    }
}
```
ou
```Java
public void attaquerToutLeMonde(List<Pokemon> cibles) throws PokemonFatigueException {
    for (Pokemon cible : cibles) {
        attaquer(cible);
    }
}
```