# Héritage
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* L’héritage est un mécanisme qui permet, lors de la déclaration d’une nouvelle classe, d'y inclure les caractéristiques d’une autre classe.
<!-- .element: class="fragment" --> 

[https://fr.wikipedia.org/wiki/Héritage_(informatique)](https://fr.wikipedia.org/wiki/H%C3%A9ritage_(informatique%29)
<!-- .element: class="fragment" --> 

----

## Héritage
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
<img src="img/heritage.png" width="300">
* Relation de <b>généralisation</b>-<b>spécialisation</b>  
* Réutilisation / mutualisation  
* Classe mère / fille / parent / superclasse  
* Pas d'héritage multiple

----

## Héritage Java
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Animal {
    private String nom;

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return this.nom;
    }
}
```

----

## Héritage Java : extends
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Chat extends Animal {

    public void miauler() {
        System.out.println("Miaouh");
    }
}
```

```Java
public class Chien extends Animal {

    public void aboyer() {
        System.out.println("Ouaf");
    }
}
```

----

## Héritage Java : instanciation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
Animal animal = new Animal();
animal.setNom("Toto");

Chat chat = new Chat();
chat.setNom("Kroquette");
chat.miauler();

Chien chien = new Chien();
chien.setNom("Médor");
chien.aboyer();
```

----

## Héritage Java : mutualisation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Animal, chien et chat sont des animaux.  
```Java
public class Collier {

    public static String lireNom(Animal animal) {
        System.out.println(animal.getNom());
    }
}
```

```Java
Animal animal = new Animal();
animal.setNom("Toto");

Chat chat = new Chat();
chat.setNom("Kroquette");

Chien chien = new Chien();
chien.setNom("Médor");

Collier.lireNom(animal);
Collier.lireNom(chat);
Collier.lireNom(chien);
```

----

## Héritage Java : super
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
super = superclass
```Java
public class Animal {
    public void crier() {
        System.out.println("Hiiii");
    }
}
```
```Java
public class Chien extends Animal {

    public void crier() {
        super.crier();
        System.out.println("Ouaf");
    }

    public void faireLaTeuf() {
        for (i = 0; i < 10; i++) {
            super.crier();
        }
    }
}
```

----

## Héritage Java : constructeur
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
super = superclass
```Java
public class Animal {
    private String nom;

    public Animal(String nom) {
        this.nom = nom;
    }
}
```
```Java
public class Chien extends Animal {

    public Chien() {
        super("Nom par défaut");
    }

    public Chien(String nom) {
        super(nom);
    }
}
```

----

## Héritage Java : la classe Object
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
<img src="img/hierarchie.png" width="300">  
* Classe mère (superclasse) de toutes les classes  
* Tout* est un Object  
* [https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html)

