# Précédemment dans Java
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Tout n'est que classe
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Créer des classes
```Java
public class Etudiant {
    public String nom = "Léponge";
    public String prenom = "Bob";

    public Etudiant(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }
}
```  

----

## Typage fort
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
Etudiant etudiant = new Etudiant("Toto","titi");
int nombre = 2;
String phrase = "Hello world";
``` 

```Java
public class Pokemon {
    public int pv;
    public int force;

    public void attaquer(Pokemon adversaire) {
        adversaire.pv = adversaire.pv - this.force;
    }
}
``` 
<!-- .element: class="fragment" -->

----

## 3 principes
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Encapsulation
* Héritage
* Polymorphisme

----

## Interface
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Signature vs corps  

```Java
public interface Sauvegardable {
    public void sauvegarder();
}
```  


```Java
public class Pokemon implements Sauvegardable  {
    public void sauvegarder() {
        getConnection().prepareStatement("INSERT INTO pokemon(id,name) VALUES(1,'toto')").executeUpdate();
    }
}
```  

----

## Conteneurs
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
List<String> mots = new ArrayList<String>();
mots.add("Hello");
mots.add("world");
mots.size();
```  
Array, Map, Set



