# Précédemment dans Java
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Tout n'est que classe
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Créer des classes
```Java
public class Etudiant {
    public String nom = "Léponge";
    public String prenom = "Bob";

    public Etudiant(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }
}
```  

----

## Typage fort
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
Etudiant etudiant = new Etudiant("Toto","titi");
int nombre = 2;
String phrase = "Hello world";
``` 

```Java
public class Pokemon {
    int pv;
    int force;

    public void attaquer(Pokemon adversaire) {
        adversaire.pv = adversaire.pv - this.force;
    }
}
``` 
<!-- .element: class="fragment" -->
