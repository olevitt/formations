# Maven
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Le sauveur

----

## Maven : le sauveur
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* "Software project management and comprehension tool"
* https://maven.apache.org/
* Installé avec Eclipse ou à part
* Paramétrage réseau [http://foad.ensai.fr/course/view.php?id=270](http://foad.ensai.fr/course/view.php?id=270)

----

## Pourquoi ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Gestion des dépendances
* Gestion du build
* Gestion de la configuration
* Conventions de projet

----

## Le deal
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Maven gère tout
* On laisse maven tout gérer
* Pas d'entourloupe
* "Convention over configuration"

----

## pom.xml : la vérité
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Fichier pom.xml à la racine
```XML
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>fr.insee.recensement</groupId>
  <artifactId>collecte-internet</artifactId>
  <version>1.0</version>
</project>
```

----

## Structure de projet
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```
my-app
|-- pom.xml
`-- src
    |-- main
    |   `-- java
    |       `-- com
    |           `-- mycompany
    |               `-- app
    |                   `-- App.java
    `-- test
        `-- java
            `-- com
                `-- mycompany
                    `-- app
                        `-- AppTest.java
```
https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html

----

## Conclusion
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
2019 : tout projet est mavenisé  
1. pom.xml à la racine  
2. on respecte les conventions  
3. ???
4. PROFIT!!!