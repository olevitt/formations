# TP2 : OOP like a boss (EN)
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Use of advanced OOP concepts
* New various features for the application 
* Creation and comparison of simple AIs

----

## Starting point
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* For this practical work, we will start from where we ended last week
* You can either use your code from last week or checkout the correction from this git repository :  
[https://bitbucket.org/olevitt/tp1/overview](https://bitbucket.org/olevitt/tp1/overview)  
* You can get the code using git clone or using bitbucket's downloads menu 
* Eclipse can import the project using "File => Import => General => Existing projects into Workspace" feature
* Take some time to make sure you understand the code and what it is doing. Feel obviously free to ask questions about it

----

## 1. Cleanup
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* During practical work 1, all attributes where defined as public. What issues do you think this choice may lead to ?
* Using as much IDE's power as you can, refactor the code to respect encapsulation
* Make sure to do it for all classes

----

## 2. Items
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* Pokemons will now be able to use various more items. All items have a name and a price (the price it costs at the pokeshop) and a usage type (either permanent, one use or unusable)  
* Sweets are items 
* Pokeballs are also items. A pokeball has a base efficiency (An integer between 0 and 100. <b>Base efficiency can never be over 100 or under 0</b>) and a efficiency multiplier which depends on it's type. 
* Simple pokeballs does not exist. A pokeball is always special. 2 types of pokeballs currently exist : superball (with a multiplier of 2) and hyperball (with a multiplier of 3)  
* For all pokeballs, we need to know the real efficiency which is : base efficiency * multiplier
* By default, all items have the following String representation : "name (price pokedollars)"

----

## 3. Inventory
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
The player now has access to an inventory    
* Create an empty Inventory class. Inventory will soon contain the list of all items the player currently have.
* Create a Player class with an Inventory attribute. In this game, there will always be only one Player. This player should be accessible from anywhere in the application. The player won't change during gameplay.  

----

## 4. Lot of stuff
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
To manage the Inventory, we need to handle lists
* Take a look at List interface : [https://docs.oracle.com/javase/8/docs/api/java/util/List.html](https://docs.oracle.com/javase/8/docs/api/java/util/List.html)  
* Take a look at ArrayList class (implementation) : [https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html)  
* Add a list of items to the Inventory class
* Within the main function, add some items to the player's inventory 
* Inside Inventory, write a totalPrice() method that calculates the total price of all items in the inventory
* Within the main function, once the Inventory has been filled, display it's total price
* Sidequest : override Inventory's [toString()](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html#toString--) to display the list of items
* Sidequest : notice there are other List implementations :[https://docs.oracle.com/javase/tutorial/collections/implementations/list.html](https://docs.oracle.com/javase/tutorial/collections/implementations/list.html)

----

## 5. More action
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Before writing AIs, we need to add more depth to fights
* Pokemons can now defend. Add a defense score (between 0 and 90). This will be the percentage of damage that will be mitigated on each attack  
* Write a defend() method that increases the defense score by 10 (up to a maximum of 90)
* The logic for damage dealt change slightly. It is now : attacker's strength * (1 - target's defense score / 100). Every attack deals at least 1 damage.  
* Simulate a fight in which one of the pokemon defends for 3 turns

----

## 6. AI may be the future
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Let's write some AI that will be able to decide the best move each time
* An AI should be able to act given the current situation 
* Define the signature of an AI method
* Create the corresponding AI interface
* Create 2 AI implementations : the "brute" one that constantly attacks and the vicious one that defends until it reachs 90 defense and then attacks  
* Make the 2 AI fight using the pokemons you want
* Create a third implementation using your own algorithm
* Sidequest : write an AI tournament. They compete against each other. Each won fight (using generated pokemon data) scores 1 point for the winning AI

----

## Going deeper
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" --> 
* Checkout the code from the main repository. Inside it you will find a correction for this practical work (TP2). Copy the "IAGeniale" class and make it fight your own AI. Improve your own AI until it beats "IAGeniale" (which, in reality, is not that geniale)
* Make your AI compete against other AI in the room
* (Going a whole lot deeper) : generate a large amount of data "state of pokemon1, state of pokemon2, action => fight's result". Using deep learning on this data, create an AI. Make it compete with other AIs