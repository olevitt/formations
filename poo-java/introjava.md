# Java ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## C'est quoi ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Un langage de programmation
* Java = Javascript ? [<b>N O N</b>](https://stackoverflow.com/questions/245062/whats-the-difference-between-javascript-and-java)  
* Java 1.0 : Janvier 1996
* Java 1.8 (8) : Mars 2014
* Java 12 : Mars 2019

----

## Java en 2019
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Au top des classements : http://pypl.github.io/PYPL.html, https://www.tiobe.com/tiobe-index/, http://githut.info/
* Partout : Desktop, Servers, mobile ...
* A l'INSEE : 92.31% des applications

----

## Le concept de JVM
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* "Write once, run everywhere"  
* JVM = Java Virtual Machine  
* JDK = Java Development Kit
* JRE = Java Runtime Environment

----

## Java / Python : le jeu des 7 différences
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

| Java        |     Python     |   
| :------------ | -------------: | 
| Pensé POO        |     POO optionnelle     |  
| Typage fort      |   Typage faible    |  
| Compilé         |     Exécuté      |   
| Application         |     Script     |  
| Accolades {}         |     Indentation     |  
| Verbeux         |     Concis     |  
| Packaging (JAR)         |     Fichiers .py     |  
