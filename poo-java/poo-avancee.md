# Concepts POO avancés
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## 3 principes
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Encapsulation
* Héritage
* Polymorphisme

