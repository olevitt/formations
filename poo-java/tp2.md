# TP2 : POO comme des pros (FR)
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Application des concepts avancés de POO
* Evolution de l'application  
* Création et comparaison d'intelligences artificielles simples

----

## Le point de départ
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Ce TP s'appuiera sur le code du TP1 
* Vous pouvez, au choix, repartir de votre propre code ou importer la correction disponible ici :  
[https://bitbucket.org/olevitt/tp1/overview](https://bitbucket.org/olevitt/tp1/overview)  
* Vous pouvez au choix, soit cloner le dépôt en utilisant git, soit télécharger le zip du dépôt  
* Le projet est importable via la fonctionnalité File => Import => General => Existing projects into Workspace  
* Prendre un peu de temps pour reparcourir le code

----

## 1. Nettoyage
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Au TP1, tous les attributs étaient publics. Quel(s) problème(s) potentiels cela peut il engendrer ?  
* En utilisant au maximum la puissance de l'IDE, refactorer le code pour respecter le principe d'encapsulation

----

## 2. Des objets
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* Les pokemons vont maintenant pouvoir utiliser différents objets. Tous les objets ont un nom et un prix (le prix de vente au pokeshop) et un type d'utilisation (permanent, utilisation unique, non utilisable)  
* Les bonbons sont des objets  
* Les pokeballs aussi. Une pokeball a un score d'efficacité de base (entier entre 0 et 100. <b>L'efficacité de base ne peut jamais être négative ou supérieure à 100</b>). 
* Pour les pokeballs, on n'a jamais de pokeballs simples. Une pokeball est forcément spéciale avec un multiplicateur d'efficacité. On a les formes suivantes : superball (multiplicateur de 2) et hyperball (multiplicateur de 3)  
* Pour toutes les pokeballs, on veut une méthode permettant de calculer l'efficacité réelle, c'est à dire le produit efficacité de base * multiplicateur
* Par défaut, la représentation sous forme de chaine de caractères d'un objet est : "nom (prix pokédollars)"

----

## 3. Un inventaire
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Le joueur a maintenant accès à un inventaire.    
* Créer une classe vide Inventaire. Inventaire contiendra bientôt la liste des objets possédés par le joueur.
* Créer une classe Joueur possédant un Inventaire. Notre jeu n'aura qu'un et un seul Joueur. Ce joueur doit pouvoir être accessible à tout endroit de notre code. Le joueur sera toujours le même pendant le jeu.  

----

## 4. Plein d'objets
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Pour les objets de notre Inventaire, il nous faut créer une liste
* Consulter la définition de l'interface List : [https://docs.oracle.com/javase/8/docs/api/java/util/List.html](https://docs.oracle.com/javase/8/docs/api/java/util/List.html)  
* Consulter la définition de la classe ArrayList : [https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html)  
* Ajouter à l'inventaire une liste d'objets  
* Dans le main, ajouter différents objets à l'inventaire  
* Ajouter à Inventaire une méthode prixTotal() calculant le prix total de son contenu
* Dans le main, une fois l'inventaire rempli, afficher son prix total
* Sidequest : réécrire la méthode [toString()](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html#toString--) d'Inventaire pour lister la liste des objets possédés
* Sidequest : constater qu'il y a d'autres implémentations de List : [https://docs.oracle.com/javase/tutorial/collections/implementations/list.html](https://docs.oracle.com/javase/tutorial/collections/implementations/list.html)

----

## 5. Des combats plus évolués
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Avant d'implémenter les intelligences artificielles, il faut que nos combats soient un peu plus intéressants
* Les pokémons peuvent maintenant défendre. Ajouter un score de défense (entre 0 et 90) correspondant au pourcentage de dégâts absorbé à chaque attaque reçue  
* Créer une méthode defendre() qui augmente le score de défense de 10  
* Lors d'une attaque, le nombre de points de vie perdu par la cible est maintenant : force de l'attaquant * (1 - défense de la cible / 100). Une attaque fait au moins 1 dégât.  
* Simuler un combat où l'un des pokémons défend 3 fois en début de combat  

----

## 6. Une intelligence un peu artificielle
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
On va maintenant créer des intelligences artificielles qui prendront les décisions d'action des pokemons
* Une IA doit être capable d'agir en fonction de la situation 
* Définir la signature d'une méthode correspondant à l'IA  
* En déduire une interface IA
* Créer 2 implémentations d'IA : une IA "brute" qui attaque dans tous les cas et une IA "maline" qui défend jusqu'à atteindre 90 de défense puis attaque  
* Faire combattre les 2 IA sur un cas d'exemple  
* Créer une 3ème IA selon un algorithme que vous élaborerez
* Sidequest : créer un tournoi d'IA. Les différentes IA s'affronteront sur différents cas de test que vous élaborerez  

----

## Aller plus loin
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" --> 
* Récupérer sur le dépôt git principal le corrigé du TP2. Copier la classe IAGeniale et la faire affronter vos IA. Tant que l'IA géniale l'emporte, améliorer votre IA
* Faites s'affronter votre IA et celle de votre voisin
* (Aller beaucoup beaucoup beaucoup beaucoup beaucoup plus loin) : générer un gros volume de données de type état pokemon1, état pokemon2, action => résultat du combat. En utilisant une méthode d'apprentissage sur ces données, élaborer une IA la plus optimale possible. La confronter à un autre jeu de données et aux IA précédentes