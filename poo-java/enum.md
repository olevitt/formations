# Les énumérations (enum)
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Principe
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Nombre défini d'éléments définis
* Eléments invariables
* Constantes globales

----

## Sans enum
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Filiere {
    private String nom;

    public Filiere(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return this.nom;
    }
}
```

```Java
public class Main {
    public static void main(String[] args) {
        Filiere gdr = new Filiere("GDR");
        Filiere marketing = new Filiere("Marketing");
    }
}
```

----

## Avec enum
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public enum Filiere {
	MARKET("marketing"), GDR("GDR");
	
	private String nom;
	
	private Filiere(String nom) {
		this.nom = nom;
	}
	
	public String getNom() {
		return this.nom;
	}
}
```

```Java
public class Main {
    public static void main(String[] args) {
        Filiere gdr = Filiere.GDR;
    }
}
```