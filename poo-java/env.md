# L'environnement de développement
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Notion de compilation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](img/compile.png)
<!-- .element: class="fragment" -->

----

## Importance de l'IDE
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Un bon artisan = de bons outils
* Integrated Development Environment
* Votre meilleur ami
* <b>Eclipse</b>, IntelliJ (visual studio code ...)  

![](img/hello-world-eclipse.png)

----

## Eclipse : raccourcis utiles
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* ctrl + f11 => exécuter le code
* ctrl + espace => auto-complétion
* ctrl + maj + o => auto-import
* alt + maj + s => menu actions rapides
* D'autres raccourcis : [http://www.vogella.com/tutorials/EclipseShortcuts/article.html](http://www.vogella.com/tutorials/EclipseShortcuts/article.html)

----

## Démo
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Démo