# TP4 : a real project
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Adding maven to an existing project
* Adding dependencies
* Unit testing
* Run SQL requests against a DB
* Run a webserver

----

## Starting point
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* For this session we will start from a light version of previous works.
* It's about the same as a finished TP3 except IAs implementations
* Start by grabing the code from   
[https://bitbucket.org/olevitt/base-tp4/overview](https://bitbucket.org/olevitt/base-tp4/overview)  
* You can get the code using either git clone or bitbucket's downloads menu 
* /!\ Don't import the project into eclipse yet /!\
* In order to use maven, it may be necessary to configure the network proxy. Instructions are available on moodle : [http://foad.ensai.fr/course/view.php?id=270](http://foad.ensai.fr/course/view.php?id=270)

----

## 1. Mavenisation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Maven will give us a lot of power but we first need to set it up.  
Although Eclipse has a "add maven to this project" feature, we are going to do it by hand.    
* Create a pom.xml in the correct location
* Move the source code onto the correct (maven conventions) location
* Setup any other necessary folder 

Now that our project is a maven project, import it onto Eclipse using the "import existing maven project" feature  
* Run the app
* Sidequest : use maven to compile and generate a JAR

----

## 2. Unit testing
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* Add [JUnit (4.12)](https://mvnrepository.com/artifact/junit/junit/4.12) dependency
* Write and run a unit test that will check that the attaquer() method deals the right amount of damage (The spec was defined in TP3 : "The logic for damage dealt change slightly. It is now : attacker's strength * (1 - target's defense score / 100). Every attack deals at least 1 damage")  
* Detect and correct the algorithm's error
* Sidequest : browse to you local maven repository and open the junit JAR to see what's inside

----

## 3. JDBC
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
JDBC (Java DataBase Connectivity) is a Java standard for interacting with a relationnal database (whatever it's type).  
The way you use JDBC and the code you write doesn't depend on the DBMS you are using (PostgreSQL, SQLite, *beurk* oracle *beurk*, *beurk* sqlserver *beurk*).  
You are free to use whatever DBMS you want. We suggest you use either the database provided by Ensai (PostgreSQL) or SQlite which stores data locally inside a file. You are invited to try both ways and see that they are interchangeable.

----

## 3.1 : PostgreSQL configuration
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
To connect to a PostgreSQL database you will need :  
<b>Database info</b> :  
Host sgbd-eleves.domensai.ecole
Port 5432  
Database idX9876 (replace with your own id)  
User idX9876 (replace with your own id)  
Password idX9876 (replace with your own id)  
  
<b>The PostgreSQL JDBC driver</b> :  
https://mvnrepository.com/artifact/org.postgresql/postgresql/42.2.5  

URL JDBC : jdbc:postgresql://hote/database?user=...&password=...

----

## 3.2 : SQLite configuration
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
SQLite stores the data inside a single file (it can also store it in memory), so you only need the SQLite JDBC driver :  
https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc/3.25.2  

URL JDBC : jdbc:sqlite:C:/path/to/the/db/base.db

----

## 4. Hello SQL
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
String url = "jdbc:postgresql://hote/database?user=...&password=...";
//String url = "jdbc:sqlite:C:/chemin/vers/la/base/base.db";
Connection connection = DriverManager.getConnection(url);
```
This code will open a connection with the database.  
Once you have an open connection, you can execute queries :   
```Java
PreparedStatement ps = connection.prepareStatement("CREATE TABLE pokemon(id INT PRIMARY KEY)");
ps.executeUpdate();
```
```Java
PreparedStatement ps = connection.prepareStatement("INSERT INTO pokemon(id) VALUES(?)");
ps.setInt(1,42);
ps.executeUpdate();
```
```Java
PreparedStatement ps = connection.prepareStatement("SELECT * FROM pokemon");
ResultSet result = ps.executeQuery();
```
https://docs.oracle.com/javase/tutorial/jdbc/basics/processingsqlstatements.html

----

## 5. Enjoy DB
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* When the application starts, insert some Pokemon data into the database
* During fights, keep an history of each action made
* Handle errors (exceptions) when the database is unreachable or the query is invalid

----

## 6. IA
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
If you haven't done it in previous sessions, implement some IA logic :
"Let's write some AI that will be able to decide the best move each time  
* An AI should be able to act given the current situation 
* Define the signature of an AI method
* Create the corresponding AI interface
* Create 2 AI implementations : the "brute" one that constantly attacks and the vicious one that defends until it reachs 90 defense and then attacks  
* Make the 2 AI fight using the pokemons you want
* Create a third implementation using your own algorithm
* Sidequest : write an AI tournament. They compete against each other. Each won fight (using generated pokemon data) scores 1 point for the winning AI"

----

## 7. A webserver
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* Using [Javalin](https://javalin.io/), write a webserver 
* On /pokemon, display the list of pokemons stored in the database
* On /fight, start a fight and display the result. Display the whole fight's history


----

## Going deeper
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" --> 
* Using [Maven shade plugin](https://maven.apache.org/plugins/maven-shade-plugin/examples/executable-jar.html), create a runnable JAR file. Run it without using eclipse