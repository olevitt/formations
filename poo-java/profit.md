# Profit
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Prérequis : projet mavenisé
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* pom.xml à la racine
```XML
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>fr.insee.recensement</groupId>
  <artifactId>collecte-internet</artifactId>
  <version>1.0</version>
</project>
```
* conventions respectées

----

## Génerer un JAR
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```
mvn package
```
* Démo

----

## Ajouter une dépendance
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```XML
<dependencies>
	<dependency>
		<groupId>groupId</groupId>
		<artifactId>artifactId</artifactId>
		<version>version</version>
	</dependency>
</dependencies>
```
* Fragment POM
* https://mvnrepository.com/

----

## Ajouter une dépendance : démo
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
http://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/stat/regression/SimpleRegression.html
```XML
<dependencies>
	<dependency>
		<groupId>org.apache.commons</groupId>
		<artifactId>commons-math3</artifactId>
		<version>3.6.1</version>
	</dependency>
</dependencies>
```
```Java
SimpleRegression regression = new SimpleRegression();
regression.addData(1, 2);
regression.addData(2, 3);
regression.addData(3, 4);
System.out.println(regression.getIntercept());
```

----

## Ajouter une dépendance : plus fort
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
https://javalin.io/
```XML
<dependencies>
<dependency>
    <groupId>io.javalin</groupId>
    <artifactId>javalin</artifactId>
    <version>2.6.0</version>
</dependency>
</dependencies>
```
```Java
Javalin app = Javalin.create().start(7000);
app.get("/", ctx -> {
	System.out.println("Requête reçue");
	ctx.result("Hello world");
});
```

----

## Conclusion
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Maven s'occupe de tout
* On déclare, il gère

