# Les conteneurs
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Objectif
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Plusieurs données du même type : conteneur.  
* Tableau
* Liste
* Set
* Map

----

## La base : le tableau
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
"Ordonné par indice, taille fixe" 
Déclaration
```Java
int[] tableau = {1,2,3};
Pokemon[] pokemons = {pokemon1, pokemon2};
String[] strings = new String[10];
``` 

Accès
```Java
int valeur = tableau[0];
String valeur2 = strings[1];
Pokemon pokemon = pokemons[0];
``` 

Taille
```Java
int taille = valeur.length;
``` 

Modification
```Java
tableau[0] = 42;
``` 

----

## La (les) liste(s)
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
"Ordonné par indice, taille variable"
* [List](https://docs.oracle.com/javase/8/docs/api/java/util/List.html) = interface
* [ArrayList](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html) = implémentation de référence

Déclaration
```Java
ArrayList<String> strings = new ArrayList<>();
ArrayList<String> strings = new List<>();
List<String> strings = new List<>();
List<String> strings = new ArrayList<>();
``` 
Lequel ?

----

## Listes : utilisation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Déclaration
```Java
List<Pokemon> pokemons = new ArrayList<>();
List<Pokemon> pokemons = Arrays.asList(pokemon1, pokemon2);
``` 

Accès
```Java
Pokemon pokemon = pokemons.get(0);
``` 

Taille
```Java
int taille = pokemons.size();
``` 

Modification
```Java
pokemons.add(pokemon);
pokemons.set(42,pokemon);
``` 

----

## La (les) set(s)
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
"Non ordonné, taille variable, unicité des éléments"
* [Set](https://docs.oracle.com/javase/8/docs/api/java/util/Set.html) = interface
* [HashSet](https://docs.oracle.com/javase/8/docs/api/java/util/HashSet.html) = implémentation de référence

Déclaration
```Java
HashSet<String> strings = new HashSet<>();
HashSet<String> strings = new Set<>();
Set<String> strings = new Set<>();
Set<String> strings = new HashSet<>();
``` 
Lequel ?

----

## Sets : utilisation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Déclaration
```Java
Set<String> strings = new HashSet<>();
Set<Pokemon> pokemons = new HashSet<>(listePokemons);
``` 

Accès
```Java
?
``` 

Taille
```Java
int taille = pokemons.size();
``` 

Modification
```Java
pokemons.add(pokemon);
``` 

----

## La (les) map(s)
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Système "clé - valeur"  
"Non ordonné, taille variable, accès par clés, unicité des clés"
* [Map](https://docs.oracle.com/javase/8/docs/api/java/util/Map.html) = interface
* [HashMap](https://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html) = implémentation de référence

Déclaration
```Java
HashMap<Utilisateur,Integer> scores = new HashMap<>();
HashMap<Utilisateur,Integer> scores = new Map<>();
Map<Utilisateur,Integer> scores = new Set<>();
Map<Utilisateur,Integer> scores = new HashSet<>();
``` 
Lequel ?

----

## Maps : utilisation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Déclaration
```Java
Map<Utilisateur,Integer> scores = new HashMap<>();
Map<Pokemon,Boolean> pokedex = new HashMap<>();
``` 

Accès
```Java
Integer score = scores.get(utilisateur);
``` 

Taille
```Java
int taille = scores.size();
``` 

Modification
```Java
scores.put(utilisateur,42);
``` 

----

## Conteneurs : bilan
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
| Type | Ordonné ? | Taille variable ? | Accès |
|------|-------------|--------|---------|
| Tableau | Oui | Non | tableau[i] |
| List | Oui | Oui | liste.get(i) |
| Set | Non | Oui | itération |
| Map | Non | Oui | map.get(key) |

----

## Conteneurs : itération
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Par indice
```Java
String[] tableau = {"toto","tata","titi"};
for (int i = 0; i < tableau.length; i++) {
    System.out.println(tableau[i]);
}
List<Integer> tirageLoto = Arrays.asList({ 4, 8, 15, 16, 23, 42 });
for (int i = 0; i < tirageLoto.size(); i++) {
    System.out.println(tirageLoto.get(i));
}
```
Itération sur [Iterable](https://docs.oracle.com/javase/8/docs/api/java/lang/Iterable.html)
```Java
for (String str : tableau) { System.out.println(str); }
for (Integer numero : tirageLoto) { System.out.println(numero); }
Set<Pokemon> pokemonsCaptures = new HashSet<>();
for (Pokemon pokemon : pokemonsCaptures) { System.out.println(pokemon.toString()); }
```
/!\ Attention aux suppressions /!\  
En exercice : itérer sur une Map