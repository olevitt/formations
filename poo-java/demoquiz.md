# Démoquiz
Comment se comporte le code ?  
* Compile / compile pas ?  (Compile time)
* Plante / plante pas ?  (Runtime)
* Effets ?

----

## ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
public class Animal {
    public void crier() {
        System.out.println("Hiiiii");
    }
}
```
```Java
public class Chien extends Animal {
    public void crier() {
        System.out.println("Ouaf");
    }
}
```
```Java
Chien chien = new Chien();
chien.crier();
Animal animal = new Animal();
animal.crier();
Animal monChien = chien;
monChien.crier();
```  

----

## ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
public class Animal {
    private String nom;

    public Animal(String nom) {
        this.nom = nom;
    }
}
```
```Java
public class Chien extends Animal {

}
```
```Java
Animal titi = new Animal("Titi");
Chien medor = new Chien("Medor");
Chien medor = new Chien();
```

----

## ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
public abstract class Animal {
    
    public void crier() {
        System.out.println("Hiiiiii");
    }
}
```
```Java
public class Chien extends Animal {

}
```
```Java
Chien chien = new Chien();
chien.crier();
Animal animal = new Animal();
animal.crier();
``` 

----

## ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
public abstract class Animal {
    
    public void crier() {
        System.out.println("Hiiiiii");
    }

    public abstract String getNom();
}
```
```Java
public class Chien extends Animal {

}
```
```Java
Chien chien = new Chien();
chien.crier();
``` 

----

## ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
public interface Volant {
    public void voler();
}
```
```Java
public interface Roulant {
    public void rouler();
}
```
```Java
public class Voiture implements Roulant {
    public void rouler() {
        System.out.println("Vroum");
    }
}
```
```Java
public class Avion implements Roulant, Volant {
    public void rouler() {
        System.out.println("Vroum");
    }
    public void voler() {
        System.out.println("Cuicui");
    }
}
```


----

## Cast
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
public class Animal {
    public void crier() {
        System.out.println("Hiiiii");
    }
}
```
```Java
public class Chien extends Animal {
    public void chercherBaton() {
        System.out.println("...");
    }
}
```
```Java
Chien chien = new Chien();
chien.chercherBaton();
Animal animal = new Chien();
animal.crier();
animal.chercherBaton();  
```
```Java
((Chien) animal).chercherBaton();
```
<!-- .element: class="fragment" --> 