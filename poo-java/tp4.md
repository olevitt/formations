# TP4 : un vrai projet
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Mavenisation d'un projet
* Ajout de dépendances
* Tests unitaires
* Utilisation d'une base de données
* Serveur web

----

## Le point de départ
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Pour ce TP, on propose de repartir sur une version un peu allégée des TP précédents  
* Cela correspond en gros au TP3 avant d'implémenter les IA
* Commencez par récupérer le code de base sur ce dépôt :  
[https://bitbucket.org/olevitt/base-tp4/overview](https://bitbucket.org/olevitt/base-tp4/overview)  
* Vous pouvez au choix, soit cloner le dépôt en utilisant git, soit télécharger le zip du dépôt  
* /!\ Ne pas importer le projet sous eclipse pour l'instant /!\
* Afin d'utiliser maven, il peut être nécessaire de configurer le proxy : [http://foad.ensai.fr/course/view.php?id=270](http://foad.ensai.fr/course/view.php?id=270)

----

## 1. Mavenisation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Pour pouvoir profiter, il est important que le projet soit mavenisé.  
Bien qu'eclipse propose une fonction de mavenisation, on se propose de le faire à la main.  
* Créer un fichier pom.xml au bon endroit
* Déplacer le code source dans la bonne arborescence
* Préparer une arborescence qui contiendra le code de test

Une fois le projet mavenisé, l'importer sous eclipse en tant que "existing maven project"  
* Lancer l'application  
* Sidequest : utiliser maven pour compiler et générer un JAR

----

## 2. Des tests
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* Ajouter [JUnit (4.12)](https://mvnrepository.com/artifact/junit/junit/4.12) au projet
* Ecrire et exécuter un test unitaire testant que la méthode attaquer() inflige bien le bon nombre de points de dégâts (Rappel de la spécification : "Lors d'une attaque, le nombre de points de vie perdu par la cible est maintenant : force de l'attaquant * (1 - défense de la cible / 100). Une attaque fait au moins 1 dégât.")  
* Détecter et corriger l'erreur dans l'algorithme
* Sidequest : ouvrir l'explorateur de fichiers windows, naviguer jusqu'au dépôt maven local et ouvrir le JAR junit pour voir ce qu'il contient

----

## 3. JDBC
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
JDBC (Java DataBase Connectivity) est un standard pour intéragir avec une base de données (peu importe son type) en Java.  
Le code est similaire, peu importe le type de base de données utilisé (PostgreSQL, SQLite, *beurk* oracle *beurk*, *beurk* sqlserver *beurk*).  
Pour ce TP, vous avez le choix d'utiliser la base de donnée fournie par l'Ensai (sous PostgreSQL) ou une base de données fichier, plus légère : SQLite. Vous êtes invités à tester les deux et à remarquer qu'il n'y a aucune différence au niveau du code.

----

## 3.1 : configuration PostgreSQL
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Pour vous connecter à une base de données PostgreSQL en Java, il vous faut :  
<b>Les informations de connexion de la base</b> :  
Hôte sgbd-eleves.domensai.ecole
Port 5432  
Database idX9876 (id à remplacer)  
User idX9876 (id à remplacer)  
Password idX9876 (id à remplacer)  
  
<b>Le driver JDBC PostgreSQL</b> :  
https://mvnrepository.com/artifact/org.postgresql/postgresql/42.2.5  

URL JDBC : jdbc:postgresql://hote/database?user=...&password=...

----

## 3.2 : configuration SQLite
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
SQLite stocke les données dans un fichier (ou en mémoire), il vous suffit donc d'avoir le driver JDBC et de choisir l'emplacement du fichier à utiliser.
https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc/3.25.2  

URL JDBC : jdbc:sqlite:C:/chemin/vers/la/base/base.db

----

## 4. Hello SQL
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
String url = "jdbc:postgresql://hote/database?user=...&password=...";
//String url = "jdbc:sqlite:C:/chemin/vers/la/base/base.db";
Connection connection = DriverManager.getConnection(url);
```
Ce code permet d'obtenir une connexion à la base de données.  
Une fois une connexion obtenue, il est possible de faire des requêtes :  
```Java
PreparedStatement ps = connection.prepareStatement("CREATE TABLE pokemon(id INT PRIMARY KEY)");
ps.executeUpdate();
```
```Java
PreparedStatement ps = connection.prepareStatement("INSERT INTO pokemon(id) VALUES(?)");
ps.setInt(1,42);
ps.executeUpdate();
```
```Java
PreparedStatement ps = connection.prepareStatement("SELECT * FROM pokemon");
ResultSet result = ps.executeQuery();
```
https://docs.oracle.com/javase/tutorial/jdbc/basics/processingsqlstatements.html

----

## 5. Enjoy DB
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* Au lancement de l'application, charger des données de Pokemon en base
* Lors d'un combat, garder un historique de toutes les actions faites  
* Gérer les erreurs en cas de problème de connexion avec la base ou de requête invalide

----

## 6. IA
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Si vous ne l'avez pas déjà fait dans les TP précédents, implémenter plusieurs IA de combat :
"On va maintenant créer des intelligences artificielles qui prendront les décisions d'action des pokemons
* Une IA doit être capable d'agir en fonction de la situation 
* Définir la signature d'une méthode correspondant à l'IA  
* En déduire une interface IA
* Créer 2 implémentations d'IA : une IA "brute" qui attaque dans tous les cas et une IA "maline" qui défend jusqu'à atteindre 90 de défense puis attaque  
* Faire combattre les 2 IA sur un cas d'exemple  
* Créer une 3ème IA selon un algorithme que vous élaborerez
* Sidequest : créer un tournoi d'IA. Les différentes IA s'affronteront sur différents cas de test que vous élaborerez"

----

## 7. Un serveur web
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* En utilisant [Javalin](https://javalin.io/), créer un serveur web.  
* Sur /pokemon, afficher la liste des pokemons contenus dans la base
* Sur /combat, déclencher un combat et afficher le resultat du combat. Afficher l'historique de chaque action du combat


----

## Aller plus loin
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" --> 
* En utilisant [Maven shade plugin](https://maven.apache.org/plugins/maven-shade-plugin/examples/executable-jar.html), créer un JAR exécutable de l'application. Lancer l'application à partir du JAR, sans utiliser Eclipse.