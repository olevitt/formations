# Précédemment dans Java
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Tout n'est que classe
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Créer des classes
```Java
public class Etudiant {
    public String nom = "Léponge";
    public String prenom = "Bob";

    public Etudiant(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }
}
```  

----

## Typage fort
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
Etudiant etudiant = new Etudiant("Toto","titi");
int nombre = 2;
String phrase = "Hello world";
``` 

```Java
public class Pokemon {
    public int pv;
    public int force;

    public void attaquer(Pokemon adversaire) {
        adversaire.pv = adversaire.pv - this.force;
    }
}
``` 
<!-- .element: class="fragment" -->

----

## 3 principes
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Encapsulation
* Héritage
* Polymorphisme

----

## Encapsulation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*
* Private partout*

\* presque partout

----

## Encapsulation, en vrai
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Pokemon {
    private int pv;
    private int force;

    public int getForce() {
        return this.force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public int getPv() {
        return this.pv;
    }

    public void setPv(int pv) {
        this.pv = pv;
        if (this.pv <= 0) {
            pokemonKO();
        }
    }
}
``` 

----

## Héritage
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Etudiant {
    private String nom;

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
``` 

```Java
public class EtudiantFonk {

    public void postulerAuSNDIP() {
        System.out.println(getNom()+" souhaite intégrer le SNDIP");
    }
}
``` 

----

## Polymorphisme
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Objet {
    public void utiliser(Pokemon cible) {

    }
}
``` 

```Java
public class Potion extends Objet {
    private int vie = 10;

    public void utiliser(Pokemon cible) {
        cible.gagnerPV(this.vie);
    }
}
``` 

```Java
public class Pokeball extends Objet {
    public void utiliser(Pokemon cible) {
        cible.capturer();
    }
}
``` 

----

## Polymorphisme : classe abstraite
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public abstract class Objet {
    public abstract void utiliser(Pokemon cible);
}
``` 

```Java
public class Potion extends Objet {
    private int vie = 10;

    public void utiliser(Pokemon cible) {
        cible.gagnerPV(this.vie);
    }
}
``` 

```Java
public class Pokeball extends Objet {
    public void utiliser(Pokemon cible) {
        cible.capturer();
    }
}
``` 

----

## Polymorphisme : interface
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public interface Utilisable {
    public void utiliser(Pokemon cible);
}
``` 

```Java
public class Potion implements Utilisable {
    private int vie = 10;

    public void utiliser(Pokemon cible) {
        cible.gagnerPV(this.vie);
    }
}
``` 

```Java
public class Pokeball implements Utilisable {
    public void utiliser(Pokemon cible) {
        cible.capturer();
    }
}
``` 