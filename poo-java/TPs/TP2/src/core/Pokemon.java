package core;

public class Pokemon {
	
	public static final int NIVEAU_MAX = 5;
	private static int currentId = 0;
	private int id;
	private Espece espece;
	private final String surnom;
	private float taille;
	private int niveau = 1;
	private int xp = 0;
	private int pv = 100;
	private int force = 10;
	private int defense = 0;
	
	
	public Pokemon(Espece espece, String surnom) {
		this.espece = espece;
		this.id = currentId;
		currentId++;
		this.surnom = surnom;
		this.pv = espece.getPvBase();
		this.force = espece.getForceBase();
	}
	
	public void gagnerExperience(int nbXP) {
		this.xp += nbXP;
		int nouveauNiveau = Niveau.niveauCorrespondant(xp);
		this.niveau = nouveauNiveau;
	}
	
	public void mangerBonbon(Bonbon bonbon) {
		if (!bonbon.isDejaConsomme()) {
			this.gagnerExperience(bonbon.getXp());
			bonbon.setDejaConsomme(true);
		}
	}
	
	public void attaquer(Pokemon cible) {
		int nbDegats = force * (1-cible.getDefense() / 100);
		if (nbDegats <= 0) {
			nbDegats = 1;
		}
		cible.pv = cible.pv - nbDegats;
	}
	
	public void defendre() {
		this.setDefense(this.getDefense() + 10);
	}
	
	private void setDefense(int defense) {
		this.defense = defense;
		this.defense = Math.max(0, this.defense);
		this.defense = Math.min(100, this.defense);
	}
	
	

	public int getDefense() {
		return defense;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Espece getEspece() {
		return espece;
	}

	public void setEspece(Espece espece) {
		this.espece = espece;
	}

	public float getTaille() {
		return taille;
	}

	public void setTaille(float taille) {
		this.taille = taille;
	}

	public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

	public int getXp() {
		return xp;
	}

	public void setXp(int xp) {
		this.xp = xp;
	}

	public int getPv() {
		return pv;
	}

	public void setPv(int pv) {
		this.pv = pv;
	}

	public int getForce() {
		return force;
	}

	public void setForce(int force) {
		this.force = force;
	}

	public String getSurnom() {
		return surnom;
	}

	public String toString() {
		return "Pokemon [id=" + id + ", espece=" + espece + ", surnom=" + surnom + ", taille=" + taille + ", niveau="
				+ niveau + ", xp=" + xp + ", pv=" + pv + ", force=" + force + "]";
	}

	

	

	
	
	

}
