package core;

public class Joueur {
	
	private static Joueur JOUEUR;
	private Inventaire inventaire = new Inventaire();
	
	private Joueur() {
		
	}
	
	public static Joueur getJoueur() {
		if (JOUEUR == null) {
			JOUEUR = new Joueur();
		}
		return JOUEUR;
	}

	public Inventaire getInventaire() {
		return inventaire;
	}
	
}
