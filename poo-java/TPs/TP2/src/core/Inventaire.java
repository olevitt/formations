package core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Inventaire {

	private List<Item> objets = new ArrayList<>();

	public List<Item> getObjets() {
		return objets;
	}
	
	public int getPrixTotal() {
		int total = 0;
		for (Item item : objets) {
			total += item.getPrix();
		}
		return total;
	}
	
	@Override
	public String toString() {
		String result = "";
		
		for (Item item : objets) {
			result = result + item.toString();
			result += System.getProperty("line.separator");
		}
		
		return result;
	}
	
	
}
