package ia;

import core.Pokemon;

public interface IA {
	
	public void agir(Pokemon pokemonActif, Pokemon cible);

}
