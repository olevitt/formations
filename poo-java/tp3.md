# TP3 : Java++
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Application des concepts avancés de POO
* Enums
* Conteneurs
* Exceptions
* Création et comparaison d'intelligences artificielles simples
Il s'agit d'un TP de stabilisation.  

Ce qui n'a pas été traité dans le TP2 est de retour : combats d'IA !

----

## Le point de départ
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Pour ce TP, on propose de repartir sur une version un peu allégée des TP précédents 
* Commencez par récupérer le code de base sur ce dépôt :  
[https://bitbucket.org/olevitt/base-tp3/overview](https://bitbucket.org/olevitt/base-tp3/overview)  
* Vous pouvez au choix, soit cloner le dépôt en utilisant git, soit télécharger le zip du dépôt  
* Le projet est importable via la fonctionnalité File => Import => General => Existing projects into Workspace  
* Prendre un peu de temps pour reparcourir le code

----

## 1. Enumération des espèces
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Actuellement, les espèces sont créees à la volée dans la méthode Main.  
En considérant que l'ensemble des espèces est fixe, on gagnerait en clarté à la définir une fois pour toute.  
* En utilisant un Enum, retravailler le code pour avoir clairement les différentes espèces disponibles
* Apprécier la simplification de la méthode main et de l'utilisation des espèces
* Sidequest : réécrire la fonction niveauCorrespondant de la classe Niveau

----

## 2. Un inventaire
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Le joueur a maintenant accès à un inventaire.    
* Créer une classe vide Inventaire. Inventaire contiendra bientôt les objets possédés par le joueur.
* Créer une classe Joueur possédant un Inventaire. Notre jeu n'aura qu'un et un seul Joueur. Ce joueur doit pouvoir être accessible à tout endroit de notre code. Le joueur sera toujours le même pendant le jeu.  

----

## 3. Plein d'objets
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Pour les objets de notre Inventaire, il nous faut une structure de données adaptée.  
* Quelle structure de données vous parait le plus adaptée ici ?
* Ajouter à l'inventaire cette structure
* Dans le main, ajouter différents objets à l'inventaire  
* Ajouter à Inventaire une méthode prixTotal() calculant le prix total de son contenu
* Dans le main, une fois l'inventaire rempli, afficher son prix total
* L'inventaire a une taille limitée. L'ajout d'un objet dans un inventaire plein échoue et l'utilisateur doit en être prévenu.
* Sidequest : réécrire la méthode [toString()](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html#toString--) d'Inventaire pour lister la liste des objets possédés
* Sidequest : créer un Pokedex qui recensera les espèces croisées. Pour chaque espèce, on veut savoir si elle a été vue, combattue ou jamais vue.

----

## 3. Des combats plus évolués
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Avant d'implémenter les intelligences artificielles, il faut que nos combats soient un peu plus intéressants
* Les pokémons peuvent maintenant se protéger. Ajouter un score de défense (entre 0 et 90) correspondant au pourcentage de dégâts absorbé à chaque attaque reçue  
* Créer une méthode defendre() qui augmente le score de défense de 10  
* Lors d'une attaque, le nombre de points de vie perdu par la cible est maintenant : force de l'attaquant * (1 - défense de la cible / 100). Une attaque fait au moins 1 dégât.  
* Simuler un combat où l'un des pokémons se protège 3 fois en début de combat  

----

## 4. Une intelligence un peu artificielle
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
On va maintenant créer des intelligences artificielles qui prendront les décisions d'action des pokemons
* Une IA doit être capable d'agir en fonction de la situation 
* Définir la signature d'une méthode correspondant à l'IA  
* En déduire une interface IA
* Créer 2 implémentations d'IA : une IA "brute" qui attaque dans tous les cas et une IA "maline" qui défend jusqu'à atteindre 90 de défense puis attaque  
* Faire combattre les 2 IA sur un cas d'exemple  
* Créer une 3ème IA selon un algorithme que vous élaborerez
* Sidequest : créer un tournoi d'IA. Les différentes IA s'affronteront sur différents cas de test que vous élaborerez  

----

## Aller plus loin
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" --> 
* Récupérer sur le dépôt git principal le corrigé du TP2. Copier la classe IAGeniale et la faire affronter vos IA. Tant que l'IA géniale l'emporte, améliorer votre IA
* Faites s'affronter votre IA et celle de votre voisin
* En utilisant vos nouvelles connaissances sur les tableaux, investiguer ce fameux String[] args en paramètres de la méthode main
* (Aller beaucoup beaucoup beaucoup beaucoup beaucoup plus loin) : générer un gros volume de données de type pvPokemon1, forcePokemon1, defensePokemon1, pvPokemon2, forcePokemon2, defensePokemon2, action => résultat du combat. En utilisant la méthode statistique de votre choix ou un algorithme d'apprentissage sur ces données, élaborer une IA la plus optimale possible. La confronter à un autre jeu de données et aux IA précédentes