# Polymorphisme
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* En informatique et en théorie des types, le polymorphisme est le concept consistant à fournir une interface unique à des entités pouvant avoir différents types.
<!-- .element: class="fragment" --> 

[https://fr.wikipedia.org/wiki/Polymorphisme_(informatique)](https://fr.wikipedia.org/wiki/Polymorphisme_(informatique%29)
<!-- .element: class="fragment" --> 

----

## Le problème
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* Tous les animaux savent crier  
* Mais ils crient différemment  

```Java
Animal animal = new Animal();
// Impossible de faire crier animal
Chien chien = new Chien();
chien.aboyer();
Chat chat = new Chat();
chat.miauler();
```

----

## Une solution
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* Tous les animaux savent crier  

```Java
public class Animal {

    public void crier() {
        System.out.println("Hiiiiii");
    }
}
```  
```Java
Animal animal = new Animal();
animal.crier(); // Hiiiiii
Chien chien = new Chien();
chien.crier(); // Hiiiiii
Chat chat = new Chat();
chat.crier(); // Hiiiiii
```

----

## Plusieurs formes
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Mais ils crient différemment  

```Java
public class Chien extends Animal {

    @Override
    public void crier() {
        System.out.println("Ouaf");
    }
}
```  

```Java
public class Chat extends Animal {

    public void crier() {
        System.out.println("Miaouh");
    }
}
```  
```Java
Animal animal = new Animal();
animal.crier(); // Hiiiiii
Chien chien = new Chien();
chien.crier(); // Ouaf
Chat chat = new Chat();
chat.crier(); // Miaouh
```

----

## Polymorphisme : en résumé
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* 1 spécification : plusieurs comportements
* Comportement déterminé à la volée par Java

----

## Aller plus loin : spécification vs implémentation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
La théorie de la machine

----

## Signature vs corps
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
/**
 Enregistre le pokemon en base de donnée
**/
public void savePokemon(Pokemon pokemon) {
    Connection c = null;
	try {
		Class.forName("org.postgresql.Driver");
		c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/db",
				"admin", "password");
		PreparedStatement st = c.prepareStatement("INSERT INTO pokemon(name) VALUES(?)");
		st.setString(1, pokemon.getName());
		st.executeUpdate();
		st.close();
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
        if (c != null) {
            c.close();
        }
	}
}
```

----

## Découpler interface et implémentation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Séparer la déclaration de la signature de celle du corps
* Signatures = interface (= contrat)
* Corps = implémentation
* 1 signature = plusieurs implémentations

----

## Interface
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
public interface Vendable {
    public int getPrix();
}
```

----

## Implémentation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class DVD implements Vendable {

    public int getPrix() {
        return 1;
    }
}
```

```Java
public class Armoire extends Mobilier implements Vendable {

    public int getPrix() {
        return 20 * getAge();
    }

}
```

----

## Utilisation des interfaces
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Caisse {

    private int argent;

    public void vendre(Vendable objetAVendre) {
        this.argent += objetAVendre.getPrix();
    }
}
```

----

## Abstract
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* abstract = définition que la signature uniquement  
* interface = ensemble de méthodes abstract
* abstract method, abstract class  

```Java
public abstract class Animal {

    private String nom;

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public abstract void crier();
    
}
```
