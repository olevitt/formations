# C'est quoi le programme ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## 3 objectifs
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
1. Renforcer les concepts de programmation orientée objet  
2. Appliquer ces concepts en Java  
3. Aller plus loin dans le cycle de développement  

----

## Articulation des séances
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* 9 janvier : rappels de POO, introduction à Java
* 16 janvier : concepts de POO avancés
* 23 janvier : Java avancé, itérateurs
* 20 février : cycle de développement, packaging, dépendances ...