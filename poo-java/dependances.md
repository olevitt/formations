# Les dépendances
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
(= bibliothèques)

----

## L'enjeu
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Réutiliser du code
* Ne pas inventer la roue
* Gérer les versions

----

## En python ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* pip (Pip Installs Packages)
<!-- .element: class="fragment" -->
```
pip install numpy
```
<!-- .element: class="fragment" -->
```
pip install -r requirements.txt
```
<!-- .element: class="fragment" -->

----

## En Java
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Le JAR
```
javac -cp lib.jar Main.java
```
```
java -jar lib.jar Main Main.class
```
/!\ A reproduire chez vous /!\

----

## En Java, via eclipse
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Démo
/!\ A ne pas reproduire chez vous /!\

----

## Conclusion (temporaire)
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Inclure les JAR à la compilation
* Inclure les JAR dans le livrable
* Dépendances de dépendances ?
* Indépendant de la machine ?