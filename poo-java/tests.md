# Tests unitaires
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Prérequis : projet mavenisé
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* pom.xml à la racine
```XML
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>fr.insee.recensement</groupId>
  <artifactId>collecte-internet</artifactId>
  <version>1.0</version>
</project>
```
* conventions respectées

----

## Rappel : structure d'un projet maven
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```
my-app
|-- pom.xml
`-- src
    |-- main
    |   `-- java
    |       `-- com
    |           `-- mycompany
    |               `-- app
    |                   `-- App.java
    `-- test
        `-- java
            `-- com
                `-- mycompany
                    `-- app
                        `-- AppTest.java
```

----

## Tests : dépendance JUnit
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```XML
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
    <scope>test</scope>
</dependency>
```

----

## Tests : GIVEN / WHEN / THEN
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* GIVEN : situation initiale
* WHEN : élément déclencheur
* THEN : situation finale

----

## Tests : premier test
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Code à tester (src/main/java/monpackage/Maths.java) :  
```Java
public class Maths {
	public int addition(int a, int b) {
		return a+b;
	}
}
```
Code de test (src/test/java/monpackage/MathsTest.java) :  
```Java
public class MathsTest {
	@Test
	public void testAddition() {
		//GIVEN
		int a = 1;
		int b = 2;
		//WHEN
		int c = new Maths().addition(a,b);
		//THEN
		Assert.assertEquals(3,c);
	}
}
```

----

## Tests : lancer les tests
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```
mvn test
```
ou via eclipse