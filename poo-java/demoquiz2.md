## Démoquiz
Comment se comporte le code ?  
* Compile / compile pas ?  (Compile time)
* Plante / plante pas ?  (Runtime)
* Effets ?

----

## ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
public class Exemple {
    public static void main(String[] args) {
        String toto = null;
        System.out.println(toto.toString());
        System.out.println("Fin");
    }
}
```

----

## ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
public class Exemple {
    public static void main(String[] args) throws NullPointerException {
        String toto = null;
        System.out.println(toto.toString());
        System.out.println("Fin");
    }
}
```

----

## ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
public class Exemple {
    public static void main(String[] args) {
        danger();
        System.out.println("Fin");
    }

    public static void danger() throws NullPointerException {
        String toto = null;
        System.out.println(toto.toString());
    }
}
```

----

## ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
```Java
public class Exemple {
    public static void main(String[] args) {
        String toto = null;
        try {
            System.out.println(toto.toString());
        }
        catch (NullPointerException exception) {
            exception.printStackTrace();
        }
        System.out.println("Fin");
    }
}
```

----

## Stacktrace || GTFO
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
<img src="img/stacktraceorgtfo.jpg" width="300">
