# Organisation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Planning
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
1h30 de cours, 3h de TP
* 9 janvier
* 16 janvier
* 30 janvier
* 20 février

----

## Le TP fil rouge
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Thème commun des 4 TP : Pokemon  

----

## Evaluation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* TP noté
* Examen écrit