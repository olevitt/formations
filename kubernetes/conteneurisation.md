# Conteneurisation
<!-- .slide: class="slide" -->  

[Tuto](https://github.com/olevitt/tutoriels/blob/master/conteneurisation/principes/README.md)

----

## C'est quoi ?
<!-- .slide: class="slide" -->  

* Lancer des processus isolés 
* Utilisation de fonctionnalités du noyau linux  
* Image vs conteneur

----

## VM vs Conteneurisation
<!-- .slide: class="slide" -->  

![](img/vm.png)<!-- .element height="40%" width="40%" --> ![](img/conteneurs.png)<!-- .element height="40%" width="40%" -->  

----

## Alternative vidéo
<!-- .slide: class="slide" -->

<iframe width="560" height="315" src="https://www.youtube.com/embed/0qotVMX-J5s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

