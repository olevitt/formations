# Docker
<!-- .slide: class="slide" -->  

[Tuto](https://github.com/olevitt/tutoriels/blob/master/docker/hello/README.md)  
[Tuto avancé](https://github.com/olevitt/tutoriels/blob/master/docker/avance/README.md)

----

## C'est quoi ?
<!-- .slide: class="slide" -->  

* Moteur no1 de conteneurisation  
* Niveau micro : management local  
* Ne pas confondre : <b>docker engine</b>, docker hub, docker compose, docker swarm

----

## Installation
<!-- .slide: class="slide" -->  

* Linux > Mac OS >>> Windows  
* https://docs.docker.com/engine/install/  

----

## Démo
<!-- .slide: class="slide" -->  

----

## Lancement de conteneurs  
<!-- .slide: class="slide" -->  

```
docker run hello-world
```  

```
docker run -it ubuntu
```  

```
docker run -it ubuntu /bin/bash
```

```
docker run -p 8888:80 nginx
```  

```
docker run --env POSTGRES_PASSWORD=secret -p 5432:5432 postgres:13
```  

```
docker run --env POSTGRES_PASSWORD=secret -p 5432:5432 -d postgres:13
```  

----

## Management (local) de conteneurs  
<!-- .slide: class="slide" -->  

```
docker ps
```  

```
docker logs ID
```  

```
docker kill ID
```  

```
docker exec -it ID COMMAND
```  

----

## Création d'images 
<!-- .slide: class="slide" -->  

Dockerfile :  
```
FROM ubuntu
RUN apt-get update
RUN apt-get -y install cowsay
ENTRYPOINT ["/usr/games/cowsay"]
```  

Docker build :  
```
docker build -t toto .
```  

Docker enjoy :
```  
docker run toto
```  

----

## Publication d'images
<!-- .slide: class="slide" -->  

Nom d'une image docker :  
```  
registry/organisation/nomimage:tag
```  

```  
postgres
postgres:12  
inseefrlab/onyxia-ui
gcr.io/distroless/base-debian10:nonroot
```  

```
docker tag toto monpseudodockerhub/titi
```  

```
docker login  
docker push monpseudodockerhub/titi
```

----

## TP : matos
<!-- .slide: class="slide" -->  

<b>Option cloud</b>  

* Une VM ```f1-micro``` (1vCPU, 600 Mo RAM) est suffisante.  
* OS conseillé pour la VM : debian 10 (les autres OS devraient aussi marcher).
* L'ouverture d'un port (via une règle firewall) peut être nécessaire pour une partie du TP.

Exemples de scripts terraform : https://bitbucket.org/olevitt/formations/src/master/kubernetes/terraform/docker  

<b>Option local</b>  

* Linux conseillé. MAC OS / Windows ok.

----

## TP 1 : Hello docker
<!-- .slide: class="slide" -->  

* Installer docker  
* Valider l'installation de Docker en lançant ```hello-world```
* Lancer un conteneur `nginx` (https://hub.docker.com/_/nginx)  
* Faire une requête HTTP sur le serveur `nginx`  
* Se connecter à chaud dans le `nginx`, supprimer le fichier /usr/share/nginx/html/index.html  
* Refaire une requête HTTP sur le serveur `nginx` : constater  
* Tuer le conteneur, le recréer, refaire une requête

----


## TP 2 : Mon image Docker
<!-- .slide: class="slide" -->  

* Créer une image Docker, en y mettant ce qui vous fait plaisir  
* Lancer des conteneurs de cette image  
* Créer un compte sur Dockerhub ou demander à quelqu'un un registre docker public anonyme (cf TP3)  
* Publier votre image  
* Diffuser le nom de votre image et épatez vos amis !  

----


## TP 3 : Aller plus loin
<!-- .slide: class="slide" -->  

* En utilisant l'image [registry](https://hub.docker.com/_/registry), mettre à disposition un registre public  
* Jeter un oeil aux volumes Docker : https://docs.docker.com/storage/volumes/
* En se baladant sur [Dockerhub](https://hub.docker.com), lancer les conteneurs de vos logiciels préférés  
* Exemples : ([VSCode](https://hub.docker.com/r/codercom/code-server), [Infinite mario](https://hub.docker.com/r/pengbai/docker-supermario/) ...)
* Jeter un oeil à [Dockercraft](https://github.com/docker/dockercraft), pour le fun  