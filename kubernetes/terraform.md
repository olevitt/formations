# Bonus : Terraform
<!-- .slide: class="slide" -->  

----

## C'est quoi ?
<!-- .slide: class="slide" -->  

* Infrastructure as code
* Hashicorp (2014)  
* Opensource (MPL-2.0)  
* https://github.com/hashicorp/terraform  

----

## Démo
<!-- .slide: class="slide" -->  

Exemples disponibles dans le dossier terraform  

----

## Tutoriel  
<!-- .slide: class="slide" -->  

https://cloud.google.com/community/tutorials/getting-started-on-gcp-with-terraform

