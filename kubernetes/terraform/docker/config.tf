
provider "google" {
  credentials = file("~/.gcp/account.json")
  project     = "lebacasable"
  region      = "europe-west1"
}
