resource "google_compute_firewall" "default" {
  name    = "allow-8000"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["8000"]
  }
}
