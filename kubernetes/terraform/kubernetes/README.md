# Terraform Kubernetes GKE  

Quelques scripts terraform pour déployer un petit cluster basique sur GKE.  
Des scripts plus poussés sont disponibles sur  
* https://www.terraform.io/docs/providers/google/r/container_cluster.html
* https://github.com/InseeFrLab/cloud-scripts/blob/master/gke