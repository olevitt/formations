# Terraform Kubernetes OVH

Quelques scripts terraform pour déployer un petit cluster basique sur OVHCloud.

- Créer une clé d'API en suivant https://docs.ovh.com/gb/en/api/first-steps-with-ovh-api/
- Complèter les credentials dans `conf.tf`
- Modifier l'id du projet dans `cluster.tf`

```
terraform init -upgrade
terraform apply
```
