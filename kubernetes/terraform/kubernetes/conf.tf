# Configure the OVHcloud Provider
# cf https://docs.ovh.com/gb/en/api/first-steps-with-ovh-api/
provider "ovh" {
  endpoint           = "ovh-eu"
  application_key    = "???"
  application_secret = "???"
  consumer_key       = "???"
}