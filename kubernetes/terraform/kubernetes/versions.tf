terraform {
  required_providers {
    ovh = {
      source = "ovh/ovh"
    }
  }
  required_version = ">= 0.13"
}
