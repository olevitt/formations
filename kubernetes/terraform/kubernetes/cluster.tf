# cf https://registry.terraform.io/providers/ovh/ovh/latest/docs/resources/cloud_project_kube

resource "ovh_cloud_project_kube" "mykube" {
   service_name = "???" # project id
   name         = "my_kube_cluster"
   region       = "GRA7"
}

resource "ovh_cloud_project_kube_nodepool" "pool" {
   service_name  = "???" # project id
   kube_id       = ovh_cloud_project_kube.mykube.id
   name          = "my_pool"
   flavor_name   = "D2-4"
   desired_nodes = 1
   max_nodes     = 1
   min_nodes     = 1
}