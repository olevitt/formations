## Objectif

<!-- .slide: class="slide" -->

- Découvrir ou redécouvrir la conteneurisation
- Kiffer
- Prise en main de Docker
- Kiffer
- Découvrir l'orchestration de conteneurs
- Kiffer
- Prise en main de Kubernetes
- Kiffer

---

## Le plan

<!-- .slide: class="slide" -->

- Petite histoire de la production informatique
- Conteneurisation
- Docker
- Orchestration de conteneurs
- Prise en main d'un cluster Kubernetes (managé)

Un cours dont vous êtes les héros :

- Installation d'un cluster Kubernetes (sur VM)  
- Utilisation avancée de Kubernetes
- Découverte terraform  
- ...

---

## Environnement de travail

<!-- .slide: class="slide" -->

- Option Cloud (compte fourni ou compte perso)
- Option Local (Linux de préférence, Mac / Windows sinon)
