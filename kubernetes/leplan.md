## Objectif
<!-- .slide: class="slide" -->  

* Découvrir la conteneurisation et l'orchestration de conteneurs  
* Prendre en main Docker et Kubernetes  
* Kiffer  
* Kiffer  
* Kiffer  
* Kiffer  
* Kiffer

----

## Le plan
<!-- .slide: class="slide" -->  

* Petite histoire de la production informatique
* Conteneurisation  
* Docker  
* Orchestration de conteneurs  
* Kubernetes  

Bonus :  
* Cloud  
* Terraform

----

## Environnement de travail
<!-- .slide: class="slide" -->  

* Option Cloud (compte fourni ou compte perso)
* Option Local (Linux de préférence, Mac / Windows sinon)

