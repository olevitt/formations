# 2. TP Client
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

Le TP client s’appuye sur le serveur mais ne doit pas avoir de dépendance directe avec lui (à l’exception
de l’URL du service).  
Parce qu’on n’a pas vraiment le choix, les projets serveur et client vont s’éxecuter sur la même machine (votre
poste de développeur). On se retiendra cependant d’utiliser des raccourcis profitant de ce fait.

----

## 2.0 : Setup

----

### 2.0 : Setup
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Créer un nouveau projet maven en choisissant “skip archetype” et packaging jar
* Ce projet ne doit pas avoir de dépendance directe avec le projet du serveur et réciproquement
* Passer en Java 8 en utilisant les properties suivantes :
```XML
<properties>
 <maven.compiler.source>1.8</maven.compiler.source>
 <maven.compiler.target>1.8</maven.compiler.target>
</properties>
```

----

## 2.1 : Le web artisanal

----

### 2.1 : Le web artisanal
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Créer une méthode main et faire une requête HTTP (sans utiliser de bibliothèque) vers la servlet
Serveur
 * Afficher le résultat dans la console

----

## 2.2 : Le web industriel

----

### 2.2 :
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Choisir le client HTTP qui vous fait le plus rêver. Par exemple, on
  pourra utiliser [OkHTTP](http://square.github.io/okhttp/),
  [Unirest](http://unirest.io/java.html),
  [Jersey client](https://jersey.github.io/),
  [Apache HTTPClient](https://hc.apache.org/httpcomponents-client-4.5.x/index.html)
 * Ajouter le client choisi au projet
 * L'utiliser pour faire une requête GET sur /jersey/agent et afficher le résultat

----

## 2.3 : Un vrai format de données

----

### 2.3 : Un vrai format de données
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Requêter la ressource exposant un Agent en JSON / XML et afficher le résultat
 * Créer le Pojo Agent
 * Mapper automatiquement le résultat vers une instance d’Agent
 * Utiliser la content negociation pour obtenir au choix du JSON ou du XML

----

## 2.4 : Envoi de paramètres

----

### 2.4 : Envoi de paramètres
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Récupérer l’agent dont l’idep est F2WBNP
 * Récupérer la liste des agents de catégorie A
 * Créer une instance d’Agent et l’envoyer vers le Serveur

----

## 2.5 : Personnalisation des retours

----

### 2.5 : Personnalisation des retours
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Améliorer la résilience du code pour prendre en compte le cas où aucun agent n’existe pour l’idep
demandé
* Améliorer la résilience du code pour prendre en compte le cas où les paramètres envoyés ne sont pas
valides (ex : idep de plus de 6 caractères)
* Bonus : améliorer la résilience du code pour prendre en compte les erreurs serveur

----

## 2.6 : Prêt à publier ?

----

## 2.6 : Prêt à publier ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Utiliser le contrat swagger.json pour générer les classes objet (voire même le client complet)

----

## 2.7 : Bonus

----

### 2.7 : Bonus
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
A faire en fonction de ce qui a été implémenté côté serveur
 * Accéder aux ressources en HTTPS
 * Envoyer un login / mdp en authentification BASIC
 * Ecrire un client Javascript (JQuery ou react+redux) accédant aux ressources
 * Utiliser l'asynchrone pour faire plusieurs requêtes en même temps
