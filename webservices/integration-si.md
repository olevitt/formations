# 5. Intégration au SI
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Conventions

----

### Conventions
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Utilisation des verbes
 * Singulier ou pluriel ?
 * Numéro de version ?  
  
Note de la cellule architecture

----

## Documentation

----

### Importance de la documentation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Contrat clair
 * Seule source de couplage
 * Au service des utilisateurs
 * Publication de clients exemples / bibliothèques ?

----

### Swagger
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
  * Description JSON du service (swagger.json)
  * Standard OpenAPI Specification (OAS) depuis la 3.0
  * Contrat
  * Multitudes d'outils ([swagger-ui](https://github.com/swagger-api/swagger-ui), [swagger-editor](https://editor.swagger.io))

----

### Swagger : exemple
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
http://petstore.swagger.io/v2/swagger.json  


----

### Swagger-core : pour générer swagger.json
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Outil générant swagger.json
 * Version 2 : génère du swagger 3 (OAS)
 * Version 1.5.16 : génère du swagger 2
 * Plugins pour les frameworks  
 * Annotations supplémentaires (@API)  

----

### Swagger-core setup
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
https://github.com/swagger-api/swagger-core/wiki/Swagger-Core-Jersey-2.X-Project-Setup-1.5
```XML
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-jersey2-jaxrs</artifactId>
    <version>1.5.16</version>
</dependency>
```
(inclu)

```XML
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-core</artifactId>
    <version>1.5.16</version>
</dependency>
```


----

### Swagger-core setup
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```XML
<servlet>
  <servlet-name>jersey</servlet-name>
  <servlet-class>org.glassfish.jersey.servlet.ServletContainer</servlet-class>
  <init-param>
    <param-name>jersey.config.server.provider.packages</param-name>
    <param-value>
        io.swagger.jaxrs.listing,
        fully.qualified.package.name.mesRessources
    </param-value>
  </init-param>
  <load-on-startup>1</load-on-startup>
</servlet>

<servlet-mapping>
    <servlet-name>jersey</servlet-name>
    <url-pattern>/api/*</url-pattern>
</servlet-mapping>
```
<!-- .element: class="stretch" -->

----

### Swagger-core setup
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Bootstrap extends HttpServlet {
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("/api");
        beanConfig.setResourcePackage("mes.resources");
        beanConfig.setScan(true);
    }
}
```
```XML
<servlet>
  <servlet-name>SwaggerBootstrap</servlet-name>
  <servlet-class>monpackage.Bootstrap</servlet-class>
  <load-on-startup>2</load-on-startup>
</servlet>
```

----

### Swagger-ui
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Documentation depuis le code
 * API testable  
Exemple : http://petstore.swagger.io/  
![](images/swagger.png)

----

### Swagger-ui : setup
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
https://swagger.io/docs/swagger-tools/#swagger-ui-documentation-29  
 * Télécharger / cloner https://github.com/swagger-api/swagger-ui  
 * Lancer dist/index.html ou inclure dist à un projet
 * Changer l'URL du swagger.json utilisé
 * ???
 * Profit

----

## Portails de services

![](images/travaux.jpg)

----

## Tests

----

### Problématique des tests unitaires
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Un test unitaire doı̂t être :
 * Isolé
 * Portable
 * Reproductible
 * Léger
Sinon, tests d’intégration

----

### Outils Jersey
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class OrderServiceTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new ResourceConfig(OrderService.class);
    }

    @Test
    public void ordersPathParamTest() {
        String response = target("orders/453").request().get(String.class);
        Assert.assertTrue("orderId: 453".equals(response));
    }

    @Test
    public void ordersFixedPathTest() {
        String response = target("orders/summary").request().get(String.class);
        Assert.assertTrue("orders summary".equals(response));
    }
}
```
<!-- .element: class="stretch" -->

----

<!-- .slide: class="no-toc-progress" -->
![](images/miaouh.jpg)
<!-- .element height="20%" width="50%" -->

----

### TP
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Fin :)
