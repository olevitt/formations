# 1. TP Serveur
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

Pour le TP serveur, on simulera à la volée les données quand ça sera nécessaire.  
L’ajout d’un système de persistance des données (fichiers, base SQL . . .) étant hors du champ de cette
formation, on considérera que le fait d’afficher le toString d’un objet dans la console du serveur équivaut à
sa persistance.

----

## 1.0 : Setup

----

### 1.0 : Setup
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Créer un nouveau projet maven en choisissant “skip archetype” et packaging war
* Passer en Java 8 en utilisant les properties suivantes :
```XML
<properties>
 <maven.compiler.source>1.8</maven.compiler.source>
 <maven.compiler.target>1.8</maven.compiler.target>
</properties>
```

----

## 1.1 : Le web artisanal

----

### 1.1 : Le web artisanal
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* Ajouter la dépendance vers la dernière version (non bêta) de servlet-api en précisant le bon scope
* Créer une servlet répondant "Hello servlet" aux requêtes GET
* Créer le fichier web.xml et mapper toutes les url de la forme /servlets/* vers la servlet
* Tester depuis un navigateur
* Bonus : tester depuis d’autres outils

----

## 1.2 : Le web industriel

----

### 1.2 : Le web industriel
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

 * Ajouter la dépendance vers Jersey 2
 * Créer une ressource AgentRessource sur /agent
 * Y exposer une méthode répondant “Hello Jersey” aux requêtes GET sur /hello
 * Mapper toutes les URL de la forme /jersey/* vers Jersey
 * Tester depuis un navigateur

----

## 1.3 : Un vrai format de données

----

### 1.3 : Un vrai format de données
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Ajouter le support JSON
* Créer un Pojo Agent (avec au moins les données idep et grade)
* Créer un endpoint renvoyant un Agent aux formats JSON et XML

----

## 1.4 : Récupération des paramètres

----

### 1.4 : Récupération des paramètres
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Exposer une méthode renvoyant l’Agent correspondant à un idep donné
 * Exposer une méthode renvoyant la liste des Agent correspondant à un filtre (on peut filtrer sur la
catégorie et / ou sur l’idep)
 * Exposer une méthode permettant d’enregistrer un nouvel Agent (on se contentera d’afficher l’agent
dans la console du serveur)

----

## 1.5 : Personnalisation des retours

----

### 1.5 : Personnalisation des retours
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Si aucun Agent n’existe pour un idep, renvoyer une réponse utilisant le code HTTP approprié
 * Si l’idep ne fait pas 6 caractères, renvoyer une réponse utilisant le code HTTP approprié
 * Bonus : Personnaliser le contenu renvoyé en cas d'erreur lors d'un traitement

----

## 1.6 : Prêt à publier ?

----

## 1.6 : Prêt à publier ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Exposer la documentation sous forme de swagger.json
 * Enrichier la documentation à l'aide des annotations swagger
 * Intégrer un swagger-ui dans l'application

----

## 1.7 : Bonus

----

### 1.7 : Bonus
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Activer l’authentification basic
 * Bonus : Ecrire une implémentation applicative de l'authentification basic
 * Activer CORS
 * Ecrire un test unitaire Jersey
