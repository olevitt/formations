# 1. De quoi on parle ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Web ?

----

### Le web ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
- Un protocole parmis d’autres : HTTP(S)
- Des ports standards
- Client / Serveur
- Mode déconnécté  
- 1 requête, 1 réponse
- [RFC 2616](https://www.ietf.org/rfc/rfc2616.txt)

----

### Anatomie du protocole HTTP
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](images/client-server.png)

----

### La requête
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```
GET /flights HTTP/1.1
Host: www.google.com
Referer: http://google.com
User−Agent: Mozilla/5.0 (X11; Linux x86_64) [...]
Cookie: yummy_cookie=choco; tasty_cookie=strawberry
Cle: valeur
```
```
POST / HTTP/1.1
Host: foo.com
Content−Type: application/x−www−form−urlencoded
Content−Length: 13
say=Hi&to=Mom
```
* Un verbe
* Des headers
* [Un contenu]

----

### La réponse
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

```Java
HTTP/1.1 200 OK
Date: Fri, 31 Dec 1999 23:59:59 GMT
Server: Apache/0.8.4
Content−Type: text/html
Content−Length: 59
Expires: Sat, 01 Jan 2000 00:59:59 GMT
Last−modified: Fri, 09 Aug 1996 14:21:40 GMT
Set−Cookie: sessionid=38afes7a8; httponly; Path=/

<TITLE>Exemple</TITLE>
<P>Ceci est une page dexemple.</P>
```

* Un code retour
* Des headers
* [Un contenu]

----

### Démotime
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

 * Démo brute (nc / telnet)
 * Démo wget
 * Démo navigateur
 * Démo plugin firefox / chrome  

----

## Services ?

----

### Exposer des données et des actions
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Lecture
 * Ecriture  
Donc un site web, non ?

----

### Caractéristiques
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * A destination d’applications
 * Données uniquement
 * Opérations unitaires
 * Contrat clair
 * Couplage faible

----

## Architecture

----

### Architecture "legacy"
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](images/archi-legacy.png)

----

### Architecture webservices
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](images/archi-webservices.png)

----

### Architecture microservices
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](images/archi-microservices.png)

----

## SOAP, REST, RESTful

----

### SOAP
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Legacy  

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Representational state transfer
 * Roy Fielding, 2000

Objectifs : performance, scalability, simplicity, modifiability,
 visibility, portability, and reliability

 Contraintes :
* Client - server
* Stateless
* Cacheable
* Layered system
* Uniform interface
* Code on demand (optionnel)
