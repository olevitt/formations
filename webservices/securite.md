# 4. Sécurité
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Authentification

----

### Les problématiques
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Qui authentifier ?
 * Fédération d'identité ?
 * Session ?

----

### Authentification BASIC
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](images/auth-basic.png)
 * Headers et code retour
 * Authorization: Basic base64(username:password)

----

### Mise en place (exemple)
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```XML
<web-app>
   <security-constraint>
      <web-resource-collection>
         <web-resource-name>API</web-resource-name>
         <url-pattern>/API/*</url-pattern>
         <http-method>GET,POST</http-method>
      </web-resource-collection>
      <auth-constraint>
         <role-name>admin</role-name>
      </auth-constraint>
    </security-constraint>

    <login-config>
        <auth-method>BASIC</auth-method>
        <realm-name>Zone sécurisée</realm-name>
    </login-config>

    <security-role>
        <role-name>admin</role-name>
    </security-role>
</web-app>
```
<!-- .element: class="stretch" -->

----

### OAuth / openid-connect
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Keycloak


----

## Confidentialité des échanges

----

### HTTPS
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](images/https.jpg)

----

### HTTPS, côté serveur
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```XML
<web-app>
 <security-constraint>
        <web-resource-collection>
            <web-resource-name>Tout</web-resource-name>
            <url-pattern>/*</url-pattern>
        </web-resource-collection>
        <user-data-constraint>
            <transport-guarantee>CONFIDENTIAL</transport-guarantee>
        </user-data-constraint>
    </security-constraint>
</web-app>
```
 * Par défaut : certificat autosigné
 * Utiliser un vrai certificat : dépend du servlet container

----

### HTTPS, côté client
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* "Transparent"
* Faire confiance ou non ?

----

## CORS

----

### Problématique
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Problématique Javascript / Ajax
* Contrainte de sécurité du navigateur
* Configuration côté serveur  

![](images/arch-cors.png)

----

### CORS, preflight request
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](images/cors-preflight.png)
* Verbe OPTIONS
* Access-Control-Request-Method / Access-Control-Request-Headers
* Access-Control-Allow-Origin / Access-Control-Allow-Methods / Access-Control-Allow-Headers
