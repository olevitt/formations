# 3. Aller plus loin
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Format de données

----

### Le problème
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```HTML
<div id=tdtopresas>
<i><b>1</b></i>
<a href="/160351/evt.htm" class=topresas>Edmond</a>
<i><b>2</b></i>
<a href="/163269/evt.htm" class=topresas>La dame blanche</a>
<i><b>3</b></i>
<a href="/185914/evt.htm" class=topresas>Le Cirque Eloize</a>
</div>
```

```JSON
{  
  "topresas" : [
    { "id" : 160351, "name" : "Edmond" },
    { "id" : 163269, "name" : "La dame blanche" },
    { "id" : 185914, "name" : "Le cirque Eloize" }
  ]
}
```
<!-- .element: class="fragment" -->

----

### XML, JSON
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```JSON
{  
  "topresas" : [
    { "id" : 160351, "name" : "Edmond" },
    { "id" : 163269, "name" : "La dame blanche" },
    { "id" : 185914, "name" : "Le cirque Eloize" }
  ]
}
```

```XML
<topresas>
  <resa><id>160351</id><name>Edmond</name></resa>
  <resa><id>163269</id><name>La dame blanche</name></resa>
  <resa><id>185914</id><name>Le cirque Eloize</name></resa>
</topresas>
```

----

### Formats de données
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
  * MIME type (media / content type)  
text/plain, text/html, application/json, image/png ...
  * Header Content-Type
  * Content-Type: text/html; charset=utf-8
  * Header Accept dans la requête : content negociation

----

### En JAX-RS
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
@GET
@Produces(MediaType.TEXT_HTML)
public String doGetAsHtml() {
    return "<span>Toto</span>";
}
```

```Java
@GET
@Produces({MediaType.APPLICATION_JSON})
public String getObject() {
    return "{ \"nom\" :\"toto\", \"idep\" : \"F2WBNP\" }";
}
```
<!-- .element: class="fragment" -->
```Java
@GET
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public User getObject() {
    User user = new User();
    return user;
}
```
<!-- .element: class="fragment" -->

----

### (de)Serialization automatique JSON
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * GRAVE : MessageBodyWriter not found for media
type=application/json, type=class toto.Pojo,
genericType=class toto.Pojo
 * Dire à Jersey comment (de)serialiser
 * Choix du moteur : Moxy, Jackson, Jettison . . .
 * Autodiscovery
 * Support des annotations JAXB

----

### JSON : choix du moteur
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```XML
<dependency>
    <groupId>org.glassfish.jersey.media</groupId>
    <artifactId>jersey-media-json-jackson</artifactId>
    <version>2.25.1</version>
</dependency>
```

OU  

```XML
<dependency>
    <groupId>org.glassfish.jersey.media</groupId>
    <artifactId>jersey-media-moxy</artifactId>
    <version>2.25.1</version>
</dependency>
```

----

### (de)Serialization automatique XML
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * GRAVE: MessageBodyWriter not found for media
type=application/xml, type=class toto.Pojo,
genericType=class toto.Pojo
 * Spécification : JAXB
 * Configuration par annotations  

 ```Java
 @XmlRootElement
 public class Pojo {

 }
 ```

----

### TP !
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* [TP serveur](TP.html#/1) 1.3
* [TP client](TP.html#/2) 2.3

----

## Paramètres

----

### Injection de paramètres
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

 * @PathParam
 * @QueryParam
 * @MatrixParam
 * @FormParam
 * @HeaderParam
 * @CookieParam
 * @BeanParam
 * @Context

----

### @Pathparam
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Exemple : /user/123  
```Java
@GET
@Path("/user/{id}")
public User getUserById(@PathParam("id") int id) {
    return userService.findUserById(id);
}
```

Exemple : /user/123/2017  
```Java
@GET
@Path("/user/{id}/{year}")
public int getUserByIdAndYear(@PathParam("id") int id,
  @PathParam("year") int year) {
    return userService.findUserByIdAndYear(id,year);
}
```

----

### @QueryParam
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Exemple : /user?ageMin=20&ageMax=30
```Java
@GET
public List<User> getUsersByQuery(
    @QueryParam("ageMin") int ageMin,
    @QueryParam("ageMax") int ageMax) {
      return userService.getUsersByQuery(ageMin,ageMax);
}
```

DefaultValue
```Java
@GET
public List<User> getUserByQuery(
  @DefaultValue("18") @QueryParam("ageMin") int ageMin,
  @DefaultValue("65") @QueryParam("ageMax") int ageMax) {
    return userService.getUsersByQuery(ageMin,ageMax);
}
```

----

### Contenu (Body)
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

En provenance d’un formulaire HTML  
(application/x-www-form-urlencoded)

```Java
@POST
public void createUser(@FormParam("name") String name,
                  @FormParam("age") String age) {
    userService.createUser(new User(name,age));
}
```  
Autres Content-Type  

```Java
@POST
@Consumes(MediaType.APPLICATION_JSON)
public void createUser(User user) {
    userService.createUser(user);
}
```

----

### TP !
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* [TP serveur](TP.html#/1) 1.4
* [TP client](TP.html#/2) 2.4

----

## Retours

----

### Rappels HTTP  
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Une réponse =
  * Un code retour
  * Des headers
  * [Un contenu]

----

### Codes réponse HTTP (=statut)  
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * 2xx : succès
  * 200 : OK
 * 3xx : redirection
  * 301 : Moved Permanently
 * 4xx : erreur client
  * 400 : Bad request
  * 401 : Unauthorized
  * 404 : Not found
 * 5xx : erreur serveur  
  * 500 : Internal Server Error

----

### Réponses personnalisées
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Quand tout va bien : 200  
```Java
@GET
public User getUser() {
  return user;
}
```
404 custom :
```Java
@GET
@Path("/user/{id}")
@Produces(MediaType.APPLICATION_JSON)
public Response get(@PathParam("id") Long id) {
    User user = userDao.findById(id);
    if (user != null) {
        return Response.ok(user).build();
    }
    return Response.status(Status.NOT_FOUND).build();
}
```

----

### Headers réponse
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
@GET
@Produces(MediaType.APPLICATION_JSON)
public User getUser(
    @Context HttpHeaders requestHeaders,
    @Context HttpServletResponse response) {
        response.setHeader("key", "value");
        return user;
}
```

* /!\ D'autres sources de headers /!\

----

### TP !
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * [TP serveur](TP.html#/1) 1.5
 * [TP client](TP.html#/2) 2.5
