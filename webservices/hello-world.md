# 2. Hello world
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Mon premier Webservice

----

### Objectifs du TP fil rouge
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
- Webservice RH
- Accéder aux infos d’un agent
- Accéder aux infos de tous les agents
- Ajouter un agent
- Supprimer un agent

----

### Zoom sur le serveur
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](images/client-server.png)

----

### Zoom sur le serveur
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](images/zoom-serveur.png)

----

### Faire du web Java ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
- servlet-api
- v<=2.5 : javax.servlet servlet-api
- v>=3.0.1 : java.servlet javax.servlet-api
- Scope : provided

----

### L’outil de base : (HTTP)Servlet
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
[https://docs.oracle.com/javaee/7/api/javax/servlet/
http/HttpServlet.html](https://docs.oracle.com/javaee/7/api/javax/servlet/
http/HttpServlet.html)
![](images/javadoc-servlet.png)

----

### Exemple de servlet
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class HelloWorld extends HttpServlet {
  public void init() throws ServletException
  {

  }

  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
            throws ServletException, IOException
  {
      response.setContentType("text/html");
      PrintWriter out = response.getWriter();
      out.println("<h1>Hello</h1>");
  }

  public void destroy()
  {

  }
}
```
<!-- .element: class="stretch" -->

----

### Enregistrement de la servlet : web.xml
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * src/main/webapp/WEB-INF/web.xml
 * Deployement descriptor
```XML
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
http://xmlns.jcp.org/xml/ns/javaee/web-app 3 1.xsd"
version="3.1">
<servlet>
<servlet-name>HelloWorld</servlet-name>
<servlet-class>fully.qualified.package.name.HelloWorld</servlet-class>
</servlet>
<servlet-mapping>
<servlet-name>HelloWorld</servlet-name>
<url-pattern>/*</url-pattern>
</servlet-mapping>
</web-app>
```
<!-- .element: class="stretch" -->

----

### Les frameworks : choix des armes
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Struts 2
 * Spring Rest
 * Jersey 2
 * RESTEasy
 * ...  

Pour les webservices : une norme JAX-RS et des implémentations

----

### Jersey 2
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * https://github.com/jersey/jersey
 * https://jersey.github.io/
 * ! Jersey 1 !
 * En gros : un moteur de servlet
 * Implémentation de JAX-RS (2)

----

### Jersey 2, setup : la dépendance
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```XML
<dependency>
  <groupId>org.glassfish.jersey.containers</groupId>
  <artifactId>jersey-container-servlet</artifactId>
  <version>2.25.1</version>
</dependency>
```

----

### Jersey 2 : les ressources
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
package fully.qualified.package.name.mesRessources;

  @Path("helloworld")
  public class HelloWorldResource {

      @GET
      @Produces(MediaType.TEXT_PLAIN)
      public String sayHello() {
          return "Hello world !";
      }

}
```

----

### Jersey 2 : setup, l'enregistrement
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 ```XML
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
http://xmlns.jcp.org/xml/ns/javaee/web-app 3 1.xsd"
version="3.1">
<servlet>
<servlet-name>Jersey REST Service</servlet-name>
<servlet-class>org.glassfish.jersey.servlet.ServletContainer</servlet-class>
<init-param>
<param-name>jersey.config.server.provider.packages</param-name>
<param-value>fully.qualified.package.name.mesRessources</param-value>
</init-param>
</servlet>
<servlet-mapping>
<servlet-name>Jersey REST Service</servlet-name>
<url-pattern>/*</url-pattern>
</servlet-mapping>
</web-app>
```
<!-- .element: class="stretch" -->

----

### TP !
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * [TP serveur](TP.html#/1), 1.1 et 1.2

----

## Mon premier client

----

### Requêtes HTTP en Java
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * A la base : UrlConnection
 * Usuellement : des bibliothèques
 * Jersey client, Apache HTTPClient, Google HTTP Client, Unirest, OkHttp ...

----

### Java : exemple sans bibliothèque
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
GET
```Java
URL url = new URL("http://google.fr?key=value");
URLConnection connection = url.openConnection();
connection.setRequestProperty("Header", value);
InputStream response = connection.getInputStream();
```

POST
```Java
URLConnection connection = new URL("http://google.fr").openConnection();
connection.setRequestProperty("Header", value);
connection.setDoOutput(true);

OutputStream output = connection.getOutputStream();
output.write(content);

InputStream response = connection.getInputStream();
```

----

### Java : Unirest
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```XML
<dependency>
    <groupId>com.mashape.unirest</groupId>
    <artifactId>unirest-java</artifactId>
    <version>1.4.9</version>
</dependency>
```

```Java
HttpResponse<JsonNode> response =
  Unirest.post("http://httpbin.org/post")
         .queryString("name", "Mark")
         .field("last", "Polo")
         .asJson();  
System.out.println(response.body());
```

----

### Java : OkHTTP
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```XML
<dependency>
  <groupId>com.squareup.okhttp3</groupId>
  <artifactId>okhttp</artifactId>
  <version>3.8.0</version>
</dependency>
```

```Java
OkHttpClient client = new OkHttpClient();

Request request = new Request.Builder()
	.url("http://google.fr")
     .build();

Response response = client.newCall(request).execute();
System.out.println(response.body().string());
```

----

### Java : Jersey client
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```XML
<dependency>
    <groupId>org.glassfish.jersey.core</groupId>
    <artifactId>jersey-client</artifactId>
    <version>2.25.1</version>
</dependency>
```

```Java
Client client = ClientBuilder.newClient();

WebTarget target  = client.target("http://google.com");
WebTarget flightsTarget = target.path("flights");
Invocation.Builder invocationBuilder
  = flightsTarget.request(MediaType.APPLICATION.JSON);

MonPojo retour = invocationBuilder.get(MonPojo.class);
MonPojo retour = invocationBuilder.post(
  Entity.entity(queryPojo,MediaType.APPLICATION_JSON)
);

```  

----

### En Java : bilan
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Possible sans bibliothèque
 * Mais relou  
 Bonus selon les bibliothèques :  
 * Jolie syntaxe :)
 * Parsing automatique (JSON, XML . . . )
 * Asynchrone (Hourra !)
 * “Surcouches" (HTTPS, zippage . . . )
 * Gestion d’erreur / retry  
Et plein d’autres goodies

----

### En Javascript
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
XMLHttpRequest (XHR)  
```Javascript
var xhr = getXMLHttpRequest();
```
```Javascript
xhr.onreadystatechange = function() {
	if (xhr.readyState == 4
      && (xhr.status == 200 || xhr.status == 0)) {
		alert(xhr.responseText);
	}
};
```

```Javascript
xhr.open("GET", "target.html", true);
xhr.setRequestHeader("key","value");
xhr.send(null);
```

----

### En Javascript : JQuery
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 ```Javascript
 $.ajax({
   url: "target.html",
   cache: false,
   success: function(html){
     alert(html);
   }
 });
```

----

### En Javascript : Fetch
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 ```Javascript
 fetch('target.html')
  .then(function(response) {
    return response.text();
  })
  .then(function(text) {
    alert(text)
  });
```

----

### Et les autres langages ?
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * (Presque) Tout le monde sait faire !
 * Même SAS, Libre office ...

----

### TP !
  <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
  * [TP client](TP.html#/2), 2.1 et 2.2
