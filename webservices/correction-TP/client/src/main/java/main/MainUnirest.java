package main;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import core.Agent;

public class MainUnirest {

	public static void main(String[] args) throws Exception {
		getHello();
		
		ajouterSupportJson();
		
		agent();
		agentXML();
		agentByIdep("TOTOTO");
		agentsByFiltre(2);
		Agent agent = new Agent();
		agent.setCategorie(1);
		agent.setIdep("NEWNEW");
		sendAgent(agent);
		
		agentNotFound();
	}
	
	
	private static void getHello() throws UnirestException {
		System.out.println(Unirest.get(URLS.HELLO).asString().getBody());
	}
	
	private static void ajouterSupportJson() {
		Unirest.setObjectMapper(new ObjectMapper() {
            private Gson gson = new GsonBuilder().disableHtmlEscaping()
                    .create();

            @Override
            public <T> T readValue(String value, Class<T> valueType) {
                return gson.fromJson(value, valueType);
            }

            @Override
            public String writeValue(Object value) {
                return gson.toJson(value);
            }
        });
	}
	
	private static void agent() throws UnirestException {
		System.out.println(Unirest.get(URLS.AGENT).asJson().getBody());
		System.out.println(Unirest.get(URLS.AGENT).asObject(Agent.class).getBody());
	}
	
	private static void agentXML() throws UnirestException {
		System.out.println(Unirest.get(URLS.AGENT).header("Accept", "application/xml").asString().getBody());
		//TODO : mapping automatique XML
	}
	
	private static void agentByIdep(String idep) throws UnirestException {
		System.out.println(Unirest.get(URLS.AGENT_BY_IDEP.replace("{idep}", idep)).asJson().getBody());
		System.out.println(Unirest.get(URLS.AGENT_BY_IDEP.replace("{idep}", idep)).asObject(Agent.class).getBody());
	}
	
	private static void agentsByFiltre(int categorie) throws UnirestException {
		System.out.println(Unirest.get(URLS.AGENTS_BY_FILTRE).queryString("categorie", ""+categorie).asJson().getBody());	
	}
	
	private static void sendAgent(Agent agent) throws UnirestException {
		System.out.println(Unirest.post(URLS.ADD_AGENT).header("Content-type", "application/json").body(agent).asString().getBody());
	}
	
	private static void agentNotFound() throws UnirestException {
		HttpResponse<JsonNode> response = Unirest.get(URLS.AGENT_BY_IDEP.replace("{idep}", "XXXXXX")).asJson();
		switch (response.getStatus()) {
		case 404:
			System.out.println("Agent introuvable");
			break;

		default:
			System.out.println(response.getBody());
			break;
		}
	}
	

}
