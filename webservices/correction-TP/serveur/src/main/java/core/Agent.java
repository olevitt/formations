package core;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Agent {

	public static final int CATEGORIE_A = 1, CATEGORIE_B = 2, CATEGORIE_C = 3;
	String idep;
	int categorie;
	
	public String getIdep() {
		return idep;
	}
	public void setIdep(String idep) {
		this.idep = idep;
	}
	public int getCategorie() {
		return categorie;
	}
	public void setCategorie(int categorie) {
		this.categorie = categorie;
	}
	
	
}
