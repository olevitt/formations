
# Bien tester
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->


----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

## Quoi tester ?
* Public, private, protected ?
* **Le contrat fait foi**
* Couverture des tests
* Getters / Setters ?
* Service, DAO ?
* Code legacy ?
* Test first ?

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Rentabiliser les tests

* Intégration avec la MOA
* Autodocumentation
* Passage en maintenance
* Changement d'environnement
* PIC

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Outillage

* Intégration dans Eclipse
* Plugins
* Alertes
* Démo ! (EclEmma, Infinitest, Moreunit)

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## TP !
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

[4- Bien tester](TP.html#/4)
