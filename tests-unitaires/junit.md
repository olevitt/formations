
# JUnit
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->


----



## C'est quoi ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Un framework de tests unitaires pour Java  

Des alternatives ?
* testNG  
Quelle version ?
* 3 ?
* 4 ?
* 5 ?

----

## Mise en place
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```XML
<dependency>
	<groupId>junit</groupId>
	<artifactId>junit</artifactId>
	<version>4.12</version>
	<scope>test</scope>
</dependency>
```  

Scope test ?

----

## Configuration
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Hello world
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
import static org.junit.Assert.assertEquals;
 import org.junit.Test;

 public class ClasseATesterTest {
   @Test
   public void testRecupererDebutNumeroSS() {
      // GIVEN
      Individu monIndividu = new Individu();
      monIndividu.setAnneeNaissance("1983");
      monIndividu.setDeptNaissance("75");
      monIndividu.setMoisNaissance("01");
      monIndividu.setArrondissementNaissance("014");
      monIndividu.setCle("03");
      monIndividu.setSexe("m");
      monIndividu.setNumeroNaissance("004");
      ClasseATester maClasse = new ClasseATester();

      // WHEN
      String retour = maClasse.recupererDebutNumeroSS(monIndividu);

      // THEN
      assertEquals("183017411400403", retour);
   }
}
```
Où écrire ce code ?

----

## Hello world
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](img/runas.png)
![](img/resulttest.png)

----

## Utilisation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* Le dossier src/test
* Conventions
* Given, when, then
* C'est quoi un import static ?
* 1 assert = 1 test
* Assert.assertEquals (["message erreur"], resultatAttendu, resultatReel)
* Assert.assertTrue (["message erreur"], expression)
* fail()

----

## Utilisation avancée : les hooks
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class ClasseATesterTest {
  @BeforeClass
  public static void beforeClass() {
    //Une seule fois au debut
  }

  @Before
  public void before() {
    //Avant chaque test
  }

  @After
  public void after() {
    //Apres chaque test
  }

  @AfterClass
  public static void afterClass() {
    //Une seule fois a la fin
  }

  @Test
  public void testMethode() {

  }
}
```

----

## Utilisation avancée : les exceptions
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class ClasseATesterTest {

  @Test(expected = IllegalArgumentException.class)
  public void testExplosif() throws Exception {
     racineCarree(-10);
  }
}
```
Alternative :(  
```Java
public class ClasseATesterTest {

  @Test
  public void testExplosif() {
     try {
       racineCarree(-10);
       fail();
     }
     catch (IllegalArgumentException e) {
       assertEquals("Nombre inferieur a 0", e.getMessage());
     }
  }
}
```

----

## TP !
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

[1- JUnit](TP.html#/1)
