
# DBUnit
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->


----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Previously on "les tests unitaires"

```Java
public class ClasseATesterTest {
	@Test
	public void testRecupererDebutNumeroSS() {
		 // GIVEN
		 Individu monIndividu = new Individu();
		 monIndividu.setAnneeNaissance("1983");
		 monIndividu.setDeptNaissance("75");
		 monIndividu.setMoisNaissance("01");
		 monIndividu.setArrondissementNaissance("014");
		 monIndividu.setCle("03");
		 monIndividu.setSexe("m");
		 monIndividu.setNumeroNaissance("004");
		 ClasseATester maClasse = new ClasseATester();

		 // WHEN
		 String retour = maClasse.recupererDebutNumeroSS(monIndividu);

		 // THEN
		 assertEquals("183017411400403", retour);
	}
}
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

## Nouvelle problématique
* Notre méthode touche à la base de données
* Par exemple, elle désactive un compte

```Java
public void desactiverCompte(String identifiant) {
	   PreparedStatement ps = connection.prepareStatement("UPDATE utilisateurs SET
	   actif = 0 WHERE identifiant = ?");
	   ps.setString(1,identifiant);
	   ps.executeUpdate();
	   ps.close();
}
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## 1ère approche

```Java
public class ClasseATesterTest {
	@Test
	public void testDesactiverCompte() {
		 // GIVEN

		 // WHEN
		 desactiverCompte("ABCDEF");

		 // THEN
		 assertFalse("Le compte est maintenant inactif", isActif("ABCDEF"));
	}
}
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## 2ème approche

```Java
public class ClasseATesterTest {
	@Test
	public void testDesactiverCompte() {
		 // GIVEN

		 // WHEN
		 desactiverCompte("ABCDEF");

		 // THEN
		 assertFalse("Le compte est maintenant inactif", isActif("ABCDEF"));
	}

	@Before
	public void avantTests() {
		 activerTousLesComptes();
	}

	@After
	public void afterTests() {
		 activerTousLesComptes();
	}
}
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## C'est quoi les problèmes ?

* Pas de reproductibilité
* Pas d'isolation
* Travail en équipe ?
* Maintenabilité ?

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## DBUnit, notre sauveur ?

Extension de JUnit pour les projets "database-driven"  
Objectif : mise en place de la base entre chaque test.  
Bonus : vérification / comparaison de l'état de la base.  

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Mise en place

```XML
<dependency>
	<groupId>org.dbunit</groupId>
	<artifactId>dbunit</artifactId>
	<version>2.5.4</version>
	<scope>test</scope>
</dependency>
```  

Scope test ?

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Configuration

```XML
<dataset>
	<compte numero="1" montant="1000" auto_decouv="100" id_client="1"/>
	<compte numero="2" montant="300" auto_decouv="200" id_client="1"/>
	<compte numero="3" montant="1000" auto_decouv="100" id_client="1"/>
	<compte numero="5" montant="50" auto_decouv="0" id_client="9"/>
	<client id="1"  nom="Jean" prenom="Dupont"/>
	<compteConnu id_numero="5" id_client="1"/>
</dataset>
```
* Le dossier src/test/resources
* 1 fichier par ensemble de classes
* Le schéma est défini implicitement

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Utilisation

On prend une connexion et on en fait une connexion DBUnit :
```Java
DatabaseConnection myDbConnection = new DatabaseConnection(connection);
```
Ensuite on charge le XML :  
```Java
FlatXmlProducer producer = new FlatXmlProducer(new
InputSource("src/test/resources/fr/insee/DbUnit/dao/impl/CompteDAOTest.xml"));
IDataSet mySetUpDataset = new FlatXmlDataSet(producer);
```
Enfin on peut insérer les données :
```Java
DatabaseOperation.CLEAN_INSERT.execute(myDbConnection, mySetUpDataset);
```  
* Où placer ce code ?

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Utilisation avancée

* DBUnit ne crée pas les tables !
* NULL : ne rien mettre
* Sensible à la casse  

Quelques questions à se poser :  
* Reproduire tout le schéma ?
* Maintenir le schéma ?

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## L'isolation

DBUnit ne règle pas le problème.  
Les bases de données en mémoire, si :
* HSQLDB
* H2
* Apache derby
* SQLite  

Limitations :
* Limitations par moteur
* SQL non standard :(

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Exemple : HSQLDB
```XML
<dependency>
			<groupId>org.hsqldb</groupId>
			<artifactId>hsqldb</artifactId>
			<version>2.4.0</version>
			<scope>test</scope>
</dependency>
```

```
driver=org.hsqldb.jdbc.JDBCDriver
url=jdbc:hsqldb:mem:myDb
username=sa
password=sa
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Exemple : H2
```XML
<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<version>1.4.196</version>
			<scope>test</scope>
</dependency>
```

```
driver=org.h2.Driver
url=jdbc:h2:mem:myDb;DB_CLOSE_DELAY=-1
username=sa
password=sa
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Exemple : SQLite
```XML
<dependency>
			<groupId>org.xerial</groupId>
			<artifactId>sqlite-jdbc</artifactId>
			<version>3.20.1</version>
			<scope>test</scope>
</dependency>
```

```
driver=org.sqlite.JDBC
url=jdbc:sqlite::memory:myDb
username=sa
password=sa
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## TP !
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

[2- DBUnit](TP.html#/2)
