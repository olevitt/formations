<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

# Formation tests unitaires
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->

[Olivier Levitt](https://bitbucket.org/olevitt)  (olivier.levitt@gmail.com)

Online : [http://formations.levitt.fr/Tests-unitaires](http://formations.levitt.fr/tests-unitaires)  
Source : [https://bitbucket.org/olevitt/formations](https://bitbucket.org/olevitt/formations)  

[Enoncé du TP](TP.html)  
Corrigé [sur le dépôt git](https://bitbucket.org/olevitt/formations/src/master/tests-unitaires/correction-TP) (git clone ou interface web)


A partir du travail d’Emmanuel L’HOUR, Sébastien LICHTENAUER et Mélanie MARTIN  
Ecrit en [markdown][1] et propulsé par [reveal.js](http://lab.hakim.se/reveal-js/)
[1]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
