package core;

import static org.junit.Assert.*;

import org.junit.Test;

public class AgentTest {

	@Test
	public void testAgentIdepValide() throws Exception {
		//Given
		String idepOK = "ABCDEF";
		
		//When
		Agent agent = new Agent(idepOK);
		
		//Then
		assertEquals(agent.getIdep(),idepOK);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAgentIdepInvalide() throws Exception {
		//Given
		String idepNOK = "BC";
		
		//When
		new Agent(idepNOK);
		
		//Then
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAgentIdepNull() throws Exception {
		//Given
		String idepNOK = null;
		
		//When
		new Agent(idepNOK);
		
		//Then
	}

}
