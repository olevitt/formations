package dao;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlProducer;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.InputSource;

import core.Agent;
import utils.Utils;

public class AgentDAOTest {
	
	DatabaseConnection myDbConnection;

	@Test
	public void testFindAgentByIdep() throws Exception {
		//GIVEN 
		AgentDAO dao = new AgentDAO();
		String idep = "ABCDEF";
		
		//WHEN
		Agent agent = dao.findAgentByIdep(idep);
		
		//THEN
		assertEquals(idep, agent.getIdep());
	}
	
	@Before
	public void initDB() throws Exception {
		Connection connection = Utils.openConnection();
		connection.prepareStatement("CREATE TABLE if not exists agent (IDEP varchar PRIMARY KEY)").execute();
		myDbConnection = new DatabaseConnection(Utils.openConnection());
		FlatXmlProducer producer = new FlatXmlProducer(new
				InputSource("src/test/resources/data.xml"));
		IDataSet mySetUpDataset = new FlatXmlDataSet(producer);
		DatabaseOperation.CLEAN_INSERT.execute(myDbConnection, mySetUpDataset);
	}

	@After
	public void closeDB() throws Exception {
		myDbConnection.close();
	}

}
