package services;

import java.sql.SQLException;

import core.Agent;
import dao.AgentDAO;

public class AgentService {

	public boolean isIdepValide(String idep) {
		return idep != null && idep.length() == 6;
	}
	
	public boolean agentExiste(String idep) {
		try {
			Agent agent = new AgentDAO().findAgentByIdep(idep);
			return agent != null;
		}
		catch (SQLException e) {
			return false;
		}
	}
}
