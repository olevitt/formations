package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import core.Agent;
import utils.Utils;

public class AgentDAO {

	public Agent findAgentByIdep(String idep) throws SQLException {
		try {
			Connection conn = Utils.openConnection();
			PreparedStatement st = conn.prepareStatement("SELECT idep FROM agent WHERE idep = ?");
			st.setString(1, idep);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				return new Agent(rs.getString("idep"));
			}
			return null;
		}
		catch (Exception e) {
			throw new SQLException();
		}
	}
}
