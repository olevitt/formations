
# 2-DBUnit
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
Objectifs :
 * Ajout dans un projet d'un driver JDBC
 * Configuration en fonction des environnements
 * Ecriture d'une méthode à tester
 * Utiliser DBUnit pour préparer la base pour les tests

----

## Ajout dans le projet du driver JDBC postgresql
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Ajouter la dépendance vers la dernière version du driver JDBC postgresql
* Créer une classe Utils avec une méthode static ouvrant et renvoyant une connexion à la base postgresql
```Java
public static Connection openConnection() throws Exception {
		Class.forName("org.postgresql.Driver");
		String url = "jdbc:postgresql://localhost:5432/postgres";
		Connection conn = DriverManager.getConnection(url,"postgres", "postgres");
		return conn;
	}
```
<!-- .element: class="fragment" -->

----

## Configuration en fonction des environnements
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Créer un fichier properties.properties externalisant l'url, l'username, le password et le nom de la classe du driver
* Dans la classe Utils, écrire une méthode chargeant les properties de la base de données. On pourra pour ce faire utiliser le code suivant ou s'appuyer sur des bibliothèques comme commons configuration.
```Java
public static Properties loadProperties() throws IOException {
		Properties properties = new Properties();
		properties.load(DBUtils.class.getClassLoader().getResourceAsStream("properties.properties"));
    //String url = properties.getProperty("url");
		return properties;
}
```
* Ecrire un test vérifiant que les properties sont bien chargées (l'url est bonne).
* Modifier la méthode openConnection() pour qu'elle utilise ces properties

----

## Utilisation de la connexion
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Créer une classe AgentDAO
* Y implémenter une méthode findAgentByIdep(String idep)
Cette méthode renverra l'agent s'il existe, null s'il n'existe pas, SQLException si quelque chose se passe mal
* Documenter cette méthode
* Tester les cas nominaux de cette méthode
* Comment tester que la méthode renvoie bien une SQLException si quelque chose se passe mal ? Réponse au prochain chapitre :)
* Comment s'assurer des données présentes en base ?

----

## Utiliser DBUnit pour préparer la base pour les tests
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Ajouter la dépendance vers la dernière version de DBUnit
* Créer un jeu de données contenant 2/3 agents avec des ideps inventés
* Dans la bonne méthode du cycle de vie d'un test, demander à DBUnit de remplir la base de données

----

## Et l'isolation ?
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Réfléchir à l'isolation des tests
* Comment illustrer ce problème d'isolation ?
* Que faire pour le régler ?
* Choisir une base de données en mémoire (HSQLDB, H2, derby ou SQLite)
* L'utiliser pour les tests sans toucher au code
