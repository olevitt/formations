
# Mockito
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->


----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

## Nouvelle problématique
![](img/mockito.png)

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

## Mock
![](img/mock.png)


----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Comment ça marche ?
* Mock = objet simulé
* Reproduire le comportement de façon contrôlée
* Flexibilité
* Simulacre, émulateur, proxy, stub, dummy, fake, espion ?

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Le choix de la bibliothèque

* JMock
* Easymock
* Mockito  

A l'INSEE : Mockito

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Cas d'utilisation classiques

* Test UNITAIRE
* Math.random()
* new Date()
* DAO / SQLException

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Mise en place

```
<dependency>
    <groupId>org.mockito</groupId>
    <artifactId>mockito-core</artifactId>
    <version>2.11.0</version>
    <scope>test</scope>
</dependency>
```

Scope test ?

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Configuration

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Exemple, problématique
```
public class CompteServiceImpl implements ICompteService {

	   private ICompteDAO compteDAO;

	   public CompteServiceImpl() {
	        compteDAO = new CompteDAOImpl();
	   }

	   public void setCompteDAO(ICompteDAO compteDAO) {
	        this.compteDAO = compteDAO;
	   }

	   //Methode a tester
	   public int getSoldeTotal() {
	        List<Compte> comptes = compteDAO.getAll();
	        int soldeTotal = 0;
	        for (Compte compte : comptes) {
	             soldeTotal += compte.getSolde();
	        }
	        return soldeTotal;
	   }
}
```  
Comment tester getSoldeTotal ?

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Exemple, tentative
```Java
public class CompteServiceImplTest {

	private ICompteService compteService;

	@Test
	public void getSoldeTotalTest() {
		//GIVEN
		compteService = new CompteServiceImpl();

		//WHEN
		int solde = compteService.getSoldeTotal();

		//THEN
		assertEquals(25,solde);
	}
}
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Le manuel du petit moqueur
Etape 1 : créer le mock :  
```Java
ICompteDAO compteDAO = Mockito.mock(ICompteDAO.class);
```  
Etape 2 : personnaliser son comportement :  
```Java
Mockito.when(compteDAO.getAll()).thenReturn(listeComptes);
```  
Etape 3 : l'injecter ni vu ni connu :  
```Java
compteService.setCompteDAO(compteDAO);
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## On peut se mocker !
```Java
public class CompteServiceImplTest {

	private ICompteService compteService;

	@Test
	public void getSoldeTotalTest() {
		//GIVEN
		List<Compte> comptesDeTest = new ArrayList<Compte>();
		comptesDeTest.add(new Compte(20));
		comptesDeTest.add(new Compte(10));
		comptesDeTest.add(new Compte(-5));

		compteService = new CompteServiceImpl();

		ICompteDAO compteDAO = Mockito.mock(ICompteDAO.class);
		Mockito.when(compteDAO.getAll().thenReturn(comptesDeTest));
		compteService.setCompteDAO(compteDAO);

		//WHEN
		int solde = compteService.getSoldeTotal();

		//THEN
		assertEquals(25,solde);
	}
}
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Personnalisation du comportement
Par défaut : ne fait rien / ``nice values''  

```Java
Mockito.when(mockRandomService.random()).thenReturn(0.5);
```
```Java
Mockito.when(mockRandomService.randomInt(10)).thenReturn(5);
```
```Java
Mockito.when(mockFichierService.lireFichier()).thenThrow(new FileNotFoundException());
```
```Java
Mockito.when(mockRandomService.random()).thenCallRealMethod();  
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Les matchers
```Java
Mockito.when(mockRandomService.randomInt(6)).thenReturn(5);
```
```Java
Mockito.when(mockRandomService.randomInt(7)).thenReturn(5);
```
```Java
Mockito.when(mockRandomService.randomInt(8)).thenReturn(5);
```
```Java
Mockito.when(mockRandomService.randomInt(Matchers.anyInt())).thenReturn(5);
```

```Java
Mockito.when(mockRandomService.randomInt(2,5)).thenReturn(3);
```
```Java
Mockito.when(mockRandomService.randomInt(2,6)).thenReturn(3);
```
```Java
Mockito.when(mockRandomService.randomInt(2,7)).thenReturn(3);
```
```Java
Mockito.when(mockRandomService.randomInt(Matchers.anyInt(),Matchers.anyInt())).thenReturn(3);
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## D'autres goodies
**DISCLAIMER**
```Java
Mockito.verify(mockRandomService,times(2)).randomInt(Matchers.anyInt());
```
```Java
InOrder ordre = Mockito.inOrder(mockRandomService);
ordre.verify(mockRandomService).randomInt(2);
ordre.verify(mockRandomService).randomInt(5);
```
**DISCLAIMER**  
Powermock ?  
**DISCLAIMER**

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Injection de dépendances
```Java
public class ExempleDependant {

	   private IService service;

	   public ExempleDependant() {
	        service = new ServiceImpl();
	   }

	   public void methodeATester() {
	        service.whatever();
	   }
}
```

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Injection de dépendances, à la main
```Java
public class ExempleDependant {

	   private IService service;

	   public ExempleDependant() {
	        service = new ServiceImpl();
	   }

		 public ExempleDependant(IService service) {
	        //Constructeur utile aux tests
	        this.service = service;
	   }

	   public void setService(IService service) {
	        this.service = service;
	   }

		 public void methodeATester() {
	        service.whatever();
	   }
}
```
Ca fait envie ?

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Injection de dépendances, like a boss
Frameworks modernes, exemple : Spring  
```Java
public class ExempleNonDependant {

		 @Autowired
	   private IService service;

	   public ExempleNonDependant() {
	        service = new ServiceImpl();
	   }

		 public void methodeATester() {
	        service.whatever();
	   }
}
```
:)

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## TP !
[3- Mockito](TP.html#/3)
