
# Conception par les tests
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->


----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

## Un principe, des mouvances
* Tests first
* Only tests we trust  

Décliné en tous les sigles possibles :
* TDD
* BDD
* DDD
* ATDD
* TDR

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Un constat
![](img/couts.png)
<!-- .element height="70%" width="70%" -->

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Un constat
![](img/cycletdd.png)
![](img/cyclevert.png)

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## TDD
![](img/rgr.png)

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Récompenses multiples
* Documentation exhaustive et exécutable
* Confiance
* Qualité de code
* Couverture de test : 100%

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Plus loin, BDD / DDD
* Tester les comportements plutôt que les méthodes
* Discussion avec MOA
* Esprit boîte noire
* Unitaire ?
* Adieu le 100%

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Plus loin, TDR
* Specs pilotées par les tests
* Clarifie la spec
* Cas de test tout prêt
* Recette réduite

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Encore plus loin, ATDD
* Conception orientée par les tests fonctionnels
* Outils : Selenium, FitNesse, Cucumber
![](img/atdd.png)

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
## Encore plus loin, ATDD
![](img/doubleboucle.png)

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](img/miaouh.jpg)
