## Concepts de sécurité

Préalable à tout : architecture

<p class="fragment"><img src="img/archi.png" /></p>

---

## Côté client

- Confiance au client
- Navigateur : XSS / CRSF / CORS

---

## Client <> serveur :

- Sniff / MITM
- HTTPS
- (D)DOS

---

## Entrée serveur :

- Port scan
- Validation des inputs
- Authentification / authentification déléguée

---

## Serveur :

- Désérialisation

---

## Serveur <> BDD :

- Zone "safe"

---

## BDD :

- Injections SQL
