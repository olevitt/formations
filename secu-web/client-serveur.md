## Client <> serveur

<!-- .slide: class="slide" -->

* Zone untrust : sniff  
* HTTPS  
* D(DOS)

---

## Zone untrust

<!-- .slide: class="slide" -->

* Fake DNS  
* Interception 

Démo wireshark

---

## HTTPS

<!-- .slide: class="slide" -->

* HTTP + TLS (anciennement SSL)
* Confidentialité des échanges client <> serveur  
* Certificat signé par une autorité  
