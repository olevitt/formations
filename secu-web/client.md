## Côté client

<!-- .slide: class="slide" -->

- Confiance au client
- Spécial navigateur : XSS / CSRF / CORS

---

## Confiance au client

<!-- .slide: class="slide" -->

Règle d'or : 0 confiance au client

Festival de démos

---

## Sidequest : spécial navigateur

<!-- .slide: class="slide" -->

- XSS : https://owasp.org/www-community/attacks/xss/
- CRSF : https://owasp.org/www-community/attacks/csrf
- CORS : https://owasp.org/www-community/attacks/CORS_OriginHeaderScrutiny
