## Objectif

<!-- .slide: class="slide" -->

Monter en compétence sur les notions de sécurité applicatives web

---

## Organisation

<!-- .slide: class="slide" -->

- Revue des attaques potentielles
- Principe et théorie de l'attaque
- Mise en pratique

---

## Fils rouge

<!-- .slide: class="slide" -->

- Votre base de code
- Juice Shop : https://owasp.org/www-project-juice-shop/
