# TCP

----

## Le besoin
* Communiquer entre applications

----

## Le principe
* Ecouter un port
* Etablir une connexion 
* Une connexion = un socket 
* Tuyau bi-directionnel
* InputStream / OutputStream

----

## La classe Socket
```Java
Socket socket = new Socket("http://google.fr", 80);
InputStream input = socket.getInputStream();
OutputStream output= socket.getOutputStream();
socket.isConnected();
```
```Java
Socket socket = new Socket("45.32.1.82", 22);
```

----

## La classe ServerSocket
```Java
ServerSocket serverSocket = new ServerSocket(12345);
while (true) {
  Socket client = serverSocket.accept();
  System.out.println("Nouveau client connécté !")
}
```

----

## Application
* Créer un serveur
* Créer un client
* Connecter les deux
* Lire et traiter un CSV envoyé par un client
* Créer un serveur web (HTTP) basique