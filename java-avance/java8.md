# "Nouveautés" Java 8

----

## Le besoin
* Syntaxe ancienne
* Nouveaux paradigmes de programmation
* Efficacité / verbosité

----

## Le principe
* Nouvelles API et nouvelles syntaxes
* Nécessite JDK 8+
* 100% intéropérable

----

## default interfaces
```
public interface JSONable {
  public String toJSON();
}
```

```
public interface JSONable {
  public default String toJSON() {
    return "{ }";
  }
}
```

----

## Lambdas
```
public void jouerUnPeu() {
  new Thread(new Runnable() {
    System.out.println();
  });
}
```

```
public void jouerUnPeu() {
  new Thread(() -> System.out.println(""));
}
```

----

## Function pointers
```
Object::toString
```

----

## Streams

----

## Application
* Ajouter une méthode streamCSV qui renvoie un stream au lieu d'un iterator

----

## Aller plus loin
* Java 10 :)
