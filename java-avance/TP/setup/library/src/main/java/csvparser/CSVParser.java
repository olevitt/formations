package csvparser;

import java.io.InputStream;
import java.util.List;

public interface CSVParser {

	public List<String[]> parseStream(InputStream stream);
	
}
