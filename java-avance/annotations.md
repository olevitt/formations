# Annotations

----

## Le besoin
* Configuration (cc XML)
* Expressivité du code
* Séparation syntaxique
* Lien fort

----

## Le principe
* On préfixe classe, methode, champ, attribut, variable, package ...
* On peut paramétrer l'annotation
* L'annotation n'est pas porteuse de code
* Introspection

----

## Hello world
```
@Deprecated
@XmlRootElement(name="individu")
public class Individu {

}
```  

----

## Je fais mon annotation
```Java
public @interface Colonne {

}
```  

--

```Java
public class Individu {

  @Colonne
  private String nom;
}
```

----

## Je fais mon annotation, suite
```Java
@Target(value= {ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Colonne {

}
```  

* Target
* Retention : SOURCE / CLASS / RUNTIME

```Java
@Target(value= ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Colonne {

  public String nomColonne() default "";
  public boolean notNull() default false;
}
```  
```Java
public class Individu {

  @Colonne(nomColonne="nomIndividu", notNull=true)
  private String nom;
}
```

----

## Utilisation des annotations
```Java
Class clazz = Individu.class;
Annotation[] annotations = clazz.getAnnotations();
Colonne colonne = clazz.getAnnotation(Colonne.class);
if (colonne != null) {
  String nomColonne = colonne.nomColonne();
  boolean notNull = colonne.notNull();
}
```  

----

## Application : paramétrage du mapping
* Prendre en compte l'annotation standard javax @XmlTransient
* Créer une annotation ColonneCSV permettant de modifier le nom CSV de l'attribut

----

## Aller plus loin
* Jeter un oeil aux annotations JPA [https://github.com/javaee/jpa-spec](https://github.com/javaee/jpa-spec)
* Conditionner des traitements aux annotations (@notNull, @maxLength, @CAPSLOCK, @PostTraitement ...)
* Mettre en place un système d'autodiscovery (cc Jersey)
* Mettre en place un système d'injection de dépendances (cc Spring)
