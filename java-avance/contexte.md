# C'est quoi le plan ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Tout est là
[https://docs.oracle.com/javase/specs/](https://docs.oracle.com/javase/specs/)

----

## Une formation pratique et pratique
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Beaucoup de TP
* TP fil rouge, sujet simple
* Une notion : une mise en pratique
* L'important c'est le voyage, pas la destination
* Tout ce que vous n'avez jamais osé regarder
* On creuse ce que vous voulez

----

## Le TP fil rouge
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Lecteur CSV
* Bibliothèque
* Dynamique
* Modulaire
* Performant
* Extensible
* [Input CSV](input.csv)

----

## Les notions
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Initialisation d'un projet
* Flux
* Introspection (reflexivité)
* Types paramétrés (generics)
* Annotations
* Debug, profiling, mémoire
* Penser API
* Multithreading
* La classe Object
* Java 8, 9, 10 ...
* Gestion des exceptions
* TCP  
  
  

\+ Tout ce qu'on aura croisé en chemin
