<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

# Formation Java avancé
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->

[Olivier Levitt](https://bitbucket.org/olevitt)  (olivier.levitt@gmail.com)  
Thomas Pensec (thomas.pensec@insee.fr)

Online : [https://formations.levitt.fr/](http://formations.levitt.fr)  
Source : [https://bitbucket.org/olevitt/formations](https://bitbucket.org/olevitt/formations)  
TP : [Input CSV](input.csv)  

Ecrit en [markdown][1] et propulsé par [reveal.js](http://lab.hakim.se/reveal-js/)

[1]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
