# Types paramétrés (Generics)

----

## Le besoin
```Java
List v = new ArrayList();
v.add("test");
Integer i = (Integer) v.get(0);
```
* Résultat ?

----

## Le besoin
* Type check au compile time
* Eviter les casts
* Faire des classes / algorithmes génériques

----

## Le principe
* Classes ou méthodes paramétrées
* On manipule un type quelconque
* Paramètre et retour fonctions de ce type

----

## Cas classiques
```Java
public interface List<E> {
    void add(E x);
    Iterator<E> iterator();
}
```
```Java
public interface Comparator<T> {
  int compare(T o1, T o2);
}
```
```Java
public interface Map<K,V> {
    int size();
    V put(K key, V value);
}
```

----

## Un peu plus loin
```Java
public class BacASable {
  public static <T> T premierElement(List<T> liste) {
    return liste.get(0);
  }

  public static <T extends Comparable<T>> int countGreaterThan(T[] anArray, T elem) {
    int count = 0;
    for (T e : anArray)
        if (e.compareTo(elem) > 0)
            ++count;
    return count;
  }
}
```

----

## Application
```
public interface CSVParser<T> {
  public List<T> readCSV(InputStream is, Class<T> targetClass);
}
```  
ou  
```
public interface CSVParser {
  public <T> List<T> readCSV(InputStream is, Class<T> targetClass);
}
```

----

## Aller plus loin
* Leçon oracle sur les generics : [https://docs.oracle.com/javase/tutorial/java/generics/index.html](https://docs.oracle.com/javase/tutorial/java/generics/index.html)
* Type inference
* Wildcard
