# Debug, profiling, mémoire

----

## Le besoin
* Crash-test avec un fichier de 4Go

----

## Le principe
* Approche scientifique
* Un bon ouvrier = de bons outils

----

## Le mode debug
On en parle ?

----

## Un profiler : JVisualVM
C:\Program Files (x86)\insee\atelier-dev-2\applications\jdk18_64\jdk-1.8.0_40\bin\jvisualvm.exe
ou 
[https://visualvm.github.io/](https://visualvm.github.io/)
```
Démo
```  

----

## Un APM : Glowroot
[Glowroot](https://glowroot.org/)
```
Démo
```  

----

## La gestion de la mémoire
* Garbage collector
* Reference
* Weakreference


----

## Travail en flux
* List vs Stream  
* Exemple : streaming JDBC

----

## Application : streaming
```
public interface CSVParser<T> {
  public List<T> readCSV(InputStream is, Class<T> targetClass);
  public Iterator<T> streamCSV(InputStream is, Class<T> targetClass);
}
```  

----

## Aller plus loin
* Utiliser le profiler (ou sampler) de JVisualVM
* Explorer les alternatives à JVisualVM
