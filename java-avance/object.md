# La classe Object

----

## Le besoin
* Comportements universels
* Comportements par défaut

----

## Le principe
* La classe mère
* Implémentations par défaut
* Surcharges possibles

----

## Il y a quoi dans Object ?
* [JAVADOC](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html)

----

## Application
* Surcharger toString, hashCode, equals
* Surcharger finalize et en constater les effets

----

## Aller plus loin
