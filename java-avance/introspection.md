# Introspection

----

## Le besoin
* Besoin d'automatisation
* Besoin de généricité
* Comment eclipse fait l'autocomplétion ?
* Comment hibernate fait le mapping ?

----

## Le principe
* Récupérer la définition des classes
* Manipuler les attributs, méthodes, annotations

----

## Récupérer l'objet Class
```
Individu.class;
monIndividu.getClass();
Class.forName("fully.qualified.package.name.Individu");
```  
https://docs.oracle.com/javase/tutorial/reflect/class/classNew.html  

----

## En profiter
```
Class<Individu> clazz = Individu.class;
clazz.getName();
clazz.getPackage().getName();
Individu individu = clazz.newInstance();
Constructor[] cons = clazz.getDeclaredConstructors();
cons[0].newInstance("toto","titi",27);
```

----

## Profit : manipuler les fields
```Java
Class<Individu> clazz = Individu.class;
Field nom = clazz.getField("nom");
nom.getName();
nom.getType();
nom.get(toto);
nom.set(toto, "toto");
Field[] fields = clazz.getFields();
```

```
Field[] fields = clazz.getDeclaredFields();
Field password = clazz.getDeclaredField("password");
password.setAccessible(true);
password.get(toto);
password.set(toto, "toto");
```
<!-- .element: class="fragment" -->
```
Method[] method = clazz.getDeclaredMethods();
Method login = clazz.getDeclaredMethod("login");
login.setAccessible(true);
login.invoke(toto);
login.set(toto, "login","password");
```
<!-- .element: class="fragment" -->
https://docs.oracle.com/javase/tutorial/reflect/class/classMembers.html

----

## S'appuyer sur JavaBeans pour les getters / setters

Specification Java  
http://www.oracle.com/technetwork/articles/javaee/spec-136004.html  
Entre autres : les conventions de nommage  
```
BeanInfo beanInfo = Introspector.getBeanInfo(Individu.class);
PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
for (PropertyDescriptor propertyDescriptor : pds) {
  propertyDescriptor.getName();
  Method getter = propertyDescriptor.getReadMethod();
	Method setter = propertyDescriptor.getWriteMethod();
}
```

----

## Grand pouvoir = grandes responsabilités

* SecurityManager
* Java 9 / modules

----

## En vrai ? Des bibliothèques bien utiles
* [Apache commons-BeanUtils](http://commons.apache.org/proper/commons-beanutils/)
* [Reflections](https://github.com/ronmamo/reflections)
* BeanWrapper (Spring)

----

## Application : mapping automatique
```Java
public List<Object> readCSV(InputStream is, Class targetClass);  
```
* Test unitaire

----

## Aller plus loin
* Utilisation de la classe [Proxy](https://docs.oracle.com/javase/8/docs/api/java/lang/reflect/Proxy.html)
* Jeter un oeil au code source de [Reflections](https://github.com/ronmamo/reflections)
* Scan de package / scan de classpath
