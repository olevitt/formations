# Gestion des exceptions

----

## Le besoin
* Gérer les comportements non nominaux
* Résilience du code
* Expressivité des méthodes

----

## Le principe
* Certaines méthodes lancent des exceptions
* On contractualise ce qu'on fait
* On gère ou en propage
* Le repère : la responsabilité

----

## Documenter, contractualiser
```Java
/**
	 * Récupère l'individu correspondant à l'idep
	 * @param idep idep de l'individu, peut être null
	 * @return l'individu correspondant à l'idep
	 * @throws IndividuIntrouvableException si l'individu n'a pas été trouvé
	 */
public Individu getIndividu(String idep) throws IndividuIntrouvableException {
  [...]
  if (pasTrouve) {
    throw new IndividuIntrouvableException();
  }
}
```

----

## Gérer une exception
```Java
try {
  getIndividu("F2WBNP")
}
catch (IndividuIntrouvableException e) {
  //whatever
}
```

----

## Propager une exception
```Java
public void gererIndividu(String idep) throws IndividuIntrouvableException {
  Individu individu = getIndividu(idep);
  //whatever
}
```

----

## L'analogie du chantier

----

## Application : documentation & contractualisation de l'API

----

## Aller plus loin
* Creuser la différence entre checked / unchecked exceptions
* Notion et analyse d'une stacktrace
