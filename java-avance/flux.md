# Les flux

----

## Le besoin
* Echanges inter-processus, inter-applications, intra-application
* Format universel
* Interopérabilité
* Abstraction

----

## Le principe
* Un tuyau
* Flux entrant : lecture
* Flux sortant : écriture

----

## L'objet universel
* "Tout n'est que Flux"
* [java.io.InputStream](https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html)
* [java.io.OutputStream](https://docs.oracle.com/javase/8/docs/api/java/io/OutputStream.html)
* io VS nio VS nio 2 ?
* Javadoc & contrat

----

```
int	available()
Returns an estimate of the number of bytes that can be read (or skipped over) from this input stream without blocking by the next invocation of a method for this input stream.

abstract int read()
Reads the next byte of data from the input stream.

int	read(byte[] b)
Reads some number of bytes from the input stream and stores them into the buffer array b.

int	read(byte[] b, int off, int len)
Reads up to len bytes of data from the input stream into an array of bytes.

long	skip(long n)
Skips over and discards n bytes of data from this input stream.

void	close()
Closes this input stream and releases any system resources associated with the stream.

void	mark(int readlimit)
Marks the current position in this input stream.

boolean	markSupported()
Tests if this input stream supports the mark and reset methods

void	reset()
Repositions this stream to the position at the time the mark method was last called on this input stream.
```  

----

## Lire un flux
```
InputStream stream = new FileInputStream(new File("input.csv"));
```
```
InputStream stream;
int currentByte;
while ((currentByte = stream.read()) != -1) {

}
stream.close();
```

----

## Et dans la vraie vie ?
* InputStream : bytes stream
* (InputStream)Reader : characters stream, encoding-aware
* BufferedReader : characters stream, encoding-aware, buffered  

--  

* OutputStream : bytes stream
* (OutputStream)Writer : characters stream, encoding-aware
* BufferedWriter : characters stream, encoding-aware, buffered

----

## Lire un flux
```
InputStream stream = new FileInputStream(new File("input.csv"));
Reader reader = new InputStreamReader(stream, Charset.forName("UTF-8"));
BufferedReader bufferedReader = new BufferedReader(reader);

String currentLine;
while ((currentLine = bufferedReader.readLine()) != null) {

}
bufferedReader.close();
```

----

## nio 2
* Java 7+
* Classe fondamentale : [Path](https://docs.oracle.com/javase/8/docs/api/java/nio/file/Path.html)

```Java
Path p1 = Paths.get("/tmp/foo");
file.toPath();
...
```  

* Classe "static" [Files](https://docs.oracle.com/javase/8/docs/api/java/nio/file/Files.html)

```Java
Files.delete(path);
Files.move(pathSource,pathTarget,CopyOption... copyOptions);
Files.newBufferedReader(path, StandardCharsets.UTF_8);
Files.readAllLines(path, StandardCharsets.UTF_8);
...
```
* Legacy I/O => nio 2 : [https://docs.oracle.com/javase/tutorial/essential/io/legacy.html](https://docs.oracle.com/javase/tutorial/essential/io/legacy.html)

----

## Application : lecture du flux CSV
```
public List<String[]> readCSV(InputStream is);
```
* Test unitaire

----

## Aller plus loin
* Récupérer les données CSV en flux via HTTP ([HttpURLConnection](https://docs.oracle.com/javase/8/docs/api/java/net/HttpURLConnection.html))  
* Utiliser [Process](https://docs.oracle.com/javase/8/docs/api/java/lang/Process.html) pour lancer la compilation (mvn package) du projet et récupérer la log.
