# Initialisation d'un projet

----

## Le besoin
* Standardiser la structure de projet
* Configuration universelle
* Configuration "as code"

----

## Structure d'un projet Java
Projet brut  
<!-- .element: class="fragment" -->
[Projet maven](https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html)  
<!-- .element: class="fragment" -->
[Projet gradle](http://www.vogella.com/tutorials/Gradle/article.html)
<!-- .element: class="fragment" -->

----

## Le principe
* On sous-traite à un outil
* "Software project management tool"
* Tout passe par l'outil
* Outils : maven, gradle

----

## C'est quoi maven ?
* Commande mvn
* Interfaçage avec eclipse
* Le pom.xml = la vie

----

## Maven : phases
* Build avec maven ?  
![](img/maven-phases.png)
<!-- .element: class="fragment" -->

----

## Application : mise en place du projet
* Nouveau projet maven
* Java 8
* Dépendance JUnit
* Packager un jar

----

## Aller plus loin
* [Site officiel maven](https://maven.apache.org/)
* [Site officiel gradle](https://gradle.org/)
* Installer le jar dans le dépôt local : mvn install
* Déployer le jar sur l'artifactory INSEE (http://integration-continue.insee.fr), sur le nexus innovation (https://nexus.beta.innovation.insee.eu/) : mvn deploy
* En utilisant par exemple ce tutorial : [https://git.stable.innovation.insee.eu/f2wbnp/modernisation-des-outils-de-dev](https://git.stable.innovation.insee.eu/f2wbnp/modernisation-des-outils-de-dev), monter la version de maven à une version respectable