# Penser API

----

## Le besoin
* Mutualisation de composants
* Ne pas réinventer le roue
* Facilité d'utilisation
* Fiabilité, garanties

----

## Le principe
* Programmation par contrat
* Interface, Javadoc
* Couverture de tests

----

## En pratique
* Relation client / fournisseur
* Engagement et non-engagement

----

## L'outil 1 : la signature
```Java
public Object getObject(String arg0) throws Exception;
```
```Java
public Individu getIndividu(String idep) throws IndividuIntrouvableException;
```

----

## L'outil 2 : la Javadoc
```Java
/**
	 * Récupère l'individu correspondant à l'idep
	 * @param idep idep de l'individu, peut être null
	 * @return l'individu correspondant à l'idep
	 * @throws IndividuIntrouvableException si l'individu n'a pas été trouvé
*/
public Individu getIndividu(String idep) throws IndividuIntrouvableException;
```

----

## L'outil 3 : la doc
* Moins fiable
* Pas standardisée
* Pas automatisée
* Pas de lien fort

----

## Développement responsable
* 100% promesses tenues
* Montée de version (compatibilité ascendante)
* [Open / closed principle](http://joelabrahamsson.com/a-simple-example-of-the-openclosed-principle/)
* Liberté de dépendance

----

## Liberté de dépendance : Mise en place des logs
* Comment rajouter des logs à la bibliothèque ?

----

## Application : SLF4J
* Lien fort / faible
* Interface unifiée (API)
* Injection de dépendance
* Bindings

----

## Application : contractualiser l'API
* S'assurer que les signatures de méthodes sont expressives
* Ecrire le contrat des méthodes public
* Creer un projet client qui utilisera l'API
* Le client devra écrire dans la log toutes les lignes

----

## Aller plus loin
* Un peu de lecture : [https://dzone.com/articles/the-java-8-api-design-principles](https://dzone.com/articles/the-java-8-api-design-principles)
* Maven deploy : publier la bibliothèque sur l'artifactory INSEE. [http://integration-continue.insee.fr/artifactory](http://integration-continue.insee.fr/artifactory)
* Doc : publier un artefact sur maven central [https://maven.apache.org/guides/mini/guide-central-repository-upload.html](https://maven.apache.org/guides/mini/guide-central-repository-upload.html)
