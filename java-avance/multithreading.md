# Multithreading

----

## Le besoin
* Crash test, le retour

----

## Le principe
* Parallélisation
* Plusieurs fils d'exécution
* Un nouveau monde

----

## Le problème de la solution
> Some people, when confronted with a problem, think, “I know, I’ll use threads,” and then they have two problems.

----

## Threads : basics
* La classe [Thread](https://docs.oracle.com/javase/8/docs/api/java/lang/Thread.html)  

```Java
Thread thread = Thread.currentThread();
thread.getName();
thread.getId();
thread.getPriority();
Thread.sleep(5 * 1000);
```

----

## Threads : mon premier thread
```Java
public interface Runnable {
  public void run();
}
```

```Java
Runnable tache = new Runnable() {
  public void run() {
    System.out.println(Thread.currentThread().getName());
  };
}
Thread thread = new Thread(runnable);
thread.start();
```

----

## On joue ?
```Java
public class CompteJusqua10 implements Runnable {
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println(i);
		}
	}
}
```
```Java
for (int i = 0; i < 100; i++) {
	 new Thread(new CompteJusqua10()).start();
}
```

----

## Le problème de la solution
> Some people, when confronted with a problem, think, “I know, I’ll use threads,” and then two they hav erpoblesms.

----

## Application : traitement parralèle
* Simuler une opération de 10ms par ligne
* Consommer le flux CSV par n threads différents et comparer les performances

----

## Thread-safe, stateless
* Thread-safe ou non ?
* Stateless = thread-safe ?
* locks
* synchronized
* Goulot d'étranglement

----

## Organisation du travail
* Executors
* Queue
* ConcurrentQueue
* BlockingQueue

----

## Application : réorganiser la logique de traitement
* Utiliser des LinkedBlockingQueues pour répartir le boulot

----

## Aller plus loin
* Lire et expérimenter wait() et notify()
* Manipuler les InterruptedExceptions
* Futures / Promises
