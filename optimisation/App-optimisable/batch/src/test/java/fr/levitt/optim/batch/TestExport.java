package fr.levitt.optim.batch;

import javax.annotation.Resource;

import org.junit.jupiter.api.Test;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestExport {

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	@Qualifier("jobExport")
	private Job job;

	@Test
	public void testExport() throws Exception  {  
		export();
	}

	public void export() {
		try {
			JobParameters jobParameters = new JobParametersBuilder()
					.addLong("time", System.currentTimeMillis())
					.toJobParameters();
			jobLauncher.run(job, jobParameters).getExitStatus().getExitCode();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
