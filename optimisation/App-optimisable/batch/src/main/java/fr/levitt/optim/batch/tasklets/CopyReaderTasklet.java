package fr.levitt.optim.batch.tasklets;

import java.io.InputStream;
import java.sql.Connection;

import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.JdbcTemplate;

public class CopyReaderTasklet implements Tasklet {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	@Value("${batch.inputFileUL}")
	private String inputFile;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		try {
			Connection con = jdbcTemplate.getDataSource().getConnection();
			CopyManager copy = new CopyManager(con.unwrap(BaseConnection.class));
			InputStream is = resourceLoader.getResource(inputFile).getInputStream();
			copy.copyIn("COPY ul FROM STDIN WITH (FORMAT csv, HEADER true, DELIMITER ',')", is);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return RepeatStatus.FINISHED;
	}

}
