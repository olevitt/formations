package fr.levitt.optim.batch.core;

public class UniteLegale {

	private String siren;
	private String statutDiffusionUniteLegale;
	private String nomUniteLegale;
	
	public String getSiren() {
		return siren;
	}
	public void setSiren(String siren) {
		this.siren = siren;
	}
	public String getStatutDiffusionUniteLegale() {
		return statutDiffusionUniteLegale;
	}
	public void setStatutDiffusionUniteLegale(String statutDiffusionUniteLegale) {
		this.statutDiffusionUniteLegale = statutDiffusionUniteLegale;
	}
	public String getNomUniteLegale() {
		return nomUniteLegale;
	}
	public void setNomUniteLegale(String nomUniteLegale) {
		this.nomUniteLegale = nomUniteLegale;
	}
	
	
}
