package fr.levitt.optim.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.levitt.optim.batch.tasklets.BasicCSVReaderTasklet;
import fr.levitt.optim.batch.tasklets.CopyReaderTasklet;

@Configuration
public class ChargementUL {
 
    @Autowired
    private JobBuilderFactory jobs;
 
    @Autowired
    private StepBuilderFactory steps;
 
    @Bean
    protected Step copyReader() {
        return steps
          .get("copyReader")
          .tasklet(copyReaderTasklet())
          .build();
    }
    
    @Bean
    public CopyReaderTasklet copyReaderTasklet() {
        return new CopyReaderTasklet();
    }
 
    @Bean("jobChargementUL")
    public Job job() {
        return jobs
          .get("taskletsJob"+System.currentTimeMillis())
          .start(copyReader())
          .build();
    }
 
}

