package fr.levitt.optim.batch.core;

import java.util.ArrayList;
import java.util.List;

public class EtablissementDB extends Etablissement {

	//On considère qu'un etablissement est l'etablissement principal si c'est le premier etablissement rencontre pour un siren donne 
	private boolean etabPrincipal = true;
	
	
	
	public boolean isEtabPrincipal() {
		return etabPrincipal;
	}

	public void setEtabPrincipal(boolean etabPrincipal) {
		this.etabPrincipal = etabPrincipal;
	}

	private static List<String> sirenDejaVus = new ArrayList<>();
	
	public EtablissementDB(Etablissement etab) {
		if (sirenDejaVus.contains(etab.getSiren())) {
			etabPrincipal = false;
		}
		sirenDejaVus.add(etab.getSiren());
		this.setNic(etab.getNic());
		this.setSiren(etab.getSiren());
		
	}
}
