package fr.levitt.optim.batch.tasklets;

import java.sql.Connection;
import java.sql.ResultSet;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.JdbcTemplate;

public class ExportTasklet implements Tasklet {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ResourceLoader resourceLoader;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		Connection connection = jdbcTemplate.getDataSource().getConnection();
		ResultSet rs = connection.prepareStatement("SELECT * FROM ul WHERE prenom1UniteLegale = 'GEORGES'").executeQuery();
		while (rs.next()) {
			System.out.println(rs.getString(1));
		}
		return RepeatStatus.FINISHED;
	}

}
