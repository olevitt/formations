package fr.levitt.optim.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.levitt.optim.batch.tasklets.CopyReaderTasklet;
import fr.levitt.optim.batch.tasklets.ExportTasklet;

@Configuration
public class Export {
 
    @Autowired
    private JobBuilderFactory jobs;
 
    @Autowired
    private StepBuilderFactory steps;
 
    @Bean
    protected Step exportStep() {
        return steps
          .get("writeLines")
          .tasklet(exportData())
          .build();
    }
    
    @Bean
    public ExportTasklet exportData() {
        return new ExportTasklet();
    }
 
    @Bean("jobExport")
    public Job job() {
        return jobs
          .get("taskletsJob"+System.currentTimeMillis())
          .start(exportStep())
          .build();
    }
 
}

