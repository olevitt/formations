package fr.levitt.optim.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.levitt.optim.batch.tasklets.BasicCSVReaderTasklet;

@Configuration
@EnableBatchProcessing
public class Chargement {
 
    @Autowired
    private JobBuilderFactory jobs;
 
    @Autowired
    private StepBuilderFactory steps;
 
    @Bean
    protected Step readLines() {
        return steps
          .get("readLines")
          .tasklet(linesReader())
          .build();
    }
    
    @Bean
    public BasicCSVReaderTasklet linesReader() {
        return new BasicCSVReaderTasklet();
    }
 
    @Bean("jobChargement")
    public Job jobChargement() {
        return jobs
          .get("taskletsJob"+System.currentTimeMillis())
          .start(readLines())
          .build();
    }
 
    // ...
 
}

