package fr.levitt.optim.batch.tasklets;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.JdbcTemplate;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import fr.levitt.optim.batch.core.Etablissement;
import fr.levitt.optim.batch.core.EtablissementDB;

public class BasicCSVReaderTasklet implements Tasklet {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Value("${batch.inputFile}")
	private String inputFile;
	
	@Autowired
	ResourceLoader resourceLoader;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		
		CsvMapper mapper = new CsvMapper();
		mapper.configure(CsvParser.Feature.IGNORE_TRAILING_UNMAPPABLE, true);
		CsvSchema schema = mapper.schema().withoutQuoteChar().withHeader().withColumnSeparator(','); // schema from 'Pojo' definition
		
		MappingIterator<Etablissement> it = mapper.readerFor(Etablissement.class).with(schema)
		  .readValues(resourceLoader.getResource(inputFile).getInputStream());
		
		Connection connection = jdbcTemplate.getDataSource().getConnection();
		List<Etablissement> all = it.readAll();
		for (Etablissement etab : all) {
			EtablissementDB etabDB = new EtablissementDB(etab);
			insererEtablissement(connection,etabDB);
		}
		
		return RepeatStatus.FINISHED;
	}
	
	private void insererEtablissement(Connection connection, EtablissementDB etab) throws SQLException {
		String siren = etab.getSiren();
		String nic = etab.getNic();
		boolean principal = etab.isEtabPrincipal();
		connection.createStatement().executeUpdate("INSERT INTO etablissement VALUES('"+siren+"','"+nic+"',"+principal+")");
	}

}
