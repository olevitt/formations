DROP TABLE IF EXISTS etablissement;
DROP TABLE IF EXISTS unitelegale;
DROP TABLE IF EXISTS ul;

CREATE TABLE etablissement  (
    siren VARCHAR(9),
    nic VARCHAR(5),
    principal boolean,
    PRIMARY KEY (siren,nic)
);

CREATE TABLE ul  (
    siren TEXT PRIMARY KEY,
    statutDiffusionUniteLegale TEXT,
    unitePurgeeUniteLegale TEXT,
    dateCreationUniteLegale TEXT,
    sigleUniteLegale TEXT,
    sexeUniteLegale TEXT,
    prenom1UniteLegale TEXT,
    prenom2UniteLegale TEXT,
    prenom3UniteLegale TEXT,
    prenom4UniteLegale TEXT,
    prenomUsuelUniteLegale TEXT,
    pseudonymeUniteLegale TEXT,
    identifiantAssociationUniteLegale TEXT,
    trancheEffectifsUniteLegale TEXT,
    anneeEffectifsUniteLegale TEXT,
    dateDernierTraitementUniteLegale TEXT,
    nombrePeriodesUniteLegale TEXT,
    categorieEntreprise TEXT,
    anneeCategorieEntreprise TEXT,
    dateDebut TEXT,
    etatAdministratifUniteLegale TEXT,
    nomUniteLegale TEXT,
    nomUsageUniteLegale TEXT,
    denominationUniteLegale TEXT,
    denominationUsuelle1UniteLegale TEXT,
    denominationUsuelle2UniteLegale TEXT,
    denominationUsuelle3UniteLegale TEXT,
    categorieJuridiqueUniteLegale TEXT,
    activitePrincipaleUniteLegale TEXT,
    nomenclatureActivitePrincipaleUniteLegale TEXT,
    nicSiegeUniteLegale TEXT,
    economieSocialeSolidaireUniteLegale TEXT,
    caractereEmployeurUniteLegale TEXT
);