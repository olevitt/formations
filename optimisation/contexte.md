## De quoi on va parler ?

<!-- .slide: class="slide" -->

- Performance
- Charge
- Optimisation

---

## Organisation

<!-- .slide: class="slide" -->

- Quelques principes
- Applications pratiques
- Cas réels

---

### Sujets principaux

<!-- .slide: class="slide" -->

- CPU / mémoire
- Java + SQL
- SQL

---

### 1 grande règle

<!-- .slide: class="slide" -->

> Le code c'est moche

---

### Notions abordées

<!-- .slide: class="slide" -->

- Grands principes de performance
  - Ressources
  - Tradeoff CPU / mémoire
  - Ordre de grandeur des coûts
- Profiling (JVisualVM)
- Parallélisation
- Tracing SQL (P6spy, JVisualVM, pg_stat_statements)
- Optimisation SQL (explain, TTFB, index, vacuum)
- N+1 select (Lazy loading)
- Insertions / requêtages massifs (fetch size, batch size, copy)
- Créer un cache (manuellement, EHCache)
- Boss final : une appli réelle
