## La boîte à outils

<!-- .slide: class="slide" -->

---

- VisualVM (https://visualvm.github.io/)
- Glowroot (deprecated ?)
- p6spy
- grafana
- arthas
- Postgres
- Client postgres

---

## Glowroot

- deprecated ?
- https://glowroot.org/
- Glowroot central

---

## P6Spy

- https://github.com/p6spy/p6spy
- https://p6spy.readthedocs.io/en/latest/install.html#generic-instructions

---

## Grafana (+ prometheus)

- https://grafana.lab.sspcloud.fr/
- https://grafana.beta.innovation.insee.eu/
- https://grafana.kub.sspcloud.fr/

---

## Arthas

- https://github.com/alibaba/arthas

---

## Postgres

- `docker run -p 5432:5432 -e POSTGRES_PASSWORD=toto postgres:13`
- https://github.com/garethflowers/postgresql-portable
- https://datalab.sspcloud.fr
- https://onyxia.beta.innovation.insee.eu

---

## Client postgres

- https://dbeaver.io/  
- https://www.pgadmin.org/
