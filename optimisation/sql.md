## Optimisation SQL

<!-- .slide: class="slide" -->

---

## Plan d'exécution

<!-- .slide: class="slide" -->

- Plan d'exécution
- `EXPLAIN SELECT * FROM toto`
- Costs, TTFB

---

## Requêtes utiles

<!-- .slide: class="slide" -->

- pg_stat_statements

---

## Index

<!-- .slide: class="slide" -->

- Tradeoff CPU / mémoire
- Seq scan, index scan, index only
- Coût d'un index

---

## Maintenance de la base

<!-- .slide: class="slide" -->

- Lignes mortes, `vacuum`
- `analyze` / statistics

---

## Infra

<!-- .slide: class="slide" -->

- Locks
- Connexions simultanées
- Configuration mémoire / CPU
- Namespace mémoire

---

## TP : SQL

<!-- .slide: class="slide" -->

- Repérer la requête couteuse
- Calculer son plan d'exécution
- Optimiser ce plan à l'aide d'index
