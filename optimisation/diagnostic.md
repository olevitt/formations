## Diagnostic

<!-- .slide: class="slide" -->

- Issue #42 : c'est lent

---

### Comment faire ?

<!-- .slide: class="slide" -->

- Lire le code
  <!-- .element: class="fragment" -->

NON

<!-- .element: class="fragment" -->

- Sysout
  <!-- .element: class="fragment" -->

NON

<!-- .element: class="fragment" -->

- Avancer pas à pas (débug)
  <!-- .element: class="fragment" -->

NON

<!-- .element: class="fragment" -->

---

### Donc ?

<!-- .slide: class="slide" -->

- Architecture
- Métriques
- Reproductibilité
