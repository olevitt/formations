## TP2 : une grosse affaire

<!-- .slide: class="slide" -->

---

On décide de s'intéresser à cette application : https://github.com/HealerMikado/Non-player-character_Generator.  
Même règle qu'avant : **on ne lit pas le code**

- Cloner le dépôt
- Lancer l'application (`cd poney-back && mvn spring-boot:run`)
- Les endpoints intéressants sont `/generate`, `/characters`, `/races`
- Optionnel : connecter l'UI (`poney-front`) en utilisant `npm` (modifier le fichier `.env` pour pointer l'API vers `http://localhost:8080`)
- Analyser le comportement de l'application en la stressant un peu
- En déduire des optimisations, les implémenter
- Optionnel : utiliser une base de données `Postgres` à la place de la base `H2`
- Optionnel : charger massivement des données dans la base
