## Principes

<!-- .slide: class="slide" -->

---

### Une machine ?

<!-- .slide: class="slide" -->

- CPU
  <!-- .element: class="fragment" -->
- RAM
  <!-- .element: class="fragment" -->
- Disque
  <!-- .element: class="fragment" -->
- Réseau
  <!-- .element: class="fragment" -->

---

### Le coût

<!-- .slide: class="slide" -->

| Opération            | Coût absolu | Coût relatif |
| -------------------- | ----------- | ------------ |
| Opération CPU        | 0.5 ns      | 1            |
| I/O RAM              | 100 ns      | 200          |
| I/O SSD              | 50–150 μs   | 200 000      |
| I/O HDD              | 1 ms        | 2 000 000    |
| Ping poste - db metz | 20ms        | 20 000 000   |
| Ping Paris - NY      | 75 ms       | 150 000 000  |

---

### Une histoire d'équilibre

<!-- .slide: class="slide" -->

- Tradeoff CPU / mémoire
- Cache
