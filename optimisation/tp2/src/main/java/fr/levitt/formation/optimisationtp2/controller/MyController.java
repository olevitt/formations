package fr.levitt.formation.optimisationtp2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.levitt.formation.optimisationtp2.model.Ville;

@RestController
public class MyController {

	@GetMapping
	public List<Ville> getHello() {
		List<Ville> villes = new ArrayList<Ville>();
		for (int i = 0; i < 36000; i++) {
			villes.add(new Ville());
		}
		return villes;
	}
}
