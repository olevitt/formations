package fr.levitt.formation.optimisationtp2.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.levitt.formation.optimisationtp2.model.Ville;

@Repository
public interface VilleDAO extends CrudRepository<Ville, Integer> {

}
