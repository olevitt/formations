package fr.levitt.formation.optimisationtp2;


import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import fr.levitt.formation.optimisationtp2.dao.VilleDAO;
import fr.levitt.formation.optimisationtp2.model.Ville;

@SpringBootApplication
@EnableTransactionManagement 
public class OptimisationTp2Application {

	public static void main(String[] args) {
		SpringApplication.run(OptimisationTp2Application.class, args);
	}
	
	@Bean
	public CommandLineRunner loadData(VilleDAO repository) {
	    return (args) -> {
	        cleanInsertData(repository);
	    };
	}
	
	@javax.transaction.Transactional
	private void cleanInsertData(VilleDAO repository) {
		repository.deleteAll();
		List<Ville> villes = new ArrayList<Ville>();
        for (int i = 0; i < 36000; i++) {
        	Ville ville = new Ville();
        	ville.setId(i);
        	villes.add(ville);
		}
        repository.saveAll(villes);
	}

}
