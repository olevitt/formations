## Optimisation Java - SQL

<!-- .slide: class="slide" -->

---

## Batching

<!-- .slide: class="slide" -->

- Aller-retour : coût fixe
- Factoriser les plans : PreparedStatement
- Batcher les insertions : batchsize
- Batcher les récupérations : fetchsize

---

## La violence postgres : copy

<!-- .slide: class="slide" -->

- Spécifique postgres (non standard SQL)
- Données brutes
- Droit au but
- Supporté par le driver Java

---

## TP : Java / SQL

- Utiliser PreparedStatement
- Batcher les insertions
- Jouer sur le batchsize
- Sidequest : utiliser copy et comparer les résultats
