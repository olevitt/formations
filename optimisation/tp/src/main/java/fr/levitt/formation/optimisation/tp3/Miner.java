package fr.levitt.formation.optimisation.tp3;


import org.springframework.beans.factory.annotation.Autowired;

import fr.levitt.formation.optimisation.common.CompteurDeVitesse;

public abstract class Miner {

    @Autowired
    private CompteurDeVitesse compteurDeVitesse;

    public final void startMining(String data, String previousHash, Long timestamp, String difficulty) {
        compteurDeVitesse.demarrer();
        mineBlock(data, previousHash, timestamp, difficulty);
        compteurDeVitesse.arreter();
    }

    protected abstract void mineBlock(String data, String previousHash, Long timestamp, String difficulty);

    protected void plusUn() {
        compteurDeVitesse.plusUn();
    }
}
