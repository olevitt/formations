package fr.levitt.formation.optimisation.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenerateurGrosFichier {

	@Autowired
	private CompteurDeVitesse compteurDeVitesse;
	
    public void genererGrosFichier(File target, int nbLignes) {
        try {
            target.getParentFile().mkdirs();
            OutputStream st = new FileOutputStream(target);
            PrintWriter writer = new PrintWriter(st);
            compteurDeVitesse.demarrer();
            for (int i = 0; i < nbLignes; i++) {
                writer.println(String.valueOf((int) (Math.random()*10000000)));
                compteurDeVitesse.plusUn();
            }
            compteurDeVitesse.arreter();
            System.out.println("Fichier crée");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
