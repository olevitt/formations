package fr.levitt.formation.optimisation.common;

public interface CompteurDeVitesse {

    public void demarrer();

    public void arreter();
    
    public void plus(int nb);

    public default void plusUn() {
    	plus(1);
    }
    
}
