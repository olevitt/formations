package fr.levitt.formation.optimisation.common;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CompteurDeVitesseBasique implements CompteurDeVitesse {

    private long compteur = 0;
    private long compteurPrecedent = 0;
    @Value("${compteur.cadencems}")
    private int cadence;

    private Timer timer = new Timer();

    @Override
    public void demarrer() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                long nb = compteur;
                System.out.println("Vitesse : "+(nb - compteurPrecedent)+"/s (Total : "+nb+")");
                compteurPrecedent = compteur;
            }
        }, new Date(),cadence);
    }

    @Override
    public void arreter() {
        timer.cancel();
        timer.purge();
    }

    @Override    
    public void plus(int nb) {
    	compteur += nb;
    }
}
