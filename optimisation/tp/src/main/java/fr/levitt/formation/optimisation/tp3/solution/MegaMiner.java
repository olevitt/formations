package fr.levitt.formation.optimisation.tp3.solution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.levitt.formation.optimisation.tp3.Hasher;
import fr.levitt.formation.optimisation.tp3.Miner;

import java.util.ArrayList;
import java.util.List;

@Service
@Qualifier("optimise")
public class MegaMiner extends Miner {

    @Autowired
    private Hasher hasher;

    private int nbThreads = 10;

    public void mineBlock(String data, String previousHash, Long timestamp, String difficulty) {
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < nbThreads; i++) {
            final int threadNumber = i;
            threads.add(new Thread(new Runnable() {
                @Override
                public void run() {
                    Long nonce = 0L;
                    while (true) {
                        if (nonce % nbThreads == threadNumber) {
                            String newHash = hasher.calculateHash(data,previousHash,timestamp,nonce);
                            if (newHash.substring(0, difficulty.length()).equals(difficulty)) {
                                return;
                            }
                            plusUn();
                        }
                        nonce++;
                    }
                }
            }));
        }

        for (Thread t : threads) {
            t.start();
        }

        try {
            Thread.sleep(500000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
