package fr.levitt.formation.optimisation.tp2;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.levitt.formation.optimisation.common.CompteurDeVitesse;

@Service
public class CompteurDistinct {
	
	@Autowired
	private CompteurDeVitesse compteur;
	
	private List<String> dejaVus = new ArrayList<>();

	public long compterDistinct(File file) throws IOException {
		
		BufferedReader reader = Files.newBufferedReader(file.toPath());
		String ligne = null;
		long nbDistincts = 0;
		compteur.demarrer();
		while ((ligne = reader.readLine()) != null) {
			compteur.plusUn();
			boolean dejaVu = isDejaVu(ligne);
			if (!dejaVu) {
				dejaVus.add(ligne);
				nbDistincts++;
			}
		}
		compteur.arreter();
		return nbDistincts;
	}
	
	private boolean isDejaVu(String str) {
		return dejaVus.contains(str);
	}
}
