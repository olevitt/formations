package fr.levitt.formation.optimisation.tp3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Timer;
import java.util.TimerTask;

@Service
@Primary
public class MinerBasique extends Miner {

    @Autowired
    private Hasher hasher;

    public void mineBlock(String data, String previousHash, Long timestamp, String difficulty) {
        Long nonce = 0L;
        while (true) {
            String newHash = hasher.calculateHash(data,previousHash,timestamp,nonce);
            if (newHash.substring(0, difficulty.length()).equals(difficulty)) {
                return;
            }
            plusUn();
            nonce++;
        }
    }
}
