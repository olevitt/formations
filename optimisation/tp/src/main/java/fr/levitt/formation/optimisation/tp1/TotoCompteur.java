package fr.levitt.formation.optimisation.tp1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import fr.levitt.formation.optimisation.common.CompteurDeVitesse;

@Controller
public class TotoCompteur {
	
	@Autowired
	private CompteurDeVitesse compteurDeVitesse;

    /**
     * Compte le nombre de toto dans le fichier
     * @param path : chemin (relatif ou absolu) du fichier
     * @return
     * @throws IOException
     */
    public int compterToto(File file) throws IOException {
        List<String> lignes = Files.readAllLines(file.toPath());
        int compteur = 0;
        compteurDeVitesse.demarrer();
        for (String line : lignes) {
        	compteurDeVitesse.plusUn();
            if (line.equals("toto")) {
                compteur++;
            }
        }
        compteurDeVitesse.arreter();
        return compteur;
    }
}
