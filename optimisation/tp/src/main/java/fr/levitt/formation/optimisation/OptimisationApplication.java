package fr.levitt.formation.optimisation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OptimisationApplication {



	public static void main(String[] args) {
		SpringApplication.run(OptimisationApplication.class, args);
	}

}
