package fr.levitt.formation.optimisation.tp3;

import org.springframework.stereotype.Service;

@Service
public class Hasher {

    public String calculateHash(String data, String previousHash, Long timestamp, Long nonce) {
        return StringUtil.applySha256(
                previousHash +
                        String.valueOf(timestamp) +
                        String.valueOf(nonce) +
                        data
        );
    }
}
