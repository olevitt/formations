package fr.levitt.formation.optimisation;

import fr.levitt.formation.optimisation.common.GenerateurGrosFichier;
import fr.levitt.formation.optimisation.tp1.TotoCompteur;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;

@SpringBootTest
class OptimisationApplicationTests {
	
	@Autowired
	GenerateurGrosFichier generateurGrosFichier;
	
	int nbLignesAEcrire = 1000*1000*500;
	
	@Test
	public void genererFichier() throws IOException {
		generateurGrosFichier.genererGrosFichier(new File("output","toto.txt"),nbLignesAEcrire);
	}

}
