package fr.levitt.formation.optimisation;

import fr.levitt.formation.optimisation.common.GenerateurGrosFichier;
import fr.levitt.formation.optimisation.tp1.TotoCompteur;
import fr.levitt.formation.optimisation.tp2.CompteurDistinct;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;

@SpringBootTest
class TP2Test {

    @Autowired
    CompteurDistinct compteurDistinct;


    @Test
    public void exo2() throws IOException {
        long nbToto = compteurDistinct.compterDistinct(new File("output","toto.txt"));
        Assertions.assertEquals(1000, nbToto);
    }

}