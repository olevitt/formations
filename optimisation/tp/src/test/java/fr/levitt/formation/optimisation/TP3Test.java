package fr.levitt.formation.optimisation;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import fr.levitt.formation.optimisation.tp3.Miner;
import fr.levitt.formation.optimisation.tp3.MinerBasique;

import java.io.IOException;

@SpringBootTest
class TP3Test {

    @Autowired
    private Miner miner;

    @Value("${tp2.difficulty}")
    private String difficulty;

    @Test
    public void exo3() throws IOException {
        miner.startMining("Hello world !","0",System.currentTimeMillis(), difficulty);
    }

}