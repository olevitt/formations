package fr.levitt.formation.optimisation;

import fr.levitt.formation.optimisation.common.GenerateurGrosFichier;
import fr.levitt.formation.optimisation.tp1.TotoCompteur;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;

@SpringBootTest
class TP1Test {

    @Autowired
    TotoCompteur totoCompteur;

    @Test
    public void exo1() throws IOException {
        int nbToto = totoCompteur.compterToto(new File("output","toto.txt"));
        Assertions.assertEquals(1000, nbToto);
    }

}