### Aller plus loin
<!-- .slide: class="slide" -->  

----

### Plus loin : plus d'événements
<!-- .slide: class="slide" -->  
* [Liste des évenements](https://www.w3schools.com/jsref/dom_obj_event.asp)

----

### Plus loin, les informations sur l'event
<!-- .slide: class="slide" -->  
exemple.html
```HTML
<input type="button" value="Clique !" onclick="direBonjour();" id="boutonClique" />
```
exemple.js
```Javascript
function direBonjour(event) {
    console.log(event);
}
```
<input type="button" value="Clique !" onclick="console.log(event)" id="boutonCliquePlusLoin" />
* Regarder le contenu de event
* Essayer d'autres types d'événements pour constater les différences

----

### Plus loin, interrompre l'événement
<!-- .slide: class="slide" -->  
exemple.html
```HTML
<form target="/toto.php">
<input type="submit" value="Clique !" onclick="verifierEtValider();" id="boutonClique" />
<form>
```
exemple.js
```Javascript
function verifierEtValider(event) {
    var ok = false;
    // On vérifie et éventuellement on passe ok à true si tout va bien

    if (ok) {
        return true;  // Tout va bien, go
    }
    else {
        return false; // Le click ne sera pas vraiment fait
    }
}
```
<form target="/toto.php">
<input type="submit" value="Return false" onclick="return false;" id="boutonCliqueFalse" />
<input type="submit" value="Return true" onclick="return true;" id="boutonCliqueTrue" />
</form>