### Rappel : objets en Javascript
<!-- .slide: class="slide" -->  

```Javascript
var classe = {
    promo : '2020',
    etudiants : [
        {
            prenom : 'Bob',
            nom : 'Le bricoleur'
        },
        {
            prenom : 'Bobette',
            nom : 'La bricoleuse'
        }
    ]
}
```
```Javascript
console.log(classe.promo);
classe.promo = '2021';
console.log(classe.etudiants.length);
console.log(classe.etudiants[1].nom);
```

----

### XML
<!-- .slide: class="slide" -->  
Enjeu : mettre en forme ces données.  
```XML
<classe>
    <promo>2020</promo>
   <etudiants>
      <element>
         <nom>Le bricoleur</nom>
         <prenom>Bob</prenom>
      </element>
      <element>
         <nom>La bricoleuse</nom>
         <prenom>Bobette</prenom>
      </element>
   </etudiants>
</classe>
```

----

### JSON
<!-- .slide: class="slide" -->  
JavaScript Object Notation
```JSON
{
    "promo" : "2020",
    "etudiants" : [
        {
            "prenom" : "Bob",
            "nom" : "Le bricoleur"
        },
        {
            "prenom" : "Bobette",
            "nom" : "La bricoleuse"
        }
    ]
}
```

----

### JSON en Javascript
<!-- .slide: class="slide" -->  
* Natif
```Javascript
console.log(JSON.stringify(classe));
var classe = JSON.parse(`
{
    "promo" : "2020",
    "etudiants" : [
        {
            "prenom" : "Bob",
            "nom" : "Le bricoleur"
        },
        {
            "prenom" : "Bobette",
            "nom" : "La bricoleuse"
        }
    ]
}
`);
```