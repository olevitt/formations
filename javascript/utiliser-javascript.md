### Utiliser Javascript
<!-- .slide: class="slide" -->  
* Etape par étape

----

### Résumé des épisodes précédentes
<!-- .slide: class="slide" -->  
* Fonction   

```Javascript
function addition(a,b) {
    return a+b;
}
```

* Variable  

```Javascript
function addition(a,b) {
    var c = a+b;
    return c;
}
```

----

### Etape 1 : un contenu HTML
<!-- .slide: class="slide" -->  
exemple.html  

```HTML
<input type="button" value="Clique !" />
```
<input type="button" value="Clique !" />

----

### Etape 2 : du code javascript
<!-- .slide: class="slide" -->  
exemple.js
```Javascript
function direBonjour() {
    alert("Bonjour");
}
```

----

### Etape 3 : lier HTML et JS
<!-- .slide: class="slide" -->  
exemple.html
```HTML
<head>
<script src="exemple.js"></script>
</head>
```

----

### Etape 4 : lier les événements
<!-- .slide: class="slide" -->  
exemple.html
```HTML
<input type="button" value="Clique !" onclick="direBonjour();" />
```
<input type="button" value="Clique !" onclick="alert('Hello');" />

----

### Application
<!-- .slide: class="slide" -->  
* Créer un champ mot de passe
* Lorsque l'utilisateur écrit dans le champ, afficher "Coucou" dans la console