### Manipuler le HTML depuis Javascript
<!-- .slide: class="slide" -->  
* Pas à pas

----

### Episodes précédents
<!-- .slide: class="slide" -->  
exemple.html  
```HTML
<head>
<script src="exemple.js"></script>
</head>
<body>
<input type="button" value="Clique !" onclick="direBonjour();" />
</body>
```

exemple.js
```Javascript
function direBonjour() {
    alert("Bonjour");
}
```

----

### Etape 1 : rajouter des identifiants
<!-- .slide: class="slide" -->  
exemple.html
```HTML
<input type="button" value="Clique !" onclick="direBonjour();" id="boutonClique" />
```
<input type="button" value="Clique !" onclick="alert('Bonjour')" id="boutonClique" />

----

### Etape 2 : rechercher le bon élément
<!-- .slide: class="slide" -->  
exemple.js
```Javascript
function direBonjour() {
    var leboutonClique = document.getElementById('boutonClique');
}
```
<input type="button" value="Clique !" onclick="var leboutonClique = document.getElementById('boutonCliqueEtape2');" id="boutonCliqueEtape2" />

----

### Etape 3, option 1 : lire le contenu 
<!-- .slide: class="slide" -->  
exemple.js
```Javascript
function direBonjour() {
    var leboutonClique = document.getElementById('boutonClique');
    var valueDuBouton = leboutonClique.value;
    alert(valueDuBouton);
}
```
<input type="button" value="Clique !" onclick="var leboutonClique = document.getElementById('boutonCliqueEtape31');var valueDuBouton = leboutonClique.value;alert(valueDuBouton);" id="boutonCliqueEtape31" />

----

### Etape 3, option 2 : modifier le contenu 
<!-- .slide: class="slide" -->  
exemple.js
```Javascript
function direBonjour() {
    var leboutonClique = document.getElementById('boutonClique');
    leboutonClique.value = 'Nouveau texte';
}
```
<input type="button" value="Clique !" onclick="var leboutonClique = document.getElementById('boutonCliqueEtape32');leboutonClique.value = 'Nouveau texte';" id="boutonCliqueEtape32" />

----

### Etape 4 : combiner les 2 étapes 3
<!-- .slide: class="slide" -->  
exemple.js
```Javascript
function direBonjour() {
    var leboutonClique = document.getElementById('boutonClique');
    var valueDuBouton = leboutonClique.value;
    leboutonClique.value = valueDuBouton + '!';
}
```
<input type="button" value="Clique !" onclick="var leboutonClique = document.getElementById('boutonClique2');
var valueDuBouton = leboutonClique.value;
leboutonClique.value = valueDuBouton + '!';
" id="boutonClique2" />

----

### Application 1
<!-- .slide: class="slide" -->  
* Créer un champ mot de passe
* Créer un bouton valider
* Quand l'utilisateur clique sur le bouton valider, afficher la valeur du mot de passe dans la console
<br /><input type="password" id="passwordApplication1"/>
<input type="button" onclick="console.log(document.getElementById('passwordApplication1').value)" value="Valider">

----

### Application 2
<!-- .slide: class="slide" -->  
* Créer un deuxième champ mot de passe
* Quand l'utilisateur clique sur valider, afficher les 2 mots de passe dans la console
* Quand l'utilisateur clique sur valider, affiche un popup indiquant si oui ou non les mots de passe sont identiques
<br />
<input type="password" id="passwordApplication2"/><br />
<input type="password" id="passwordApplication22"/><br />
<input type="button" onclick="console.log(document.getElementById('passwordApplication2').value);
console.log(document.getElementById('passwordApplication22').value);
if (document.getElementById('passwordApplication2').value != document.getElementById('passwordApplication22').value) {
    alert('Les mots de passe sont différents');
} else {
    alert('Les mots de passe sont identiques');
}" value="Valider">

----

### Application 3
<!-- .slide: class="slide" -->  
* Quand l'utilisateur écrit dans l'un des deux champs mots de passe, faire la même vérification d'égalité des mots de passe
* Si les mots de passe ne sont pas identiques, désactiver (disabled=true) le bouton. Si les mots de passe sont identiques, réactiver le bouton (disabled=false).<br />
<input type="password" id="passwordApplication3" oninput="if (document.getElementById('passwordApplication3').value != document.getElementById('passwordApplication32').value) {
    document.getElementById('boutonValider').disabled = true;
} else {
    document.getElementById('boutonValider').disabled = false;
}" /><br />
<input type="password" id="passwordApplication32" oninput="if (document.getElementById('passwordApplication3').value != document.getElementById('passwordApplication32').value) {
    document.getElementById('boutonValider').disabled = true;
} else {
    document.getElementById('boutonValider').disabled = false;
}" /><br />
<input type="button" value="Valider" id="boutonValider">