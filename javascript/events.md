### Les events
<!-- .slide: class="slide" -->  
* Système d'évenements
* Enregistrement auprès des composants

```HTML
<button onclick="alert('Bonjour');">Dire bonjour</button>
<button onclick="maFonction();">Dire bonjour</button>
```

----

### Différents évenements
<!-- .slide: class="slide" --> 
<table>
<tr><th>Nom</th><th>Evenement</th></tr>
<tr><td>onclick</td><td>The user clicks an HTML element</td></tr>
<tr><td>onchange</td><td>An HTML element has been changed</td></tr>
<tr><td>onmouseover</td><td>The user moves the mouse over an HTML element</td></tr>
<tr><td>onmouseout</td><td>The user moves the mouse away from an HTML element</td></tr>
<tr><td>onkeydown</td><td>The user pushes a keyboard key</td></tr>
<tr><td>onload</td><td>The browser has finished loading the page</td></tr>
</table>
[Liste des évenements](https://www.w3schools.com/jsref/dom_obj_event.asp)

----

### Exemples
<!-- .slide: class="slide" --> 
```HTML
<div id="reponse"></div>
<button onclick="document.getElementById('reponse').innerHTML='Hop !';">
Clique pour voir ?
</button>
```
<div id="reponse"></div>
<button onclick="document.getElementById('reponse').innerHTML='Hop !';">
Clique pour voir ?
</button>

----

### Exemples
<!-- .slide: class="slide" --> 
```HTML
<body onload="alert('toto');">
<body onload="mafonctionInit();">
```
[Exemple](exemples/onload.html)
