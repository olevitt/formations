## Pré-requis
<!-- .slide: class="slide" -->  

Conseillé :  
* HTML

Utile :  
* CSS
* Notions de programmation
* Notions de Web / HTTP

----

### HTML
<!-- .slide: class="slide" -->  

* HyperText Markup Language
* Langage à balise
* Prévu pour le web

```HTML
<h1>Titre</h1>
<p>
    <i>Texte en italique</i>
</p>
```
<h1>Titre</h1>
<p>
    <i>Texte en italique</i>
</p>
* Exemple : view-source:https://supinternet.fr/

----

### HTML, s'entrainer
<!-- .slide: class="slide" -->  

* Créer un fichier nommé exemple.html
* Y écrire un code HTML simple
```HTML
<h1>Titre</h1>
<p>
    <i>Texte en italique</i>
</p>
```
* Ouvrir le fichier avec un navigateur
* Modifier le fichier pour le rendre plus complet

----

### CSS
<!-- .slide: class="slide" -->  

* Cascading Style Sheets
* Langage de feuilles de style
* Précise le style des éléments HTML

```CSS
body {
    background-color: white;
}
                
h1 {
    color: maroon;
    margin-left: 40px;
} 
```
<div style='background-color:white'>
<h1 style='color:maroon;margin-left:40px'>Titre</h1>
<p>
    <i>Texte en italique</i>
</p>
</div>
* Exemple : view-source:https://supinternet.fr/

----

### CSS, s'entrainer
<!-- .slide: class="slide" -->  

* Repartir d'un code HTML quelconque
* Y inclure le code CSS suivant (via la balise style) :  

```HTML
<style>
body {
    background-color: white;
}
                
h1 {
    color: maroon;
    margin-left: 40px;
} 
</style>
```
* Ouvrir le fichier avec un navigateur
* Modifier le style CSS pour tester d'autres fonctionnalités