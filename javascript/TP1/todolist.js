

function initTableau() {
    var taches = ["Découvrir JS", "Faire une appli de TODO-list", "Partir en pause", "Finir ma TODO-list"];
    var statuts = ["OK", "En cours", "Pas le temps", "Bientôt :)"];
    var html = '<tr><th>Tâche</th><th>Statut</th></tr>';
    for (var i = 0; i < taches.length; i++) {
        html += '<tr><td>'+taches[i]+'</td><td>'+statuts[i]+'</td></tr>'
    }
    document.getElementById('taches').innerHTML=html;
}