function afficherCoucou() {
    console.log("Coucou");
}

function afficherMotDePasse() {
    afficherLeMotDePasseCorrespondant('password');
}

function afficherLeMotDePasseCorrespondant(id) {
    var password = document.getElementById(id);
    var leMotDePasse = password.value;
    console.log(leMotDePasse);
}

function afficherMotDePasse2() {
    afficherLeMotDePasseCorrespondant('password2');
}

function afficherLesMotsDePasse() {
    afficherLeMotDePasseCorrespondant('password');
    afficherLeMotDePasseCorrespondant('password2');
}

function verifierMotsDePasse() {
    var motdepasse1 = document.getElementById('password').value;
    var motdepasse2 = document.getElementById('password2').value;
    if (motdepasse1 == motdepasse2) {
        alert('Les mots de passe sont identiques');
    }
    else {
        alert('Nop');
    }
    //document.getElementById('lebouton').disabled = true;
}