
var taches = [
    {
        tache : "Découvrir JS",
        statut : 'OK'
    },
    {
        tache : "Faire une appli de TODO-list",
        statut : 'En cours'
    },
    {
        tache : "Partir en pause",
        statut : 'Pas le temps'
    },
    {
        tache : "Finir ma TODO-list 2",
        statut : 'Bientôt :)'
    }
]

function initTableau() {
    var html = '<tr><th>Tâche</th><th>Statut</th></tr>';
    for (var i = 0; i < taches.length; i++) {
        html += '<tr><td>'+taches[i].tache+'</td><td>'+taches[i].statut+'</td></tr>'
    }
    document.getElementById('taches').innerHTML=html;
}

function exporter() {
    alert(JSON.stringify(taches, null, 2));
}