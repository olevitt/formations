### Le DOM (Document Object Model)
<!-- .slide: class="slide" -->  
![DOM](images/dom.gif)
* Arbre des composants HTML
* Modifiable par Javascript

----

### Modifier le DOM
<!-- .slide: class="slide" -->  
```Javascript
document.write('Hello :)');
```
Récupérer un élément par son id :
```HTML
<div id="mondiv">Avant</div>
```
```Javascript
document.getElementById('mondiv');
var div = document.getElementById('mondiv');
```
Modifier le contenu :
```Javascript
document.getElementById('mondiv').innerHTML = 'Après';
```

----

### Query selector
<!-- .slide: class="slide" --> 
Récupérer des éléments selon un critère 
```Javascript
var bouton = document.querySelector('button');
var tousLesBoutons = document.querySelectorAll('button');
console.log(tousLesBoutons.length);

var ayantLaClasse = document.querySelectorAll('.maClasse');
```