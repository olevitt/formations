## Environnement de développement
<!-- .slide: class="slide" -->  
Un bon artisan = de bons outils
* IDE
* Runtime
* Debug

----

### IDE
<!-- .slide: class="slide" -->  
Environnement de développement intégré
* Visual studio code
* Atom
* Webstorm
* Editeur texte

----

### Runtime
<!-- .slide: class="slide" -->  
Environnement d'exécution
* Un navigateur  
* Différences entre les navigateurs

----

### Debug : Console de développeur
<!-- .slide: class="slide" -->  
* Sur Chrome & Firefox : Ctrl + Maj + i  
  
Différents outils :  
* Console (logs)
* Code HTML (inspector)
* Sources (code source JS)
* Réseau
* Stockage (cookies, données ...)