## TP 1 : Hello Javascript
<!-- .slide: class="slide" -->  
Ce premier TP a pour objet de se familiariser avec les concepts, la syntaxe et l'environnement Javascript.

----

### 1.0 : L'environnement de dev
<!-- .slide: class="slide" -->  
* S'assurer de la bonne installation des différents outils mentionnés sur [cette page](setup.html)

----

### 1.1 : Hello Javascript !
<!-- .slide: class="slide" -->  
* A l'aide de l'IDE, créer un premier fichier HTML
* Ouvrir ce fichier HTML dans le navigateur
* Créer une deuxième fichier, leNomQueVousVoulez.js contenant le code suivant :  
```Javascript
alert('Hello world !');
```
* Inclure le fichier JS dans le fichier HTML
* Recharger le fichier HTML ... Hello world !

----

### 1.2 : Le dessous des cartes
<!-- .slide: class="slide" -->  
* Depuis le navigateur, consulter la source de votre programme
* Modifier, à la volée, le code HTML
* Vérifier, dans la console, qu'il n'y pas d'erreur
* (via l'IDE) remplacer l'alert par un message dans la console
* Vérifier que le message apparait bien

----

### 1.3 : Ma premère application : une calculatrice
<!-- .slide: class="slide" -->  
* Modifier le code HTML pour faire une interface ressemblant à :  
  
<div style="background-color:gray">
<input type="text" placeholder="2+2" id="calcul">
<button>Calculer</button>
<input type="text" placeholder="4" id="result" disabled="true">
</div>  
* S'assurer que tous les éléments ont bien des IDs
* Lors d'un click sur le bouton, logger un message de test
* Lors d'un click sur le bouton, logger le contenu du premier champ de texte : [ .value](https://stackoverflow.com/a/11563667)
* En utilisant la fonction [eval](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/eval), logger le résultat
* Mettre à jour le résultat
* Supprimer le bouton et mettre à jour le résultat en temps réel 

[Exemple de corrigé](TP1/calculette.html)

----

### 1.4 : Ma deuxième application : la TODO-list
<!-- .slide: class="slide" -->  
* Créer un tableau HTML qui représentera notre TODO-list
<div style="background-color:gray">
<table>
<tr><th>Tâche</th><th>Statut</th></tr>
<tr><td>Découvrir JS</td><td>Ok</td></tr>
<tr><td>Faire une appli de TODO-list</td><td>En cours ...</td></tr>
</table>
</div>
* S'assurer que ce tableau a un ID
* En JS, créer une structure de données contenant des tâches bidons
* En JS, créer une structure de données contenant des statuts bidons
* Ecrire une fonction qui génère le code HTML d'une ligne de tableau
* Ecrire une fonction qui génère le code HTML de toutes les lignes du tableau correspondant aux données précédentes
* Au chargement de la page, déclencher la fonction précédente et remplir le tableau

[Exemple de corrigé](TP1/todolist.html)

----

### 1.5 : Utilisation d'objets
<!-- .slide: class="slide" -->  
* Critiquer la structure de données de l'étape précédente
* Créer des objets tâches
* Adapter le code pour utiliser ces objets
* Ajouter une interface d'ajout de tâche
* Ajouter une interface de suppression de tâche
* Ajouter une interface d'export JSON (optionnel : CSV et autres formats)

[Exemple de corrigé](TP1/todolist2.html)
