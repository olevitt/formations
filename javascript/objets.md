### Notion d'objet
<!-- .slide: class="slide" -->  
Objectif : modéliser un étudiant
```Javascript
var prenom = 'Bob';
var nom = 'Le bricoleur';
var dateNaissance = '1990-02-26';
var notes = [19,18,19,20,20,17];
```
<!-- .element: class="fragment" -->

----

### Notion d'objet
<!-- .slide: class="slide" -->  
Objectif : modéliser 2 étudiants
```Javascript
var prenom = 'Bob';
var nom = 'Le bricoleur';
var dateNaissance = '1990-02-26';
var notes = [19,18,19,20,20,17];
```
<!-- .element: class="fragment" -->
```Javascript
var prenom2 = 'Bobette';
var nom2 = 'Le bricoleuse';
var dateNaissance2 = '1989-01-01';
var notes2 = [20,20,20,20,20,20];
```
<!-- .element: class="fragment" -->

----

### Notion d'objet
<!-- .slide: class="slide" -->  
Objectif : modéliser une classe de 10 étudiants
```Javascript
NOP
```
<!-- .element: class="fragment" -->
```Javascript
var etudiant = {
    prenom : 'Bob',
    nom : 'Le bricoleur',
    dateNaissance : '1990-02-26',
    notes : [19,18,19,20,20,17]
}
```
<!-- .element: class="fragment" -->
```Javascript
console.log(etudiant.nom);
console.log(etudiant.notes[2]);
etudiant.nom = 'Le bricoleur 2';
```
<!-- .element: class="fragment" -->

----

### Notion d'objet
<!-- .slide: class="slide" -->  
Objectif : modéliser une classe de 10 étudiants
```Javascript
var classe = {
    promo : '2020',
    etudiants : [
        {
            prenom : 'Bob',
            nom : 'Le bricoleur'
        },
        {
            prenom : 'Bobette',
            nom : 'La bricoleuse'
        }
    ]
}
```
<!-- .element: class="fragment" -->
```Javascript
console.log(classe.promo);
console.log(classe.etudiants.length);
console.log(classe.etudiants[1].nom);
```
<!-- .element: class="fragment" -->
