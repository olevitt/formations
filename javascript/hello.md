### Le trio du Web
<!-- .slide: class="slide" -->  
<h3>
    <li>HTML</li>
    <li>CSS</li>
    <li>Javascript</li>
</h3>

----

### Qui fait quoi ?
<!-- .slide: class="slide" -->  

* <b>HTML</b> : mise en forme
* <b>CSS</b> : style
* <b>Javascript</b> : action

Interprêté par le navigateur

----

### Séparation des responsabilités
<!-- .slide: class="slide" -->  

* [Qui fait quoi ?](exemples/responsabilites.html)

----

### Javascript : du code
<!-- .slide: class="slide" -->  

```Javascript
alert('Hello world');
```
```Javascript
console.log(2+2);
```
```Javascript
if (score > 400) {
    alert('Wow !');
}
```
```Javascript
var total = 0;
for (var i = 0; i < 100; i++) {
    total += i;
}
console.log(total);
```

----

### Inclure du Javascript
<!-- .slide: class="slide" -->  

```HTML
<head>
<script>
alert('Hello world');
</script>
</head>
```
ou  
```HTML
<head>
<script src="moncode.js"></script>
</head>
```
