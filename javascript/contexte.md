## Organisation du cours
<!-- .slide: class="slide" -->  

* Formation très pratique avec alternance forte entre théorie et TP  
* TP fil rouge : Todo-list

----

### Le plan
<!-- .slide: class="slide" -->  

* Découverte de Javascript
* Introduction à jQuery
* Le javascript moderne : Node.js & un framework (React)

----

### Programme
<!-- .slide: class="slide" -->  

* 5 novembre : Principes et premiers pas en Javascript
* 7 novembre : Découverte de jQuery
* 9 novembre : Première application complète
* 15 novembre : Node.js & npm, react
* 21 novembre : Application

----

### TP fil rouge
<!-- .slide: class="slide" -->  

* Todo-list
* Interfaçage avec le cours de PHP

----

### Projet / évaluation
<!-- .slide: class="slide" -->  

* ?