function echauffement() {
    return 42;
}

function carre(n) {
    return multiplication(n,n);
    //return n * n;
}

function multiplication(a,b) {
    return a * b;
}

function somme(n) {
    var somme = 0;
    for (var i = 0; i < n; i++) { 
        somme = somme + i;
        //somme += i;
    }
    return somme;
}

console.log(echauffement()); // 42
console.log(carre(12)); // 144
console.log(somme(12345)); // 76193340

function sommePairs1(n) {
    var somme = 0;
    for (var i = 0; i < n; i = i+2) { 
        somme = somme + i;
    }
    return somme;
}

function sommePairs2(n) {
    var somme = 0;
    for (var i = 0; i < n; i++) { 
        if (i%2 == 0) {
            somme = somme + i;
        }
    }
    return somme;
}

function populationAnneeSuivante(n) {
    var population = n;
    // La population double
    var nouvellePopulation = population * 2;
    //population = population * 2;

    // 1/3 de maladie
    var nombreDeMorts = nouvellePopulation / 3;
    nombreDeMorts = Math.floor(nombreDeMorts);
    var populationApresMaladie = nouvellePopulation - nombreDeMorts;
    return populationApresMaladie;
}

function populationFuture(anneeFuture,nombreDeChatsInitial) {
    var nombreDeChats = nombreDeChatsInitial;
    var nombreDanees = anneeFuture - 2018;
    for (var i = 0; i < nombreDanees; i++) {
        var nouveauNombre = populationAnneeSuivante(nombreDeChats);
        nombreDeChats = nouveauNombre;
    }
    return nombreDeChats;
}
console.log(populationFuture(2025,10));