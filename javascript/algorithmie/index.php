<?php
function echauffement() {
    return 42;
}

function carre($n) {
    return multiplication($n,$n);
    //return $n * $n;
}

function multiplication($a,$b) {
    return $a * $b;
}

function somme($n) {
    $somme = 0;
    for ($i = 0; $i < $n; $i++) { 
        $somme = $somme + $i;
        //$somme += $i;
    }
    return $somme;
}

function sommePairs1($n) {
    $somme = 0;
    for ($i = 0; $i < $n; $i = $i+2) { 
        $somme = $somme + $i;
    }
    return $somme;
}

function sommePairs2($n) {
    $somme = 0;
    for ($i = 0; $i < $n; $i++) { 
        if ($i%2 == 0) {
            $somme = $somme + $i;
        }
    }
    return $somme;
}

function populationAnneeSuivante($n) {
    $population = $n;
    // La population double
    $nouvellePopulation = $population * 2;
    //population = population * 2;

    // 1/3 de maladie
    $nombreDeMorts = $nouvellePopulation / 3;
    $nombreDeMorts = floor($nombreDeMorts);
    $populationApresMaladie = $nouvellePopulation - $nombreDeMorts;
    return $populationApresMaladie;
}

function populationFuture($anneeFuture,$nombreDeChatsInitial) {
    $nombreDeChats = $nombreDeChatsInitial;
    $nombreDanees = $anneeFuture - 2018;
    for ($i = 0; i < $nombreDanees; $i++) {
        $nouveauNombre = populationAnneeSuivante($nombreDeChats);
        $nombreDeChats = $nouveauNombre;
    }
    return $nombreDeChats;
}
echo populationFuture(2025,10);
?>