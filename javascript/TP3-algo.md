### TP 3 : algorithmie (Javascript + PHP)
<!-- .slide: class="slide" -->  
Ce TP est une collection d'exercices d'algorithmie. En commençant simple, en finissant dur :)  
Les solutions sont à implémenter dans les 2 langages, pour constater les éventuelles différences.  
A chaque fois, il est demandé de faire une fonction avec les (éventuels) paramètres mentionnés en gras et retournant le résultat.

----

### Un échauffement
<!-- .slide: class="slide" -->  
Ecrire une fonction renvoyant toujours 42.  
Exemples :
```Javascript
console.log(echauffement()); // 42
```
```PHP
echo echauffement(); // 42
```

----

### Mise au carré
<!-- .slide: class="slide" -->  
Ecrire une fonction calculant le carré d'un nombre entier <b>n</b> 
Exemples :  
```Javascript
console.log(carre(2)); // 4
console.log(carre(4)); // 16
console.log(carre(184)); // 33856
```
```PHP
echo carre(2); // 4
echo carre(4); // 4
echo carre(184); // 4
```


----

### Somme des entiers
<!-- .slide: class="slide" -->  
Ecrire une fonction calculant la somme des <b>n</b> premiers entiers.  
Exemples :  
```Javascript
console.log(sommeDesEntiers(2)); // 3
console.log(sommeDesEntiers(3)); // 6
console.log(sommeDesEntiers(184)); // 17020
```
```PHP
echo carre(2); // 3
echo carre(3); // 6
echo carre(184); // 17020
```

----

### Somme des carrés des entiers
<!-- .slide: class="slide" -->  
Ecrire une fonction calculant la somme des <b>n</b> premiers carrés des entiers.  
Exemples :  
```Javascript
console.log(sommeDesEntiersAuCarre(2)); // 1*1+2*2 = 5
console.log(sommeDesEntiersAuCarre(3)); // 1*1+2*2+3*3 = 14
console.log(sommeDesEntiersAuCarre(42)); // 1*1+2*2+...+42*42 = 23821
```

----

### Somme des entiers pairs
<!-- .slide: class="slide" -->  
Ecrire une fonction calculant la somme des <b>n</b> premiers entiers pairs.  
(a % 2 == 0 permet de tester si a est pair).    
Exemples :  
```Javascript
console.log(sommeDesEntiersPairs(2)); // 2
console.log(sommeDesEntiersPairs(3)); // 2
console.log(sommeDesEntiersPairs(42)); // 2 + 4 + 6 + ... + 42 = 420
```

----

### Population de chats
<!-- .slide: class="slide" -->  
On étudie une population de chats qui vivent en autonomie sur l'île miaouh.  
Au début, il y a 2 chats.  
Chaque année, la population double (naissances) mais, ensuite, un tiers (arrondi à l'inférieur) des chats meurent de maladie.  
Ecrire une fonction calculant la population de l'année suivante à partir de la population actuelle.  
Ecrire une fonction permettant d'obtenir la population des chats à n'importe quelle année future.  
Combien il y aura t'il de chats sur l'île miaouh dans 15 ans ?  
En quelle année la population de l'île dépassera t'elle 100 000 chats ? 1 million ? 100 millions ? 

----

### Qui est le plus long ?
<!-- .slide: class="slide" -->  
Ecrire une fonction comparant 2 chaines de caractères <b>str1</b> et <b>str2</b> et renvoyant 1 si <b>str1</b> est plus long que <b>str2</b>, 0 si ils font la même longueur, -1 si <b>str2</b> est plus long.
Exemples :  
```Javascript
console.log(comparerLongueur('Bonjour','Hello')); // 1
console.log(comparerLongueur('Bonjour','Bonjour')); // 0
console.log(comparerLongueur('Hello','Bonjour')); // -1
console.log(comparerLongueur('','')); // 0
```

----

### Palindrome
<!-- .slide: class="slide" -->  
Ecrire une fonction déterminant si une chaine de caractère <b>str</b> est un palindrome (ex : ete, kayak, rer, snobons)  
Exemples :  
```Javascript
console.log(palindrome('ete')); // true
console.log(palindrome('hiver')); // false
console.log(palindrome('snobons')); // true
console.log(palindrome('')); // true
```
```PHP
echo palindrome('ete'); // true
echo palindrome('hiver'); // false
echo palindrome('snobons'); // true
echo palindrome(''); // true
```

----

### Découpage en mots
<!-- .slide: class="slide" -->  
Ecrire une fonction renvoyant un tableau des différents mots composant une chaine <b>str</b> (on considère que 2 mots sont toujours séparés par au moins un espace)  
Exemples :  
```Javascript
console.log(decoupage('ete')[0]); // ete
console.log(decoupage('Bonjour, on est le 14 novembre.').length); // 6
console.log(decoupage('Bonjour           comment va ?')[1]); // comment
console.log(decoupage('').length); // 0
```
  
En déduire une fonction comptant le nombre de mots d'une chaine de caractères.

----

### Palindromes
<!-- .slide: class="slide" -->  
Ecrire une fonction comptant le nombre de palindromes dans une chaine de caractère <b>str</b>
Exemples :  
```Javascript
console.log(palindromes('ete kayak ete')); // 3
console.log(palindromes('toto tata titi')); // 0
console.log(palindromes('snobons')); // 1
console.log(palindromes('')); // 0
```

----

### Maximum
<!-- .slide: class="slide" -->  
Ecrire une fonction renvoyant le maximum d'un tableau d'entiers.
Exemples :  
```Javascript
console.log(sommeChiffresRepetes([2,9,3])); // 9
console.log(sommeChiffresRepetes([2])); // 2
console.log(sommeChiffresRepetes([])); // 0
console.log(sommeChiffresRepetes([-3,-23,-34])); // -3
```

----

### Somme des chiffres répétés
<!-- .slide: class="slide" -->  
Ecrire une fonction prenant en paramètre une chaine de caractère <b>str</b> constituée uniquement de chiffres et calculant la somme des chiffres qui sont suivis par le même chiffre.
Exemples :  
```Javascript
console.log(sommeChiffresRepetes('1')); // 0
console.log(sommeChiffresRepetes('11')); // 1
console.log(sommeChiffresRepetes('122')); // 2
console.log(sommeChiffresRepetes('133445933')); // 10 (3+4+3)
console.log(sommeChiffresRepetes('3893445835429722678558456317563893861752455542588369533636585887178232467588827193173595918648538852463974393264428538856739259399322741844613957229674619566966921656443476317729968764183945899765294481327998956154956571467872487576314549468261122281384513266834769436913544431258253346374641589492728885222652146158261225296144835682556133922436438188211288458692217737145834468534829945993366314375465767468939773939978272968388546791547526366348163672162245585168892858977723516752284597322176349412485116173844733679871253985762643852151748396593275274582481295864991886985988427966155944392352248314629138972358467959614279553511247863869663526823326467571462371663396188951696286916979923587358992127741723727623235238531991996999181976664226274715591531566495345212849683589582225465555847312199122268773923175183128124556249916458878785361322713513153175157855597289482439449732469754748544437553251412476225415932478849961897299721228198262823515159848941742786272262236888514421279147329383465929358896761449135917829473321834267122759371247338155787774952626616791265889922959653887288735233291968146648533754958199821789499914763279869931218136266492627818972334549751282191883558361871277375851259751294611921756927694394977764633932938573132221389861617195291742156362494769521829599476753198422283287735888197584327719697758442462886311961723849326959213928195182293316227334998926839139915138472514686689887874559367524254175582135318545912361877139367538434683933333264146289842238921989275112323681356256979576948644489986951538689949884787173194457523474156229389465725473817651516136514446513436419126533875125645855223921197481833434658264655912731133356464193251635637423222227273192628825165993827511625956856754776849919858414375874943572889154281862749595896438581889424559988914658387293414662361364793844213298677236787998677166743945812899526292132465751582925131262933636228593134861363493849168168765261647652342891576445292462341171477487223253795935253493869317616741963486473')); // 1174
```