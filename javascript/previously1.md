### Précédemment dans Javascript
<!-- .slide: class="slide" -->  
* HTML
* CSS
* Javascript

----

### 3 étapes
<!-- .slide: class="slide" -->
* index.js
```Javascript
alert('exemple');
```  
* index.html
```HTML
<button>Contenu exemple</button>
```  
* index.html
```HTML
<head>
<script src="index.js"></script>
</head>
```

----

### Modifier le HTML (DOM)
<!-- .slide: class="slide" -->  
```HTML
<div id="mondiv">Avant</div>
```

```Javascript
document.getElementById('mondiv').innerHTML = 'Après';
```
