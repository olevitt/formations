### Syntaxe
<!-- .slide: class="slide" -->  
* Syntaxe flexible, langage non typé
```Javascript
alert('toto')
console.log('Hello');
// Commentaire
/*
Commentaire multi-ligne
*/
```

----

### Variables
<!-- .slide: class="slide" -->  
```Javascript
var age = 18;
```
```Javascript
var nom = 'Bobby';
```
```Javascript
var technologies = ['HTML','CSS','Javascript'];
console.log(technologies.length);
console.log(technologies[0]);
```
/!\ var /!\

----

### Conditions
<!-- .slide: class="slide" -->  
```Javascript
var pseudo = 'toto';
if (pseudo.length <= 2) {
    alert('Le pseudo doit faire au moins 3 caractères');
}
else {
    alert('Bonjour '+pseudo);
}
```

----

### Boucles
<!-- .slide: class="slide" -->  
```Javascript
for (var i = 0; i < 9; i++) {
  console.log(i); // 0,1,2,3,4,5,6,7,8
}
```
```Javascript
var i = 2;
var stop = 2048;
while (i < 2048) {
  i = i * i + 1;
}
```

----

### Fonctions
<!-- .slide: class="slide" -->  
```Javascript
function addition(a, b) {
    return a + b;
}
```
```Javascript
var c = addition(2,3);
```