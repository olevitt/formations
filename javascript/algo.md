### Algorithmie
<!-- .slide: class="slide" -->  
Petite pause dans les notions web, focus sur l'algorithmie (JS & PHP).  

----

### Fonctions
<!-- .slide: class="slide" -->  
* Bloc cohérent de code
* "Machine"
* But déterminé
* Paramètres
* Valeur de retour  

```Javascript
function multiplication(a,b) {
    return a*b;
}
function carre(a) {
    return multiplication(a,a);
}
var c = multiplication(3,4);
console.log(c); // 12
console.log(carre(10)); // 100
```

----

### Variables
<!-- .slide: class="slide" -->  
```Javascript
var total = 0;
total = 20;
total = total + 10;
total += 10;
total++;
```

```PHP
$total = 0;
$total = 20
$total = $total + 10;
$total += 10;
$total++;
```

```Javascript
var message = 'Total : ';
message = message + total + ' points';
```

```PHP
$message = 'Total : ';
$message = $message . $total . ' points';
```

----

### Conditions
<!-- .slide: class="slide" -->  
```Javascript
var score = 200;
if (score >= 150) {
    alert('Super !');
}
else if (score >= 100) {
    alert('ok');
}
else {
    alert('bof');
}
```

```PHP
$score = 200;
if ($score >= 150) {
    echo 'Super !';
}
else if ($score >= 100) {
    echo 'ok';
}
else {
    echo 'bof';
}
```

----

----

### Boucles for
<!-- .slide: class="slide" -->  
```Javascript
var total = 0;
for (var i = 0; i < 10; i++) {
  total += i;
}
```

```PHP
$total = 0;
for ($i = 0; $i < 10; $i++) {
    $total += i;
} 
```

----

### Boucles while
<!-- .slide: class="slide" -->  
```Javascript
var i = 0;
while (i < 5) {
    console.log(i);
    i++;
}
```

```PHP
$i = 0;
while ($i < 5) {
    echo $i;
    $i++;
} 
```

----

### Tableaux ordonnés
<!-- .slide: class="slide" -->  
```Javascript
var fruits = ['Banane', 'Pomme', 'Kiwi'];
console.log(fruits.length); // 3
console.log(fruits[0]); // Banane
console.log(fruits[2]); // Kiwi
console.log(fruits[3]); // NE PAS FAIRE
fruits[0] = 'Fraise';
```

```PHP
$fruits = ['Banane', 'Pomme', 'Kiwi'];
echo count($fruits); // 3
echo $fruits[0]; // Banane
echo $fruits[2]; // Kiwi
echo $fruits[3]; // NE PAS FAIRE
$fruits[0] = 'Fraise';
```

----

### Chaines de caractères
<!-- .slide: class="slide" -->  
```Javascript
var phrase = 'Bonjour, je fais du javascript';
console.log(phrase.length); // 30
console.log(phrase[0]); // B
console.log(phrase[phrase.length - 1]); // t
phrase[0] = 'M';
console.log(phrase);
```

```PHP
$phrase = 'Bonjour, je fais du PHP';
echo strlen($phrase); // 23
echo $phrase[0]; // B
echo $fruits[strlen($phrase) - 1]; // P
$phrase[0] = 'M';
echo $phrase;
```

