### TP 2 : Fullstack (Javascript + PHP)
<!-- .slide: class="slide" -->  
Ce TP va manipuler l'ensemble des notions vues à la fois en Javascript et en PHP.  
Ce TP sera noté à partir du code déposé sur votre espace web personnel.  
Vous êtes libres des choix de design et artistiques. Le client n'est pas particulièrement regardant sur ça mais est très à cheval sur son <b>cahier des charges</b>.<br />
Ce TP est très long et contient beaucoup de choses à faire. Il n'est pas attendu que vous le finissiez entièrement. A vous de prioriser les fonctionnalités et de vous organiser comme vous le voulez.

----

### 1.0 : Objectif
<!-- .slide: class="slide" -->  
Vous travaillez pour une petite entreprise de 10 salariés, spécialisée dans la vente de boulons par internet.  
Votre chef a eu vent de vos compétences en développement et vous demande de lui construire un minisite, simple, qui sera utilisé pour le service après vente et les réclamations client.  
Le <b>cahier des charges général</b> est le suivant :    
* Le site comprendra un formulaire de contact
* Le formulaire de contact contiendra différents champs, qui seront détaillés dans les slides suivantes
* Au fur et à mesure que l'utilisateur rempli les champs, il doit avoir un retour sur les champs invalides (exemple : il n'a pas choisi d'objet ou son email est invalide)
* Lorsqu'un utilisateur valide le formulaire, si toutes les données sont valides (on vérifiera, on ne fait pas confiance à l'internaute), un email est envoyé au webmaster (vous pouvez utiliser votre email perso) avec les informations du formulaire
* En plus du mail, on aimerait garder une trace sur le serveur de tous les envois

----

### 1.1 : Le formulaire
<!-- .slide: class="slide" -->  
Le <b>cahier des charges</b> du formulaire est le suivant :  
* L'utilisateur <b>DOIT</b> préciser son email. Pour simplifier, on considère un email valide si et seulement s'il contient un @ (point bonus pour une vérification plus précise de la syntaxe de l'email).
* L'utilisateur <b>DOIT</b> choisir une catégorie pour son message. Il a le choix entre "Question", "Réclamation", "Prise de contact", "Autres".
* L'utilisateur <b>PEUT</b> préciser qu'il s'agit d'un message urgent.
* L'utilisateur <b>DOIT</b> rentrer un objet qui <b>DOIT</b> faire entre 2 et 30 caractères.  
* L'utilisateur <b>DOIT</b> rentrer un texte explicatif qui <b>DOIT</b> faire entre 20 et 2000 caractères.  
* Lorsque toutes les conditions sont réunies, l'utilisateur <b>PEUT</b> valider sa demande en cliquant sur un bouton.  
L'utilisateur <b>DOIT</b> être prévenu en temps réel (au fur et à mesure qu'il rempli le formulaire) des champs qui sont mal remplis (ex : l'objet fait plus de 30 caractères).

----

### 1.2 : Le traitement
<!-- .slide: class="slide" -->  
Lorsqu'un utilisateur a validé le formulaire précédent, on traite les données suivant le <b>cahier des charges</b> suivant :  
* Toutes les informations mentionnées à la slide précédente <b>DOIVENT</b> être revalidées (on ne fait jamais confiance au client)
* Si une information est invalide, on <b>DOIT</b> afficher une erreur. Point bonus : on affiche l'erreur / les erreurs correspondant au problème. Point bonus bonus : on redirige l'utilisateur vers le formulaire pour qu'il corrige.
* Si toutes les informations sont valides, on <b>DOIT</b> envoyer un email au webmaster (vous pouvez utiliser votre adresse email comme destinataire) avec TOUTES les informations précédentes. Un peu de mise en forme serait apprécié. [Fonction mail](http://php.net/manual/fr/function.mail.php)
* Si toutes les informations sont valides, on <b>DOIT</b> sauvegarder dans un fichier texte une trace de l'envoi. On considérera que sauvegarder l'adresse email de l'utilisateur dans un fichier texte est suffisant.
* Si toutes les informations sont valides (point bonus : et que les opérations précédentes se sont bien passées), on <b>DOIT</b> afficher à l'utilisateur un message en lui disant que le message a bien été transmis.


----

### 1.3 : Bonus
<!-- .slide: class="slide" -->  
Si vous êtes satisfaits de ce que vous avez fait en 1.1 et 1.2 et que vous l'avez uploadé sur votre espace web personnel, vous pouvez vous confronter aux fonctionnalités bonus suivantes :  
* Créer une page admin permettant de lister les envois qui ont été faits (en lisant les informations sauvegardées dans la partie précédente).  
* Restreindre l'accès à cette page à l'aide d'un mot de passe.