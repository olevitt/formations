### TP final, règles
<!-- .slide: class="slide" -->  
* Ce dernier TP mèle Javascript et PHP  
* Le contenu jugé sera le contenu présent dans votre espace personnel (demo.levitt.fr) le lundi 3 décembre à 13h.  
* Toutes les données doivent être validées côté serveur (PHP) et, autant que possible, côté client (Javascript).
* Les validations côté Javascript doivent être faites le plus en temps réel possible.
* Le style (CSS) n'est pas jugé mais vous êtes libres d'en ajouter.  
* Par contre, l'expérience utilisateur (facilité d'utilisation, formulaires intuitifs, messages explicites ...) rentre en ligne de compte pour la note Javascript  
* Les différentes parties sont relativement indépendantes, vous êtes libres de l'ordre dans laquelle vous les implémentez  
* Certaines fonctionnalités nécessitent des recherches complémentaires sur internet
* Si vous n'arrivez pas à implémenter une fonctionnalité, faites en une version simplifiée. On préférera une application complète et cohérente avec certaines simplifications qu'une application non fonctionnelle.

----

### TP final, cahier des charges
<!-- .slide: class="slide" -->  
On souhaite réaliser un mini réseau social, inspiré de Twitter.  
Ce site aura 4 pages, détaillées dans la suite :  
* Un formulaire d'inscription  
* Un formulaire de connexion  
* Une page pour consulter le flux de tweets
* Une page pour poster un nouveau tweet

----

### Le formulaire d'inscription
<!-- .slide: class="slide" -->  
Pour s'inscire, les utilisateurs doivent renseigner :  
Email, pseudonyme et mot de passe (2 champs pour confirmation)  

Les vérifications suivantes doivent être effectuées. Pour améliorer l'expérience utilisateur, on le préviendra le plus tôt possible des erreurs potentielles qu'il a pu faire.  
* Les emails doivent avoir une forme d'email (vérification minimum : présence d'un @)
* Les 2 mots de passe rentrés doivent être égaux
* Le mot de passe doit faire au minimum 6 caractères

<b>Si les données sont correctes</b>, on fait plusieurs choses :  
* On enregistre les données (pseudo:motdepasse) dans le fichier inscrits.txt à la suite des données existantes
* On envoie un mail à l'adresse email pour lui confirmer son inscription
* On affiche à l'utilisateur un message lui confirmant son inscription avec un lien vers la page de connexion

----

### Le formulaire de connexion
<!-- .slide: class="slide" -->  
Pour se connecter, les utilisateurs doivent renseigner :  
Pseudonyme, mot de passe

* Dans un premier temps, on considèrera que les données sont valides si le pseudonyme est égal au mot de passe
* Dans un second temps, on remplacera la vérification précédente par la vérification dans le fichier inscrits.txt que le pseudonyme et le mot de passe existent

Si les données sont valides :  
* On connecte l'utilisateur en mémorisant son pseudonyme et on lui affiche un lien vers la page de post de tweet  

Si les données sont invalides :  
* On signale à l'utilisateur que les données sont invalides et on lui affiche un lien vers le formulaire de connexion (ou on le redirige)

----

### Le flux de tweet
<!-- .slide: class="slide" -->  
La page de flux de tweet est publique (ne nécessite pas d'être connécté).  
* La page de flux de tweet affichera le contenu du fichier tweets.txt s'il y en a un, sinon on affichera "il n'y a pas encore de tweet".  
* On appréciera que les différents tweets soient séparés de façon claire visuellement.

----

### Le formulaire de post de tweet
<!-- .slide: class="slide" -->  
Le titre de la page contiendra le pseudonyme de l'utilisateur.  
Le formulaire contient un champ libre déstiné au contenu du tweet ainsi qu'un bouton valider.  
* <b>Ce formulaire n'est accessible que si l'utilisateur est connécté. S'il ne l'est pas, on affiche un message lui indiquant de le faire.</b>
* Le contenu doit faire minimum 1 caractère et maximum 140 caractères. L'utilisateur doit être prévenu au fur et à mesure qu'il écrit du nombre de caractères restants.
* Tant que le message est vide ou fait plus de 140 caractères, le bouton est désactivé.
* Lors de la validation du formulaire, si les données sont valides, on enregistre le message dans le fichier tweets.txt, à la suite des éventuelles données existantes.

----

### Points bonus
<!-- .slide: class="slide" -->  
Ces fonctionnalités, non prioritaires, sont des pistes d'amélioration de l'application pouvant rapporter des points supplémentaires :  
* Remplacer l'affichage des liens par des redirections lorsque ça a du sens
* Lors de l'inscription, si le pseudonyme est déjà pris, l'indiquer et refuser le formulaire
* Ajouter et afficher la date et l'heure des tweets
* Donner la possibilité à l'utilisateur, via un paramètre GET, de spécifier une recherche lors de la visualisation du flux de tweets. Si un terme de recherche est précisé, on n'affichera que les tweets comportant ce texte.
