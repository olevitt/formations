## Pleine puissance
<!-- .slide: class="slide" -->  
PHP contient déjà un certain nombre de fonctions bien utiles

----

## La fonction mail
<!-- .slide: class="slide" -->  
[http://php.net/mail](http://php.net/mail)  
3 paramètres :  
* Email du destinataire
* Sujet du message
* Contenu du message
```PHP
<?php
mail('toto@gmail.com', 'Sujet du message', 'Contenu du message');
?>
```  

----

## file_get_contents : lire un fichier
<!-- .slide: class="slide" -->  
[http://php.net/manual/fr/function.file-get-contents.php](http://php.net/manual/fr/function.file-get-contents.php)  
1 paramètre :  
* Le nom (ou chemin) du fichier à lire
```PHP
<?php
$contenu = file_get_contents('input.txt');
echo $contenu;
?>
```

----

## file_put_contents : écrire dans un fichier
<!-- .slide: class="slide" -->  
[http://php.net/file_put_contents](http://php.net/file_put_contents)  
2 paramètres :  
* Le nom (ou chemin) du fichier dans lequel écrire
* Le contenu à écrire
```PHP
<?php
file_put_contents('message.txt','Hop, je sauvegarde ça');
?>
```

----

## Application 1
<!-- .slide: class="slide" -->  
* Ecrire un script PHP écrivant dans un fichier txt tous les nombres de 0 à 1000

----

## Application 2
<!-- .slide: class="slide" -->  
* Ecrire un script PHP lisant le fichier précédent et calculant la somme de tous les nombres lus