## Environnement de développement
<!-- .slide: class="slide" -->  
* IDE
* Environnement d'exécution (runtime)

----

## IDE
<!-- .slide: class="slide" -->  
* Visual studio code
* PHPstorm
* Atom
* Eclipse
* Netbeans 

----

## Environnement d'exécution
<!-- .slide: class="slide" -->  
![HTTP](images/zoom-serveur.png)
* Navigateur
* Serveur Web
* PHP

----

## WAMP, LAMP, XAMPP ...
<!-- .slide: class="slide" -->  
* Pack tout en un
* A = Apache
* M = Mysql
* P = PHP  
  [XAMPP](https://www.apachefriends.org/fr/index.html)