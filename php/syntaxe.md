## Syntaxe
<!-- .slide: class="slide" -->  
```PHP
echo 'Hello world';
```

```PHP
$score = 20;
$pseudo = 'toto';
echo $pseudo;
```

```PHP
echo '<div>Exemple de HTML</div>';
```

----

## Mélange HTML / PHP
<!-- .slide: class="slide" -->  
```PHP
<h1>Bonjour <?php echo $pseudo; ?></h1>
<div><?php echo 'Exemple'; ?></div>
```

----

## Fonctions
<!-- .slide: class="slide" --> 
```PHP
function addition($a,$b) {
    return $a + $b;
}
```

```PHP
echo addition(2,3);
```

Documentation : [http://php.net/echo](http://php.net/echo)