## Les sessions
<!-- .slide: class="slide" -->  
Comment connecter un utilisateur ?

----

## Le problème
<!-- .slide: class="slide" -->  
![HTTP](images/client-server.png)  
* Chaque requête est indépendante

----

## La solution
<!-- .slide: class="slide" -->  
![HTTP](images/cookies.jpg)  

----

## Exemple réel : connexion
<!-- .slide: class="slide" -->
<input type="text" placeholder="Identifiant">  
<input type="password" placeholder="Mot de passe">  
<input type="submit" value="Se connecter"></button>  

----

## En PHP
<!-- .slide: class="slide" -->
* Tout (ou presque) est automatique
* Tableau clé / valeur $_SESSION

Obligation de rajouter 
```PHP
session_start();
```
en début de chaque page PHP

----

## Pas à pas
<!-- .slide: class="slide" -->
Etape 1 : sauvegarder des données en session  
connexion.php  
```PHP
session_start();
```
```PHP
$_SESSION['pseudo'] = 'Toto';
```

----

## Pas à pas
<!-- .slide: class="slide" -->
Etape 2 : récupérer les éventuelles données  
autrepage.php
```PHP
session_start();
```
```PHP
if (isset($_SESSION['pseudo'])) {
    echo 'Bonjour '.$_SESSION['pseudo'];
}
else {
    echo 'Connexion obligatoire';
}
```

----

## Application
<!-- .slide: class="slide" -->
* Créer un formulaire de connexion
* Si le pseudo est égal au mot de passe, le sauvegarder en session
* Créer une autre page PHP affichant le pseudo de l'utilisateur s'il est connécté ou un lien vers le formulaire de connexion s'il ne l'est pas