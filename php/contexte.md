<!-- .slide: class="slide" -->  
![Datacenter](images/datacenter.jpg)  

----

## Client - serveur
<!-- .slide: class="slide" -->  
![HTTP](images/client-server.png)  
* Côté serveur

----

## Zoom
<!-- .slide: class="slide" -->  
![HTTP](images/zoom-serveur.png)
* Transfert à PHP

----

## Hello world
<!-- .slide: class="slide" -->  
exemple.php
```PHP
<?php
echo 'Hello world';
?>
```
[Démo](http://localhost/exemple.php)