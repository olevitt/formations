## Utiliser la puissance de PHP
<!-- .slide: class="slide" -->  

----

## Un langage comme les autres
<!-- .slide: class="slide" -->  
```PHP
<?php
function carre($x) {
    return $x * $x;
}
?>
<body>
<div><?php echo carre(4); ?></div>
</body>
```  
[http://localhost/carre.php](http://localhost/carre.php)

----

## Des paramètres
<!-- .slide: class="slide" -->  
```PHP
<?php
function carre($x) {
    return $x * $x;
}
?>
<body>
<div><?php echo carre($_GET['nombre']); ?></div>
</body>
```  
[http://localhost/carre.php?nombre=4](http://localhost/carre2.php?nombre=4)

----

## Les formulaires
<!-- .slide: class="slide" -->  
Etape 1 : le formulaire HTML
```HTML
<form method="POST" action="formulaire.php">
<input type="text" name="nombre" placeholder="Nombre à mettre au carré" />
<input type="submit" value="Calculer" />
</form>
```
[http://localhost/formulaire.html](http://localhost/formulaire.html)

----

## Les formulaires
<!-- .slide: class="slide" -->  
Etape 2 : le script PHP
```PHP
<?php
function carre($x) {
    return $x * $x;
}
?>
<div>Le résultat est <?php echo carre($_POST['nombre']); ?></div>
```
[http://localhost/formulaire.html](http://localhost/formulaire.html)

----

## Application 1
<!-- .slide: class="slide" -->  
Ecrire un script PHP tel que http://localhost/ahah.php?nombre=5 affiche ahahahahahahah (avec un nombre de ah égal au nombre passé en paramètre).<br />
* http://localhost/ahah.php?nombre=2 => ahah
* http://localhost/ahah.php?nombre=5 => ahahahahah
* http://localhost/ahah.php?nombre=15 => ahahahahahahahahahahahahahahah

----

## Application 2
<!-- .slide: class="slide" -->  
* Ecrire un formulaire permettant à l'utilisateur de choisir son nombre de ah. Lorsque l'utilisateur valide le formulaire, on affiche le résultat (cf application précédente).
* Aller plus loin : en plus du résultat, réafficher le formulaire