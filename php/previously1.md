## Précédemment dans PHP
<!-- .slide: class="slide" --> 
![HTTP](images/client-server.png)  

----

## Syntaxe
<!-- .slide: class="slide" -->  
```PHP
echo 'Hello world';
```
```PHP
$chiffre = random_int(5,15);
```
```PHP
echo $chiffre;
```

----

## Mélange HTML / PHP
<!-- .slide: class="slide" --> 
```PHP
<h1>Bonjour <?php echo $pseudo; ?></h1>
<div><?php echo 'Exemple'; ?></div>
```

----

## Fonctions
<!-- .slide: class="slide" --> 
```PHP
function addition($a,$b) {
    return $a + $b;
}
```

```PHP
echo addition(2,3);
```
