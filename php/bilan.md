## Bilan
<!-- .slide: class="slide" -->  
* Mettre tout bout à bout

----

## Côté navigateur
<!-- .slide: class="slide" -->  
* Côté navigateur : OK
* HTML / CSS / JS
<br/>
<input type="password" id="passwordPHP" placeholder="Mot de passe"  /><br />
<input type="password" id="passwordPHP2" placeholder="Mot de passe (confirmation)" /><br />
<input type="button" onclick="alert(document.getElementById('passwordPHP').value+document.getElementById('passwordPHP2').value)" value="Clique !" />

----

## Comment ça fonctionne ?
<!-- .slide: class="slide" -->  
![HTTP](images/client-server.png)
* http://monsite.fr/mapage.html
* http://monsite.fr/style.css
* http://monsite.fr/monscript.js
* http://monsite.fr/sousdossier/image.png

Si un fichier n'existe pas : 404

----

## Exemple
<!-- .slide: class="slide" -->  
mapage.html
```HTML
<html>
<body>
<h1>Titre de la page</h1>
</body>
</html>
```
[Démo](http://localhost/mapage.html)

----

## Pareil en différent
<!-- .slide: class="slide" -->  
mapage.php
```HTML
<html>
<body>
<h1>Titre de la page</h1>
</body>
</html>
```
[Démo](http://localhost/mapage.php)

----

## Pareil, en différent
<!-- .slide: class="slide" -->  
mapage.php
```PHP
<?php
echo '<html>
<body>
<h1>Titre écrit par PHP</h1>
</body>
</html>';
?>
```
[Démo](http://localhost/mapage2.php)

----

## Pareil, mix des 2
<!-- .slide: class="slide" -->  
mapage.php
```PHP
<html>
<body>
<h1>
<?php
echo 'Titre écrit par PHP';
?>
</h1>
</body>
</html>
```
[Démo](http://localhost/mapage3.php)

