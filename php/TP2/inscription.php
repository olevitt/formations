<?php
    $erreur = '';
    if (strlen($_POST['pseudo']) < 3) {
        // Problème de longueur
        $erreur = 'Pseudo trop court';
    }
    if ($_POST['password'] != $_POST['password2']) {
        $erreur = $erreur.' Mots de passe différents';
    }
    if (strpos($_POST['email'],'@') === false) {
        $erreur = $erreur.' Manque un arobase';
    }

    if ($erreur != '') {
        echo 'Il y a eu une erreur : '.$erreur;
    }
    else {
        file_put_contents('utilisateurs.txt',$_POST['pseudo']);
        echo 'Inscription ok';
    }
?>