## Paramètres
<!-- .slide: class="slide" -->  
* 1 script PHP = 1 requête
* De base : pas de paramètres
* Lien : paramètres
* Formulaire : paramètres

----

## Paramètres
<!-- .slide: class="slide" -->  
* Clé : valeur
* [http://localhost/profil.php?id=4](http://localhost/profil.php?id=4)
* [http://localhost/profil.php?id=4&mode=visu](http://localhost/profil.php?id=4&mode=visu)
* En PHP, tableaux $_GET / $_POST
```PHP
$id = $_GET['id'];
```

----

## GET vs POST
<!-- .slide: class="slide" -->  
* 2 types de requêtes
* GET : paramètre(s) dans l'URL
* POST : paramètre(s) dans la requête
```HTML
<form method="POST" action="inscription.php">
<input type="text" name="pseudo" />
<button type="submit">Valider</button>
</form>
```

----

## URL rewriting
<!-- .slide: class="slide" -->  
* https://facebook.com/JustinBieber/
* https://www.programme-tv.net/news/tv/218357-burger-quiz-tmc-voici-la-premiere-star-qui-va-remplacer-alain-chabat-le-28-novembre-prochain/

=> Meilleur référencement  
=> Utilisation du serveur Web (ex : apache) pour rediriger https://twitter.com/justinbieber vers https://twitter.com?user=justinbieber