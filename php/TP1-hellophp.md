## TP 1 : Hello PHP
<!-- .slide: class="slide" -->  
Ce premier TP a pour objet de se familiariser avec les concepts, la syntaxe et l'environnement PHP.

----

### 1.0 : L'environnement de dev
<!-- .slide: class="slide" -->  
* S'assurer de la bonne installation des différents outils mentionnés sur [cette page](setup.html)
* En particulier, on testera que [http://localhost](http://localhost) répond bien

----

### 1.1 : Hello PHP
<!-- .slide: class="slide" -->  
* A l'aide de l'IDE, ouvrir le dossier INSTALL_XAMPP/htdocs
* Vider le fichier index.php et remplacer son contenu par un Hello world  
```PHP
<?php
echo 'Hello world';
?>
```
* Ouvrir [http://localhost](http://localhost) : Hello World !

----

### 1.2 : PHP / HTML
<!-- .slide: class="slide" -->  
* Dans le fichier index.php, rajouter, hors des balises "<?php ?>" du code HTML
* Tester
* Mélanger du HTML et du PHP pour afficher Hello world dans un div

----

### 1.3 : Une page dynamique
<!-- .slide: class="slide" -->  
* En utilisant la fonction [random_int](http://php.net/manual/fr/function.random-int.php), afficher un nombre aléatoire entre -5 et 5
* Si ce nombre est positif, l'afficher en rouge


----

### 1.4 : Un formulaire
<!-- .slide: class="slide" -->  
* Dans un fichier PHP, créer un formulaire HTML contenant au moins un champ texte pseudo (name="pseudo").
* Tester le formulaire dans le navigateur
* Dans ce même fichier PHP, rajouter l'instruction
```PHP
<?php
echo $_GET['pseudo'];
?>
```
* Tester la validation du formulaire dans le navigateur
* Si le pseudo est égal à admin, afficher un message de bravo en gras. Sinon, afficher une erreur.