## TP 2 : Formulaire PHP
<!-- .slide: class="slide" -->  
Ce deuxième TP a comme but d'apprendre à traiter les retours d'un formulaire HTML en PHP.  
On souhaite réaliser un formulaire d'inscription dont certaines données seront validées (mots de passe égaux, adresse email valide ...) et dont les données seront sauvegardées dans un fichier texte.

----

### 2.0 : Un formulaire HTML
<!-- .slide: class="slide" -->  
Notre code PHP s'attend à recevoir les données d'un formulaire. Ce formulaire va être défini en HTML (avec éventuellement du CSS pour le style et du Javascript pour le rendre plus dynamique).  
* Ecrire, dans un fichier .html, un formulaire HTML simple contenant un champ pseudo, 1 champ email, 2 champs mot de passe (on vérifiera plus tard que les mots de passe choisis sont égaux) et un bouton valider. 

----

### 2.1 : Envoi des données au PHP
<!-- .slide: class="slide" -->  
* Créer un fichier inscription.php. Il aura pour but de recevoir, valider et sauvegarder les données.
* Modifier le form HTML pour lui faire envoyer les données vers notre script inscription.php en utilisant l'attribut [action](https://www.w3schools.com/tags/att_form_action.asp) de la balise form
* S'assurer que les champs utiles ont bien un attribut name
* Dans inscription.php, renvoyer (afficher) le pseudo de la personne. Afficher aussi son email ainsi que les mots de passe choisis.

----

### 2.2 : Validation des données
<!-- .slide: class="slide" -->  
Pour s'inscrire sur notre site web il faut respecter un certain nombre de règles :  
-> Le pseudo doit faire au minimum 3 caractères  
-> Les 2 mots de passe doivent être égaux  
-> L'adresse email doit contenir un @  
* Implémenter, une par une, ces règles (si une règle n'est pas valide, on affiche NON -mieux, on affiche la règle en erreur-. Sinon on affichera OUI).  
* (optionnel) Rajouter, dans le formulaire, une case à cocher correspondant à l'acceptation du contrat d'utilisation du service (on fera comme si un contrat existait). Valider, côté PHP, que la case a bien été cochée. 

----

### 2.3 : Sauvegarde des données
<!-- .slide: class="slide" -->  
Maintenant qu'on est capable de valider les données, on voudrait, pour finaliser le processus d'inscription, sauvegarder les données de l'utilisateur.  
En général, on sauvegarderait les données dans une base de données de type SQL (par exemple, MYSQL, PostgreSQL, SQLServer ...). Pour simplifier, on sauvegardera ici les données dans des fichiers texte simples.  
* En utilisant la fonction [file_put_contents](http://php.net/manual/fr/function.file-put-contents.php), sauvegarder les données de l'utilisateur lorsque la validation a réussi.  
* Créer une nouvelle page PHP, admin.php, qui liste l'ensemble des utilisateurs inscrits (s'il y en a). On pourra utiliser la fonction [file_get_contents](http://php.net/manual/fr/function.file-get-contents.php)


----

### 2.4 : Un formulaire de connexion
<!-- .slide: class="slide" -->  
Déjà tout fait ? Il est temps de s'attaquer au formulaire de connexion.  
* Créer un formulaire HTML simple de connexion (pseudo, mot de passe).
* Faire pointer ce formulaire HTML vers un script PHP.
* Dans ce script PHP, récupérer les informations envoyées et les comparer à la "base" (fichier) des utilisateurs.  
* Afficher le résultat de la connexion : OK ou pas OK.  
* Déjà tout tout fait ? Il est temps de jeter un oeil aux sessions PHP pour garder l'utilisateur connécté : [session_start](http://php.net/manual/fr/function.session-start.php)