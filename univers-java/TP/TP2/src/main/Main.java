package main;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import core.Bonbon;
import core.Espece;
import core.Hyperball;
import core.Joueur;
import core.Pokeball;
import core.Pokemon;
import ia.IA;
import ia.IABrute;
import ia.IAGeniale;
import ia.IAMaline;

public class Main {

	public static void main(String[] args) {
		System.out.println("Hello world");
		
		Espece bulbizarre = new Espece();
		bulbizarre.setNom("Bulbizarre");
		bulbizarre.setNumero(1);
		bulbizarre.setType("PLANTE");
		bulbizarre.setPvBase(100);
		bulbizarre.setForceBase(10);
		
		Pokemon pokemon = new Pokemon(bulbizarre, "Mon bulbizarre");
		pokemon.setTaille(2);
		
		System.out.println(pokemon.getSurnom()+" : niveau "+pokemon.getNiveau());
		System.out.println(pokemon.toString()); 
		System.out.println(pokemon); // équivalent à la ligne précédente
		
		for (int i = 0; i < 100; i++) {
			pokemon.gagnerExperience(10);
			System.out.println(pokemon);
		}
		
		Bonbon bonbon = new Bonbon();
		bonbon.setNom("Dragibus");
		bonbon.setXp(4);
		
		pokemon.mangerBonbon(bonbon);
		System.out.println(pokemon);
		pokemon.mangerBonbon(bonbon);
		System.out.println(pokemon);
		
		Espece salameche = new Espece();
		salameche.setNom("Salamèche");
		salameche.setNumero(2);
		salameche.setType("FEU");
		salameche.setPvBase(50);
		salameche.setForceBase(15);
		
		Pokemon adversaire = new Pokemon(salameche, "Méchant salamèche");
		while (pokemon.getPv() > 0 && adversaire.getPv() > 0) {
			pokemon.attaquer(adversaire);
			adversaire.attaquer(pokemon);
		}
		
		System.out.println("Fin du combat");
		
		Pokemon gagnant = adversaire;
		if (adversaire.getPv() <= 0) {
			gagnant = pokemon;
		}
		
		gagnant.gagnerExperience(5);
		System.out.println("Victoire de "+gagnant);
		
		Random random = new Random();
		
		Espece especeRandom = new Espece();
		especeRandom.setNom("Espece aléatoire");
		especeRandom.setNumero(3);
		especeRandom.setType("EAU");
		especeRandom.setPvBase(random.nextInt(200));
		especeRandom.setForceBase(random.nextInt(50));
		
		Bonbon dragibus = new Bonbon();
		dragibus.setPrix(5);
		Joueur.getJoueur().getInventaire().getObjets().add(dragibus);
		
		Bonbon carambar = new Bonbon();
		carambar.setPrix(20);
		Joueur.getJoueur().getInventaire().getObjets().add(carambar);
		
		Pokeball pokeball = new Hyperball();
		pokeball.setPrix(50);
		Joueur.getJoueur().getInventaire().getObjets().add(pokeball);
		
		System.out.println(Joueur.getJoueur().getInventaire().getPrixTotal());
		
		Pokemon pokemon1 = new Pokemon(salameche, "Gentil salamèche");
		Pokemon pokemon2 = new Pokemon(salameche, "Méchant salamèche");
		for (int i = 0; i < 3; i++) {
			pokemon1.defendre();
		}
		
		Pokemon winner = fight(pokemon1, pokemon2);
		System.out.println("Victoire de "+winner.getSurnom());
		
		
		// Tournoi d'IA !
		List<IA> IAATester = Arrays.asList(new IABrute(), new IAGeniale(), new IAMaline());
		int[] scores = new int[] {0,0,0};
		for (int i = 0; i < IAATester.size(); i++) {
			for (int j = 0; j < IAATester.size(); j++) {
				if (i == j) {
					//combat entre 2 ia pareilles, on ne fait pas
				}
				else {
					IA gagnante = testIA(IAATester.get(i), IAATester.get(j));
					if (gagnante == IAATester.get(i)) {
						scores[i]++;
					}
					else {
						scores[j]++;
					}
				}
			}
		}
		
		for (int i = 0; i < IAATester.size(); i++) {
			System.out.println(IAATester.get(i).getClass().getName()+" : "+scores[i]);
		}
	}
	
	private static IA testIA(IA ia1, IA ia2) {
		Espece salameche = new Espece();
		salameche.setNom("Salamèche");
		salameche.setNumero(2);
		salameche.setType("FEU");
		salameche.setPvBase(50);
		salameche.setForceBase(15);
		
		Pokemon pokemon1 = new Pokemon(salameche, "Gentil salamèche");
		Pokemon pokemon2 = new Pokemon(salameche, "Méchant salamèche");
		
		Pokemon gagnant = fight(ia1,ia2,pokemon1,pokemon2);
		if (gagnant == pokemon1) {
			return ia1;
		}
		else {
			return ia2;
		}
	}
	
	/**
	 * Lance un combat entre les 2 pokémons en paramètre en utilisant des IA "brute"
	 * @param ia1
	 * @param ia2
	 * @param pokemon
	 * @param adversaire
	 * @return le gagnant
	 */
	private static Pokemon fight(Pokemon pokemon, Pokemon adversaire) {
		return fight(new IABrute(), new IABrute(), pokemon,adversaire);
	}
	
	/**
	 * Lance un combat entre les 2 pokémons en paramètre en les faisant agir à partir des 2 IA en paramètre
	 * @param ia1
	 * @param ia2
	 * @param pokemon
	 * @param adversaire
	 * @return le gagnant
	 */
	private static Pokemon fight(IA ia1, IA ia2, Pokemon pokemon, Pokemon adversaire) {
		while (pokemon.getPv() > 0 && adversaire.getPv() > 0) {
			ia1.agir(pokemon, adversaire);
			ia2.agir(adversaire, pokemon);
		}
		
		Pokemon gagnant = adversaire;
		if (adversaire.getPv() <= 0) {
			gagnant = pokemon;
		}
		
		return gagnant;
	}
}
