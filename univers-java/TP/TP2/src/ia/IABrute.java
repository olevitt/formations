package ia;

import core.Pokemon;

public class IABrute implements IA {

	@Override
	public void agir(Pokemon pokemonActif, Pokemon cible) {
		pokemonActif.attaquer(cible);
	}

}
