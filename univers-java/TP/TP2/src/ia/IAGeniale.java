package ia;

import core.Pokemon;

/**
 * Contrairement à son nom, cette IA n'est pas géniale.
 * Elle a plusieurs défauts et plusieurs pistes d'amélioration. A vous de les trouver !
 */
public class IAGeniale implements IA {

	@Override
	public void agir(Pokemon pokemonActif, Pokemon cible) {
		int nbDegats1 = estimerDegats(pokemonActif, cible);
		int nbDegats2 = estimerDegats(cible, pokemonActif);
		float survie1 = pokemonActif.getPv() / nbDegats2; //Estimation de la survie de notre pokemon en gardant le cap actuel
		float survie2 = cible.getPv() / nbDegats1; // Estimation de la survie du pokemon adverse en gardant le cap actuel
		
		if (survie1 >= survie2) {
			// On est bon, on tape
			pokemonActif.attaquer(cible);
		}
		else {
			// On s'en sortira pas, on active le mode défense
			pokemonActif.defendre();
		}
	}
	
	private int estimerDegats(Pokemon attaquant, Pokemon cible) {
		int nbDegats = attaquant.getForce() * (1-cible.getDefense() / 100);
		if (nbDegats <= 0) {
			nbDegats = 1;
		}
		return nbDegats;
	}
	
}

