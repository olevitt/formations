package ia;

import core.Pokemon;

public class IAMaline implements IA {

	@Override
	public void agir(Pokemon pokemonActif, Pokemon cible) {
		if (pokemonActif.getDefense() < 90) {
			pokemonActif.defendre();
		}
		else {
			pokemonActif.attaquer(cible);
		}
	}

}
