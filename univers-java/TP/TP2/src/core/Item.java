package core;

public class Item {

	private String nom;
	private int prix;
	private String typeUtilisation;
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getPrix() {
		return prix;
	}
	public void setPrix(int prix) {
		this.prix = prix;
	}
	public String getTypeUtilisation() {
		return typeUtilisation;
	}
	public void setTypeUtilisation(String typeUtilisation) {
		this.typeUtilisation = typeUtilisation;
	}
	
	
}
