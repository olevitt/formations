package core;

public abstract class Pokeball extends Item {
	
	private int efficaciteDeBase;


	public int getEfficaciteDeBase() {
		return efficaciteDeBase;
	}



	public void setEfficaciteDeBase(int efficaciteDeBase) {
		efficaciteDeBase = Math.min(efficaciteDeBase, 100);
		efficaciteDeBase = Math.max(efficaciteDeBase, 0);
		this.efficaciteDeBase = efficaciteDeBase;
	}
	
	public int getEfficaciteReelle() {
		return getEfficaciteDeBase() * getMultiplicateur();
	}

	public abstract int getMultiplicateur();


	public Pokeball(String nom) {
		setNom("Pokeball");
	}
}
