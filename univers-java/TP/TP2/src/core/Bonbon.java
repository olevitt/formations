package core;

public class Bonbon extends Item {

	private int xp;
	private boolean dejaConsomme = false;
	
	public int getXp() {
		return xp;
	}
	public void setXp(int xp) {
		this.xp = xp;
	}
	public boolean isDejaConsomme() {
		return dejaConsomme;
	}
	public void setDejaConsomme(boolean dejaConsomme) {
		this.dejaConsomme = dejaConsomme;
	}
	
	
}
