### Programme

<!-- .slide: class="slide" -->

- (re)Découverte de la POO
- Hello Java
- Dev en Java avec zoom sur la compilation, le build et le packaging
- Architecture applicative
- Intégration continue
- Mise en production / déploiement de l'application

---

### TP fil rouge

<!-- .slide: class="slide" -->

- Pokémon :)
- TP openworld
- Combats de pokemon
- Récupération des données https://pokeapi.co/
- Archivage des données
- Exposition sur un webservice
- Déploiement en production

---

### Projet / évaluation

<!-- .slide: class="slide" -->

A confirmer :

- Projet, sujet "libre"
- Techno : Java
- Jusqu'au bout : déploiement en prod
