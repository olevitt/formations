## TP 3 : Utilisation de spring
<!-- .slide: class="slide" -->  
Ce troisième TP a pour but de prendre en main spring et l'injection de dépendances  

----

### 3.1 : Initialiser le projet
<!-- .slide: class="slide" -->  
* Créer un nouveau projet Maven (ou en réutiliser un)
* Paramétrer ce projet pour compiler en Java 11 (ou 8 si vous avez qu'un JDK 8)  

----

### 3.2 : Interfaces & implémentations  
<!-- .slide: class="slide" -->  

On souhaite mettre en place un système de stockage des préférences utilisateur de notre application.  
En fonction de l'environnement (ex : serveur de dev, prod, version portable ...), on souhaite stocker les préférences de différentes façons (mémoire, fichier, base de données, webservice ...).

* Créer une classe `Preferences` contenant une information : `pseudo`
* Créer un service `PreferencesService` avec 2 méthodes : `save(Preferences)` et `Preferences load()`
* Ecrire une implémentation sauvegardant et chargeant l'info en mémoire  
* Ecrire une implémentation sauvegardant et chargeant l'info depuis un fichier `JSON` (En profiter pour découvrir [Jackson](https://www.baeldung.com/jackson-object-mapper-tutorial))
* Tester ces implémentations  

----

### 3.3 : Injection de dépendance
<!-- .slide: class="slide" -->  

* Ajouter les dépendances nécessaires pour l'utilisation de `ApplicationContext`  
* Initialiser un contexte contenant tous les beans du package et sous-packages
* Transformer vos implémentations en Beans
* Récupérer une instance de `PreferencesService`  

Quel est le problème ?  

----

### 3.4 : Choix dynamique de l'implémentation
<!-- .slide: class="slide" -->  

On veut que le choix de l'implémentation soit dynamique à partir de la configuration  

* En utilisant [System.getenv()](https://www.tutorialspoint.com/java/lang/system_getenv_string.htm), récupérer la valeur de la variable d'environnement `PATH`. Puis `STORAGE_MODE`. Faire varier cette variable (via votre OS, via les lancements IntelliJ ...).  
* En utilisant [@Value](https://www.journaldev.com/21448/spring-value-annotation), injecter la valeur directement dans un Bean  
* Sidequest : utiliser [@PropertySource](https://www.baeldung.com/spring-value-annotation) pour externaliser la valeur par défaut dans un fichier de configuration  
* En utilisant [@Conditional](https://javapapers.com/spring/spring-conditional-annotation/), écrire une logique de choix de l'implémentation en fonction de la valeur du paramètre `STORAGE_MODE`  

----

### New game +
<!-- .slide: class="slide" -->  

* Découvrir les préceptes de `12 factor app` (https://12factor.net/) et, en l'occurence, le point numéro III. Config
* Utiliser les fonctionnalités offertes par Spring boot sur l'externalisation de la configuration : https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config  
* Utiliser les annotations supplémentaires de Spring boot, en particulier @ConditionalOnProperty, pour simplifier le code