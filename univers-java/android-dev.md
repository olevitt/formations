## Développement Android
<!-- .slide: class="slide" -->  

----

### Pré-requis
<!-- .slide: class="slide" -->
* OS au choix
* SDK
* IDE : Android Studio

----

### Le SDK
<!-- .slide: class="slide" -->
![](images/sdk.png)

----

### ADB
<!-- .slide: class="slide" -->
* Android Debugging Bridge
* Outil du SDK
* Communication ordinateur <=> appareil / émulateur
* Installation d’application, lancement, log . . .
* Intégré dans l’IDE ou ligne de commande
```
adb devices
```
```
adb logcat
```

----

### Niveaux d'API
<!-- .slide: class="slide" -->
![https://developer.android.com/about/dashboards/index.html](images/api-levels.png)

----

### Déployer l'application
<!-- .slide: class="slide" -->
* Une application android = un APK (équivalent d’un jar)
* Une application android doit être signée
* Attention à ne pas perdre la clé !
* Création et signature de l’APK simplifié par l’IDE

----

### Logcat
<!-- .slide: class="slide" -->
![](images/logcat.jpg)
```
adb logcat
```

----

### Log, utilisation
<!-- .slide: class="slide" -->
* Oublier System.out.println() !
* 5 niveaux de gravité : Error, Warn, Info, Debug, Verbose
* Possibilité dans adb logcat de filtrer par gravité et/ou TAG
* Pratique recommandée : un TAG par composant / classe
```Java
Log.i(TAG,"Message de log");
```
```Java
Log.e(TAG ,"Exception", e) ;
```

----

### Structure de projet
<!-- .slide: class="slide" -->
* Démo
* Gradle
* Manifest
* Système de ressources

----

## IHM
<!-- .slide: class="slide" -->

----

### Main
<!-- .slide: class="slide" -->
* Une application Android est un ensemble de composants
* Pas de méthode main
* Des points d’entrée multiples possibles
* Exemple : appli SMS

----

### Activity
<!-- .slide: class="slide" -->

* 1 activity ∼ un écran
* Une application peut avoir 0 − n activities
* A déclarer dans le manifest
* Créer une classe Java héritant de Activity  
![](images/activity.png)

----

### Les Views
<!-- .slide: class="slide" -->
Une vue = un élément à l’écran
* TextView = Un texte
* EditText = Un champ de texte remplissable
* ImageView
* Button
* CheckBox
* Plein d’autres views de base dans android
* Possibilité de créer ses propres views en héritant (directement
ou indirectement) de View

----

### Les Viewgroups
<!-- .slide: class="slide" -->
![](images/viewgroup.png)
* LinearLayout
* RelativeLayout
* ListView
* Plein d’autres
* Les vôtres

----

### L'organisation d'une activity : les layouts
<!-- .slide: class="slide" -->
```XML
<LinearLayout
  xmlns:android="http://schemas.android.com/apk/res/android"
  android:layout_width="matchparent"
  android:layout_height="matchparent">

<TextView
android:layout_width="wrapcontent"
android:layout_height="wrapcontent"
android:text="@string/helloworld" />

</LinearLayout>
```
* Ils définissent l’organisation des vues
* Ils sont définis en XML dans le dossier res/layout
* @string fait référence à une ressource de strings.xml
* Eviter au maximum de modifier / créer les layouts au runtime

----

### Cycle de vie d'une Activity
<!-- .slide: class="slide" -->
<img src="images/activity_lifecycle.png" width="40%">

----

### Créer une Activity : étendre Activity
<!-- .slide: class="slide" -->
```Java
public class MainActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }
}
```
* onCreate est appelé à la création de l’activity (cf cycle de vie)
* appel obligatoire à super.onCreate
* savedInstanceState contient les informations en cas
de relance de l’activity
* savedInstanceState est null s’il s’agit du premier lancement

----

### Créer une activity : la déclarer dans le manifest
<!-- .slide: class="slide" -->

```XML
<activity android:name="fully.qualified.package.name.MainActivity"
  android:label="Accueil">
  <intent-filter>
      <action android:name="android.intent.action.MAIN" />
      <category android:name="android.intent.category.LAUNCHER" />
  </intent-filter>
</activity>
```

* Balise Activity à l’interieur de la balise Application
* On précise la classe Java correspondante

----

### Manipuler les éléments de l'UI en Java
<!-- .slide: class="slide" -->

Etape 1 : donner un identifiant à la vue
```XML
<LinearLayout
  xmlns:android="http://schemas.android.com/apk/res/android"
  android:layout_width="matchparent"
  android:layout_height="matchparent"
  android:id="@+id/monlayout">

  <Button
    android:layout_width="wrapcontent"
    android:layout_height="wrapcontent"
    android:text="@string/helloworld"
    android:id="@+id/monbouton" />
</LinearLayout>
```
* @+id crée l'identifiant s'il n'existe pas déjà
* L’identifiant doit être unique dans la hiérarchie (sinon conflit)

----

### Manipuler les éléments de l'UI en Java
<!-- .slide: class="slide" -->

Etape 2 : récupérer les références vers les views
```Java
public class MyActivity extends Activity {
  ViewGroup layout = null;
  Button bouton = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    layout = (ViewGroup) findViewById(R.id.monlayout);
    bouton = (Button) findViewById(R.id.monbouton);
    bouton.setText("Profit :)");
  }
}
```
* findViewById renvoie un objet de type View : on cast
* si aucune vue n'a l’identifiant demandé, findViewById renvoie
null
