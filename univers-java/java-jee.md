## Java : serveur web
<!-- .slide: class="slide" -->  

----

### Le web ?
<!-- .slide: class="slide" -->
![](images/client-server.png)
<!-- .element: class="fragment" -->

----

### La requête
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```
GET /flights HTTP/1.1
Host: www.google.com
Referer: http://google.com
User−Agent: Mozilla/5.0 (X11; Linux x86_64) [...]
Cookie: yummy_cookie=choco; tasty_cookie=strawberry
Cle: valeur
```
```
POST / HTTP/1.1
Host: foo.com
Content−Type: application/x−www−form−urlencoded
Content−Length: 13
say=Hi&to=Mom
```
* Un verbe
* Des headers
* [Un contenu]

----

### La réponse
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

```Java
HTTP/1.1 200 OK
Date: Fri, 31 Dec 1999 23:59:59 GMT
Server: Apache/0.8.4
Content−Type: text/html
Content−Length: 59
Expires: Sat, 01 Jan 2000 00:59:59 GMT
Last−modified: Fri, 09 Aug 1996 14:21:40 GMT
Set−Cookie: sessionid=38afes7a8; httponly; Path=/

<TITLE>Exemple</TITLE>
<P>Ceci est une page dexemple.</P>
```

* Un code retour
* Des headers
* [Un contenu]

----

### En java  
Du plus lourd au plus léger
* Monolithes : JEE : Java Entreprise Edition  
* Frameworks : [spring-boot](https://spring.io/projects/spring-boot)  
* Frameworks légers : [Javalin](https://javalin.io/)

----

### JEE
<!-- .slide: class="slide" -->  
* Java EE (maintenant Jakarta EE)
* Extension de Java SE pour les "applications d'entreprises"
* Déployé sur un "servlet-container"
* Ex : Tomcat, Jetty, Glassfish, JBoss ...

----

### WAR
<!-- .slide: class="slide" -->  
* Web Application Archive
* Contenu d'un war
```XML
<packaging>war</packaging>
```
* Lancer l'application ?

----

### JSP
```HTML
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

	<!-- Access the bootstrap Css like this, 
		Spring boot will handle the resource mapping automcatically -->
	<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

	<!-- 
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
	<c:url value="/css/main.css" var="jstlCss" />
	<link href="${jstlCss}" rel="stylesheet" />

</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Spring Boot</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#about">About</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">

		<div class="starter-template">
			<h1>Spring Boot Web JSP Example</h1>
			<h2>Message: ${message}</h2>
		</div>

	</div>
	
	<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>
```

----

### Spring MVC
<!-- .slide: class="slide" -->  
* https://spring.io/guides/gs/spring-boot/

(Spring initializr : https://start.spring.io/)

----

### Frameworks légers : Javalin  
* https://javalin.io/

