## Java : industrialisation

<!-- .slide: class="slide" -->

---

### Notion de classpath

<!-- .slide: class="slide" -->

- Notion de classpath

```
java -cp . Main
```

- Unicité des classes

---

### Packaging : jar

<!-- .slide: class="slide" -->

- Java ARchive
- Contenu d'un jar
- jar exécutable

```
java -jar jeanmichel.jar
```

- Comment packager ?

---

### Gestion des dépendances : 101

<!-- .slide: class="slide" -->

- Quid des dépendances ?
- Et les dépendances des dépendances ?
- Et les conflits ?

---

### Maven to the rescue

<!-- .slide: class="slide" -->

- Fondation Apache
- "Software project management tool"
- Build & dépendances
- cli : mvn
- Conventions over configuration

---

### Repositories maven

<!-- .slide: class="slide" -->

- Dépôt local
- Dépôts centraux

---

### pom.xml : la vérité

<!-- .slide: class="slide" -->

- TOUTE la configuration du projet
- TOUTES les dépendances
- Profils et étapes de build
- Exemples de pom.xml

---

### Structure de projet

<!-- .slide: class="slide" -->

- pom.xml
- src/main/java : les sources (.java)
- src/main/resources : les ressources (images, data ...)
- target : résultats de compilation (.class, .jar ...)

---

## Qualité : Tests unitaires

<!-- .slide: class="slide" -->

---

### Principe

<!-- .slide: class="slide" -->

- Définition de cas de tests
- Exécution automatique
- Résultats binaires : OK / NOK
- GIVEN / WHEN / THEN

---

### En Java

<!-- .slide: class="slide" -->

- JUnit
- Deux dépendances à ajouter
- https://junit.org/junit5/docs/current/user-guide/#running-tests-build-maven

```
  <dependency>
        <groupId>org.junit.jupiter</groupId>
        <artifactId>junit-jupiter-api</artifactId>
        <version>5.7.1</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.junit.jupiter</groupId>
        <artifactId>junit-jupiter-engine</artifactId>
        <version>5.7.1</version>
        <scope>test</scope>
    </dependency>
```

- Tests dans src/test/java

---

### Test : exemple

src/test/java/pkg/MathUtilsSpec.java

```Java
public class MathUtilsSpec {

  @Test
  public void addTwoNumbers() {
    int a = 2;
    int b = 3;

    int total = MathUtils.addition(a,b);

    Assert.assertEquals(5,total);
  }
}
```
