## TP 5 : Intégration continue
<!-- .slide: class="slide" -->
Un TP pour mettre en place l'intégration en continue de notre application.

----

### 5.1 : Des préparatifs
<!-- .slide: class="slide" -->
Le CI consiste à automatiser des tâches (compilation, tests, packaging ...).  
Il faut donc d'abord lister les commandes à automatiser ainsi que les inputs / outputs de chaque étape.  
* Reprendre le projet du TP précédent  
  
Quelles sont les commandes correspondantes aux actions suivantes :  
* Lancer les tests  
* Packager l'application  

Quels sont les outputs de chacune de ces étapes ?


----

### 5.2 : Hello CI
<!-- .slide: class="slide" -->
Pour notre premier CI, on se propose d'utiliser `gitlab-ci`, l'outil de CI de `gitlab.com`.  
* Créez, reprenez ou demandez un compte gitlab.  
* Créez un dépôt  
* Pushez le code de votre projet sur ce dépôt  
* Pusher le fichier `.gitlab-ci.yml` suivant à la racine de votre dépôt :  

```YML
test:
    script:
        - ls
```  

* Suivre les logs du job

----

### 5.3 : Le choix des armes
<!-- .slide: class="slide" -->
Quel environnement est actuellement utilisé pour notre job ?  
De quel environnement avez vous besoin pour builder votre application ?  
* Trouver l'image qui vous convient sur [Dockerhub](https://hub.docker.com/)  
* Sidequest : tester l'image en local (nécessite docker) pour en vérifier le contenu  
* Sidequest + : utiliser [dive](https://github.com/wagoodman/dive) pour inspecter profondément l'image  
* Utiliser l'image pour notre job

----

### 5.4 : Let's build !
<!-- .slide: class="slide" -->
Mettre en place le CI suivant :  
* Lancer les tests unitaires  
* Si les tests unitaires sont ok, lancer le packaging de l'application  
* Exporter le `.jar` du job ([artifacts](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html) )  
* Sidequest : en utilisant l'API [release](https://docs.gitlab.com/ee/api/releases/#create-a-release), publier une release de l'application à chaque tag 

----

### New game +
<!-- .slide: class="slide" -->
* Mettre en place un CI équivalent à l'aide de `travis-ci`, `github actions` ou un système de CI équivalent  
* Mettre en place un CD en utilisant des webhooks, la connexion à un cluster kubernetes existant ou toute autre solution de CD
