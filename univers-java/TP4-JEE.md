## TP 4 : Java : Serveur web

<!-- .slide: class="slide" -->

Ce quatrième TP a pour but de créer un serveur web en Java.

---

### 4.1 : Hello world

<!-- .slide: class="slide" -->

- En utilisant la technologie de votre choix (Javalin, Spring boot, JEE pur), mettre en place un serveur web renvoyant `Hello world` sur `/`
- Packager votre application
- Lancer le serveur sans utiliser votre IDE

---

### 4.2 : Des endpoints un peu plus intéressants

<!-- .slide: class="slide" -->

- Créer un endpoint `/espece` qui renvoie les informations sur une espèce de pokemon  
  Dans la vraie vie, une requête GET sans paramêtre de filtre renvoie en général la liste des entitées
- Déplacer le endpoint précédent vers `/espece/xxx` et renvoyer les informations sur l'espèce `xxx`
- Mettre en cache les données de la pokeapi afin de ne pas faire n appels
- Renvoyer, sur `/pokemon`, une liste des pokémons de l'utilisateur (liste inventée)

---

### 4.3 : Un peu d'écriture

<!-- .slide: class="slide" -->

- On veut pouvoir ajouter de nouveaux pokemons capturés. Faire une requête `POST` à `/pokemon`. Dans un premier temps, afficher le nouveau Pokemon reçu dans la console.
- Maintenant, sauvegarder ce train dans la base de données de votre choix.

---

### 4.3 : Swagger / open-API

<!-- .slide: class="slide" -->

- Mettre en place la documentation de la spécification de votre API en utilisant Swagger (open-API)
- Vérifier que la documentation est utilisable avec swagger-ui
- Faire diffuser swagger-ui par votre serveur
- Sidequest : modifier le code de swagger-ui pour pointer, par défaut, sur votre spécification

---

### New game +

<!-- .slide: class="slide" -->

- Mettre en place des tests fonctionnels et d'intégration. En particulier, jeter un oeil à `mockmvc`
