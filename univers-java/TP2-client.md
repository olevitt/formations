## TP 2 : Pokémon Or / Argent (client HTTP)

<!-- .slide: class="slide" -->

Ce deuxième TP a pour but d'utiliser des dépendances pour construire une mini application de Pokédex.

---

### 1.1 : Initialiser le projet

<!-- .slide: class="slide" -->

- Créer un nouveau projet Maven (sans archetype)
- Paramétrer ce projet pour compiler en Java 11 (ou 8 si vous avez qu'un JDK 8)

---

### 1.2 : Sécuriser les données

<!-- .slide: class="slide" -->

Pour notre pokédex, nous allons avoir besoin des données sur les Pokémons.  
Le site https://pokeapi.co/ propose une API avec de nombreuses données sur les pokémons.

Avant d'ajouter une source de données à une application, il convient de vérifier les éléments suivants (non exhaustif) :

- L'API est elle publique / privée ?
- L'API est elle gratuite ?
- L'API impose t'elle des quotas d'utilisation ?
- L'API impose t'elle des conditions à la réutilisation des données ?
- L'API est elle fiable (disponibilité) ?
- L'API est elle opensource ?

Répondre à ces questions pour `pokeapi`.  
Une fois ces vérifications faites, on peut passer au prototypage. Créer (crafter) avec le client HTTP de votre choix (postman, insomnia, plugin vscode ...) quelques requêtes de récupération des informations.

---

### 1.3 : Ajout d'une dépendance

<!-- .slide: class="slide" -->

Un webservice expose les données via HTTP.  
La lib standard Java permet de faire des requêtes HTTP mais les bibliothèques apportent plus de confort.

- Sidequest : en utilisant [URL::openConnection](https://docs.oracle.com/javase/8/docs/api/java/net/URL.html#openConnection--), récupérer et afficher les informations sur `Dracaufeu` (No 6).
- Ajouter la dépendance vers le client HTTP de votre choix (ex : [OkHTTP](http://square.github.io/okhttp/))
- En utilisant ce client, récupérer et afficher les informations sur `Dracaufeu` (No 6).

---

### 1.4 : Intégration des données

<!-- .slide: class="slide" -->

- En créant les objets Java correspondant à votre modèle de données, transformer la réponse en objet Java.
- Afficher, dans la console, la liste des 100 premiers pokémons.

---

### 1.5 : Packaging

<!-- .slide: class="slide" -->

Maintenant que notre application est utilisable, il est temps de la packager.

- En utilisant maven, compiler et créer un jar contenant le code de l'application
- Exécuter votre application

```
java -cp monappli.jar pkg.ClasseMain
```

- Sidequest : paramétrer maven pour exporter un jar exécutable

---

### 1.6 : Tests

<!-- .slide: class="slide" -->

- Ecrire la Javadoc des méthodes publiques (hors getters / setters) pour mettre en valeur les inputs, outputs et comportements attendus.
- Ecrire des tests unitaires couvrant les comportements documentés.

---

### New game +

<!-- .slide: class="slide" -->

- Utiliser un moteur de (de)serialisation JSON (ex : [Jackson](https://github.com/FasterXML/jackson)) pour automatiser le mapping
- Mettre en place un système de logs (log4j2, logback, tinylog ...)
- Jeter un oeil et implémenter le [mutation testing](http://pitest.org/)
