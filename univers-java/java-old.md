## Java

----

### Pourquoi Java ?
<!-- .slide: class="slide" -->  

* "Write once, run anywhere"
* Orienté objet et typage fort
* Sun microsystems
* Racheté en 2009 par Oracle

----

### Historique
<!-- .slide: class="slide" -->  

* Java 1 : 23 Janvier 1996
* Java 8 (LTS) : 18 Mars 2014
* Java 9 : 21 Septembre 2017
* Java 10 : 20 Mars 2018  
* Java 11 (LTS) : 25 Septembre 2018  
* Java 12 : Mars 2019  
* Java 13 : Septembre 2019  
* Java 14 : Mars 2020
* Java 15 : 15 Septembre 2020  
* ...
* Java 17 (LTS) : Septembre 2021


----

### Des différences avec C# ?
<!-- .slide: class="slide" -->

* Pas vraiment
* https://www.youtube.com/watch?v=RnqAXuLZlaE
* [Comparison of C# and Java (Wikipedia)](https://en.wikipedia.org/wiki/Comparison_of_C_Sharp_and_Java)

----

### Le concept de JVM
<!-- .slide: class="slide" -->  
* Java virtual machine
* Bytecode Java
* JDK / JRE ?

----

### Java aujourd'hui
<!-- .slide: class="slide" -->  
* Au top des classements : http://pypl.github.io/PYPL.html, https://www.tiobe.com/tiobe-index/, http://githut.info/
* Desktop, Webservers, Mobile
* "Paralangages" : Kotlin, Scala ...

----

### La checklist
<!-- .slide: class="slide" -->  

* Un PC sous Windows / Mac OS / Linux
* Un JDK (openJDK ou oracle JDK)
* Un IDE au choix : IntelliJ, Eclipse (Emacs, vi, nano)

----

### Choix de l'IDE
<!-- .slide: class="slide" -->  

* Apports de l'IDE
* Eclipse
* IntelliJ
* Visual Studio Code

----

### Prêts ?
<!-- .slide: class="slide" -->  

----

### Hello world
<!-- .slide: class="slide" -->  
```C#
using System;
namespace HelloWorld
{
    class Hello
    {
        static void Main()
        {
            Console.WriteLine("Hello World!");
        }
    }
}
```
```Java
package mon.pkg;

public class MaPremiereClasse {

  public static void main(String[] args) {
    System.out.println("Hello world");
  }

}
```
<!-- .element: class="fragment" -->

----

### Source et compilation
<!-- .slide: class="slide" -->
* .java : code source  
* .class : code compilé
* Anatomie du JDK
