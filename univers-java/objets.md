# Programmation orientée objet (POO)
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## La problématique
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Modélisation de la situation  

![](img/diagramme-bdd.png)  
<!-- .element: class="fragment" -->
```Python
idEtudiant1 = 1
nomEtudiant1 = 'Léponge'
prenomEtudiant1 = 'Bob'
idNote1Etudiant1 = 1
idMatiereNote1Etudiant1 = 1
noteNote1Etudiant1 = 10
... 
```
<!-- .element: class="fragment" -->

----

## La classe
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Groupe cohérent de variables
* "1 classe ~= 1 table"  

```Python
class Etudiant:
    id = 1
    nom = 'Léponge'
    prenom = 'Bob'
```

----

## L'instance
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* "1 instance = 1 individu"
* 1 classe => n instances  
* Créer un individu = instancier
* Constructeur  

```Python
etudiant = Etudiant()
etudiant.nom = 'Sponge'
etudiant2 = Etudiant()
etudiant2.prenom = 'Bobette'
```