# Le langage le plus classe du monde
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Classe
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Tout n'est que classe 

```Java
public class Etudiant {
    public String nom = "Léponge";
    public String prenom = "Bob";
}
```  

----

## Instance
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
Etudiant etudiant = new Etudiant();
etudiant.prenom = "Bobette";
```  

----

## Constructeur
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Etudiant {
    public String nom = "Léponge";
    public String prenom = "Bob";

    public Etudiant(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }
}
```  
```Java
Etudiant etudiant = new Etudiant("Toto","titi");
``` 
* Par défaut : constructeur vide  
* Attention : constructeur plein = plus de constructeur vide

----

## Typage fort
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Tout a un type  

```Java
Etudiant etudiant = new Etudiant();
etudiant.prenom = "Toto";
etudiant.prenom = 14; // Erreur
String nouveauNom = "Bobette";
nouveauNom = 3; // Erreur
int unNombre = 0;
unNombre = 3.5; //Erreur
```  

----

## Typage fort, PARTOUT  
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
La fin de l'insouciance

```Java
public class Etudiant {
    public String nom;

    public void changerNom(String nom) {
        this.nom = nom;
    }
}
```  

----

## Types primitifs
| Type | Explication | Défaut | Taille | Exemples |
|------|-------------|--------| ----------------|------------------|
| int | entier | 0 | 32 bits | 42, -435 |
| boolean | booléen | false | 1 bit | true, false |
| char | 1 caractère | '' | 16 bits | 'L', 'b', '&', ' ' |
| float | décimal |  0.0 | 32 bits | 42.1303f, -3440.0f, 3.14f |
| double | décimal |  0.0 | 64 bits | 42.1303d, -1.23456e-300d |
| short | entier |  0 | 16 bits | 42, -435 |
| long | entier |  0 | 64 bits | 42L, -435L |
| byte | octet | 0 | 8 bits | 24, -89 |

----

## Types primitifs ou non ?
* Que 8 types primitifs
* Type primitif = minuscule (int)  
* Type non primitif = instance d'une classe  

```Java
int unEntier = 2;
char unCaractere = 'O';
float pi = 3.14f
boolean unBooleen = true;
```

```Java
String phrase = "Hello world";
String phrase2 = new String("Hello world :)");
Pokemon pokemon = new Pokemon(1, "Bulbizarre");
MaClasse monInstance = new MaClasse();
```

----

## null, [the billion dollar mistake](https://www.infoq.com/presentations/Null-References-The-Billion-Dollar-Mistake-Tony-Hoare)
* Type primitif : valeur par défaut
* Type non primitif : par défaut = null
* null = le néant
* null.attribut => crash
* null.methode() => crash
* NullPointerException

![](img/npe.png)



----

## Static  
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Attaché à la classe et non à l'instance  

```Java
public class VoiturePeugeot {
    public static String NOM_CONSTRUCTEUR = "PEUGEOT";

    public String modele = "206";
    public String couleur;
}
```
```Java
VoiturePeugeot.NOM_CONSTRUCTEUR;

VoiturePeugeot uneVoiture = new VoiturePeugeot();
uneVoiture.modele;
```

----

## Static, pour les méthodes
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
Attaché à la classe et non à l'instance  

```Java
public class Mathematiques {
    
    public static int addition(int a, int b) {
        //ici, this n'a pas de sens
        return a + b;
    }
}
```

```Java
int total = Mathematiques.addition(2 + 3);
```

----

## Conventions de nommage
* Résumé : [https://www.javatpoint.com/java-naming-conventions](https://www.javatpoint.com/java-naming-conventions)

----

## Hello world
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* Principe 1 : tout n'est que classe

```Java
public class ClassePrincipale {

}
```

----

## Hello world, principe 2
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* Principe 2 : le code c'est dans les méthodes

```Java
public class ClassePrincipale {

    public static void main(String[] args) {
        
    }
}
```

----

## Hello world, principe 3
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->  
* Principe 3 : cf principe 1 et 2

```Java
public class ClassePrincipale {

    public static void main(String[] args) {
        System.out.println("Hello world");
    }
}
```