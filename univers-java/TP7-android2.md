## TP 7 : Android, en mieux
<!-- .slide: class="slide" -->
Et si on affichait vraiment le tableau des départs ?

----

### Partie 1A : UI
<!-- .slide: class="slide" -->
La partie 1A est indépendante de la partie 1B, vous pouvez les faire dans l'ordre de votre choix.
* On veut afficher le tableau des prochains départs de la gare. Quelle vue vous parait adaptée ?
* Créer un layout avec cette vue et, au dessus, un champ de texte modifiable permettant de renseigner le nom de la gare à afficher.
* Comment remplir le tableau ? Créer un adapter contenant des données fictives.
* Créer un layout simple pour chaque item. On affichera au moins la destination et l'heure de départ.
* Sidequest : Faire que le champ de texte du nom de la gare soit autocomplétable à partir d'une liste de gares livrée avec l'application.

----

### Partie 1B : Les données
<!-- .slide: class="slide" -->
* En utilisant [OkHTTP](http://square.github.io/okhttp/), ou le client HTTP de votre choix, récupérer les informations sur les prochains départs de la gare de votre choix. Ecrire les résultats dans la log.  
Note : OkHTTP nécessite maintenant l'activation de [Java 8](https://developer.android.com/studio/write/java8-support)
* Sidequest bien cool : Utiliser [Stetho](http://facebook.github.io/stetho/) pour débogguer efficacement votre application et ses flux HTTP.
* Faire les choses proprement : faire les requêtes en asynchrone, rien ne doit être fait dans le thread main / UI.
* Mapper les résultats dans des objets Java contenant au moins la destination et l'heure de départ.
* Sidequest : sauvegarder les données obtenues en utilisant le système de persistance de votre choix afin d'y avoir accès en mode hors connexion.


----

### Partie 2 : FUUUUUUUUUUUUUUUUUUUUUUUUUSION
<!-- .slide: class="slide" -->
* Injecter les données du 1B dans l'interface du 1A. KIFFER.
* Rajouter un bouton refresh qui recharge les données.
* Lors d'un click sur un des trains, afficher une activity avec le détail de ce train.
* Depuis cette Activity, proposer un bouton de partage permettant à l'utilisateur de partager par tous les moyens possibles (mail, sms, whatsapp, facebook messenger ...) les infos de ce train.

----

### New game +
<!-- .slide: class="slide" -->
* Utiliser le GPS pour localiser la gare la plus proche (on comparera à une liste de gares en dur ou on utilisera l'API SNCF).
* Faire communiquer l'appli avec votre propre webservice plutôt qu'avec l'API SNCF directement.
