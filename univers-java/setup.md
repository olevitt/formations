# Pré-requis

Liste des outils qui seront utilisés pendant ce cours.  
Il est fortement conseillé de les avoir pré-installé et testé (des exemples de tests sont proposés dans les blocs de code) avant le début du cours.

## Système d'exploitation

Le système d'exploitation n'est pas imposé, le développement Java / Android est possible sur les 3 OS majeurs :

- Linux
- Windows
- Mac OS

Il est cependant conseillé de se familiariser autant que possible avec les technologies du monde Linux car elles sont centrales dans la conteneurisation.

## GIT

Git doit être disponible avec, de préférence, la possibilité de se connecter à des dépôts via SSH.

```
git
```

## JDK (Java development kit)

Un JDK est nécessaire pour compiler les applications.  
Le choix du JDK est libre (Oracle JDK, OpenJDK ...). Le plus simple est probablement de le télécharger sur https://adoptopenjdk.net/.  
Les deux versions LTS (Long Term Support) actuellement disponibles sont 8 et 11.  
Il est conseillé d'ajouter le dossier bin du JDK au PATH du système. Exemple pour Windows : https://docs.alfresco.com/4.2/tasks/fot-addpath.html

```
java -version
```

doit être reconnu et mentionner JDK (oracle, openJDK ou adoptopenJDK) ainsi que la version 1.8, 8 ou 11.  
Attention, le JRE (Java Runtime Environment) ne permet que de lancer des applications mais pas de les compiler et ne suffit donc pas, il faut bien télécharger le JDK.

## IDE

L'IDE n'est pas imposé.  
Les IDE conseillés sont [Eclipse](http://www.eclipse.org/) (version Java standard ou Java EE) et [IntelliJ](https://www.jetbrains.com/idea/) (version community, gratuite).  
Avantage d'Eclipse : 100% libre et open-source.  
Avantage d'IntelliJ : Plus moderne, plus apprécié des développeurs, équivalents disponibles pour Python (pyCharm), Android (Android studio), C (CLion) ...

```
L'IDE se lance bien.
```

## Maven

La configuration des projets sera déléguée à Maven. Il s'occupera en particulier de la gestion des dépendances et du build.  
Eclipse et IntelliJ supportent, par défaut et sans installation supplémentaire, Maven.

```
L'IDE propose maven dans les options de création de projet.
```

On peut cependant le télécharger en plus : https://maven.apache.org/download.cgi et ajouter le dossier bin de maven au PATH.  
Il sera alors possible d'utiliser maven directement depuis la ligne de commande :

```
mvn
```

## Katacoda (optionnel)

Le site Katacoda (https://katacoda.com/learn) propose de nombreux tutoriaux intéractifs.  
Pour les utiliser, il est nécessaire de posséder un compte (gratuit).

## Intégration continue

### IDE

Même si l'IDE est moins indispensable que pour le dev Java, il reste très utile pour l'écriture de scripts d'intégration continue.  
Visual Studio Code (https://code.visualstudio.com/) est un bon choix de par ses multiples plugins. Les plugins yaml (`redhat.vscode-yaml`), docker (`ms-azuretools.vscode-docker`) kubernetes (`ms-kubernetes-tools.vscode-kubernetes-tools`) sont conseillés.

### Gitlab

L'intégration continue sera illustrée principalement sur Gitlab en utilisant l'instance `gitlab.com`.  
Posséder un compte `gitlab.com` est conseillé pour pouvoir mettre en pratique.

## Mise en production

### Compte cloud

Les applications seront déployées sur le cloud en utilisant des machines virtuelles `ovhcloud` (https://www.ovhcloud.com/fr/public-cloud/).  
Pour toutes les séances sur ce sujet, des comptes temporaires seront fournis.
Pour le reste, vous êtes libres d'utiliser le fournisseur de cloud de votre choix (ovhcloud, google cloud platform, azure, AWS ...). La plupart ont des offres gratuites de bienvenue. Si le financement est un problème, n'hésitez pas à le dire, on proposera une solution gratuite.
