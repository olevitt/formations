# Pré-requis

Liste des outils qui seront utilisés pendant ce cours.  
Il est fortement conseillé de les avoir pré-installés et testés (des exemples de tests sont proposés dans les blocs de code) avant le début du cours.

## Système d'exploitation

Le système d'exploitation n'est pas imposé, le développement Java est possible sur les 3 OS majeurs :

- Linux
- Windows
- Mac OS

Il est cependant conseillé de se familiariser autant que possible avec les technologies du monde Linux car elles sont centrales pour l'intégration et la déploiement des applications.

## GIT

Git doit être disponible avec, de préférence, la possibilité de se connecter à des dépôts via SSH (création d'une paire de clé publique / privée).

```
git
```

## JDK (Java development kit)

Un JDK est nécessaire pour compiler les applications.  
Le choix du JDK est libre (Oracle JDK, OpenJDK ...). Le plus simple est probablement de le télécharger sur https://adoptium.net/ (anciennement AdoptOpenJDK).    
Parmis les versions proposées, privilégiez la version 11. La version 8 commence à être vraiment vielle et la LTS suivante, la 17 vient tout juste de sortir et n'est donc pas pleinement adoptée par les IDE, bibliothèques et outils.  
Il est conseillé d'ajouter le dossier bin du JDK au PATH du système. Exemple pour Windows : https://docs.alfresco.com/4.2/tasks/fot-addpath.html

```
java -version
javac -version
```

doit être reconnu et mentionner JDK (oracle, openJDK ou adoptopenJDK) ainsi que la version 11.  
Attention, le JRE (Java Runtime Environment) ne permet que de lancer des applications mais pas de les compiler et ne suffit donc pas, il faut bien télécharger le JDK.

## IDE

L'IDE n'est pas imposé.  
Les IDE conseillés sont [Eclipse](http://www.eclipse.org/) (version Java standard ou Java EE) et [IntelliJ](https://www.jetbrains.com/idea/) (version community, gratuite).  
Avantage d'Eclipse : 100% libre et open-source.  
Avantage d'IntelliJ : Plus moderne, plus apprécié des développeurs, équivalents disponibles pour Python (pyCharm), Android (Android studio), C (CLion) ...

```
L'IDE se lance bien.
```

## Maven

La configuration des projets sera déléguée à Maven. Il s'occupera en particulier de la gestion des dépendances et du build.  
Eclipse et IntelliJ supportent, par défaut et sans installation supplémentaire, Maven.

```
L'IDE propose maven dans les options de création de projet.
```

On peut cependant le télécharger en plus : https://maven.apache.org/download.cgi et ajouter le dossier bin de maven au PATH.  
Il sera alors possible d'utiliser maven directement depuis la ligne de commande :

```
mvn
```

## Katacoda (optionnel)

Le site Katacoda (https://katacoda.com/learn) propose de nombreux tutoriaux intéractifs.  
Pour les utiliser, il est nécessaire de posséder un compte (gratuit).

## Intégration continue

### IDE

Même si l'IDE est moins indispensable que pour le dev Java, il reste très utile pour l'écriture de scripts d'intégration continue.  
Visual Studio Code (https://code.visualstudio.com/) est un bon choix de par ses multiples plugins. Les plugins yaml (`redhat.vscode-yaml`), docker (`ms-azuretools.vscode-docker`) kubernetes (`ms-kubernetes-tools.vscode-kubernetes-tools`) sont conseillés.

### Gitlab

L'intégration continue sera illustrée principalement sur Gitlab en utilisant l'instance `gitlab.com`.  
Posséder un compte `gitlab.com` est conseillé pour pouvoir mettre en pratique.

## Mise en production

### Compte cloud

Un des objectifs de ce cours est d'étendre votre périmètre classique d'activité pour couvrir à la fois le développement, l'intégration en continue mais aussi d'aller jusqu'au déploiement en production.  
Pour cela, on utilisera des services (machines virtuelles, éventuellement cluster Kubernetes) dans le cloud.  
Dans le cadre des séances de cours, la mise en pratique aura lieu sur `ovhcloud` (https://www.ovhcloud.com/fr/public-cloud/) et des comptes temporaires vous seront fournis.  
Si vous souhaitez aller plus loin sur ce sujet (et vous y êtes très fortement invités), il vous est conseillé de créer un compte sur le fournisseur de cloud de votre choix (ovhcloud, Google Cloud Platform, Amazon Web Services, Microsoft Azure ...).  La plupart de ces services proposent des offres gratuites de bienvenue. 
