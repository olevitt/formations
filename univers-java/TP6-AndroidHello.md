## TP 6 : Hello Android
<!-- .slide: class="slide" -->
Petit TP de réalisation d'une application de conversion de devises.

----

### Hello world !
<!-- .slide: class="slide" -->
* Créer un nouveau projet Android en ciblant le "bon" niveau d'API
* Créer un émulateur compatible
* Lancer l'application sur un émulateur
* Sidequest : lancer l'application sur un appareil Android réel
* Exporter l'APK
* Signer et installer l'APK sur un émulateur / appareil

----

### Hello UI
<!-- .slide: class="slide" -->
* En utilisant un layout simple, ajouter un champ de texte remplissable (valeur à convertir) ainsi qu'un bouton (convertir) à l'interface
* [Lors d'un click sur le bouton](https://developer.android.com/reference/android/widget/Button.html), récupérer la valeur contenue dans le champ de texte
* Implémenter la logique complexe suivante : montant converti = montant * 1.2
* Afficher le résultat dans un champ de texte
* Afficher le résultat dans un [Toast](https://developer.android.com/guide/topics/ui/notifiers/toasts.html)

----

### Aller plus loin
<!-- .slide: class="slide" -->
* Changer de type de layout pour redesigner l'interface
* Utiliser un [spinner](https://developer.android.com/guide/topics/ui/controls/spinner.html) pour choisir parmis plusieurs devises
