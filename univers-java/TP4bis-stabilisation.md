## TP 4.5 : Stabilisation
<!-- .slide: class="slide" -->
Un TP pour stabiliser les connaissances acquises :  
* Serveur web en Java  
* Consommation de l'API SNCF
* Mapping JSON  
* Persistence SQL

----

### Hello world
<!-- .slide: class="slide" -->
* En utilisant [spring initializr](https://start.spring.io/), initialiser un webservice  
* Renvoyer "ok" sur le endpoint `/healthcheck`

----

### Des endpoints un peu plus intéressants
<!-- .slide: class="slide" -->
* Sur `/gare/xxx`, renvoyer le tableau des départs de la gare `xxx`
* Accepter un paramètre optionnel `date` correspondant à l'heure de départ
* Sur `/gare`, renvoyer la liste des gares disponibles

----

### Du cache
<!-- .slide: class="slide" -->
L'appel à l'API SNCF coûte relativement cher, on voudrait éviter de le faire plusieurs fois quand ce n'est pas utile.   
* Découpler les appels aux données statiques des appels dynamiques  
* Sauvegarder, dans le format qui vous parait le plus adapté, les données statiques
* Mettre en cache, dans le format qui vous parait le plus adapté, les données dynamiques  

----

### Swagger / open-API
<!-- .slide: class="slide" -->  
* Mettre en place la documentation de la spécification de votre API en utilisant open-API : https://github.com/springdoc/springdoc-openapi
* Vérifier que la documentation est utilisable avec swagger-ui  

----

### New game +
<!-- .slide: class="slide" -->
* Mettre en place des tests fonctionnels et d'intégration. En particulier, jeter un oeil à `mockmvc`
