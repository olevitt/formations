## Java : POO

----

### L'homme le plus class du monde
<!-- .slide: class="slide" -->  
```Java
public class Etudiant {

}
```
Typage fort :
```Java
Etudiant etudiant = new Etudiant();
```

----

### Attributs
<!-- .slide: class="slide" -->  
```Java
public class Etudiant {

  public String nom;
  public int annee;

}
```
```Java
Etudiant etudiant = new Etudiant();

etudiant.nom = "Bob";
etudiant.annee = 4;

System.out.println(etudiant.nom);
```

----

### Methodes
<!-- .slide: class="slide" -->  
```Java
public class Etudiant {

  public String nom;

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

}
```
```Java
Etudiant etudiant = new Etudiant();

etudiant.setNom("Bob");
System.out.println(etudiant.getNom());
```

----

### Constructeur
<!-- .slide: class="slide" -->
```Java
public class Etudiant {

  public String nom;
  public int annee = 1;

  public Etudiant() {

  }

  public Etudiant(String nom, int annee) {
    this.nom = nom;
    this.annee = annee;
  }

}
```
```Java
Etudiant etudiant = new Etudiant();
Etudiant etudiant2 = new Etudiant("Bobette",2);
```

----

### Encapsulation, visibilité
<!-- .slide: class="slide" -->
```Java
public class Etudiant {

  private String nom;

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

}
```
```Java
Etudiant etudiant = new Etudiant();
etudiant.nom //Ne compile pas
etudiant.getNom() //OK
```
* public, private, protected, default
* https://docs.oracle.com/javase/tutorial/java/javaOO/accesscontrol.html

----

### Héritage
<!-- .slide: class="slide" -->
* Pas d'héritage multiple  

```Java
public class Vehicule {
    private int x,y,z;
    //getters, setters
}

public class Avion extends Vehicule {

  public void decoller() {
    this.setZ(this.getZ() + 10);
  }
}
```
```Java
Vehicule vehicule = new Vehicule();
Avion avion = new Avion();
avion.decoller();
Vehicule avion2 = new Avion();
```

----

### Tout n'est qu'Object
<!-- .slide: class="slide" -->  
* Classe mère : Object    

```Java
public class Object {

  public String toString() {
      return nomClasse+"@"+adresseMemoire;
  }

  public boolean equals(Object otherObject) {
    return this == otherObject;
  }

  //...
}
```

----

### Interfaces, implémentations
<!-- .slide: class="slide" -->
```Java
public interface LecteurDVD {
  DVD eject();
  void choixChapitre(int chapitre);
  void pause();
}
```
```Java
public class LecteurSAMSUNG implements LecteurDVD, Lecteur3D {

  public DVD eject() {

  }

  public void choixChapitre(int chapitre) {

  }

  public void pause() {

  }

  public void activer3D() {

  }
}
```

----

### Static
<!-- .slide: class="slide" -->
Comme en C#
```Java
public class Animal {
  private static int nbAnimaux = 0;

  public Animal() {
    nbAnimaux++;
  }
}
```

```Java
public class MathUtils {
  public static int addition(int a, int b) {
    return a+b;
  }
}
```  
