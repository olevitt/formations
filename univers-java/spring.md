## Spring framework

----

### Pourquoi ?
<!-- .slide: class="slide" -->  

* Philosophie Java  
* Opinionated  
* Des libs pour à peu près tout

----

### La galaxie spring
<!-- .slide: class="slide" -->  

Overview : https://spring.io/projects/spring-framework  

----

### Spring core : dependency injection
<!-- .slide: class="slide" -->

```Java
ServiceImpl service = new ServiceImpl();
```  

```Java
Service service = new ServiceImpl();
```  
<!-- .element: class="fragment" -->

```Java
Service service = context.getBean(Service.class);
```  
<!-- .element: class="fragment" -->

```Java
@Autowired
Service service;
```  
<!-- .element: class="fragment" -->

----

### Bean
<!-- .slide: class="slide" -->  

```Java
@Service
public class BDDService implements DataService {
  public Data getData() {
    // ...
  }
}
```  

----

### Context
<!-- .slide: class="slide" -->  

```Java
public class Main {
  public static void main(String[] args) {
    ApplicationContext ctx = new AnnotationConfigApplicationContext(BDDService.class);
    Service service = context.getBean(Service.class);
  }
}
```  

----

### Package scan
<!-- .slide: class="slide" -->  

```Java  
package fr.levitt.example;

@Configuration
@ComponentScan
public class Main {
  public static void main(String[] args) {
    ApplicationContext ctx = new AnnotationConfigApplicationContext(Main.class);
    Service service = context.getBean(Service.class);
  }
}
```  