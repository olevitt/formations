# TP1 : Pokémon bleu / rouge

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

- Prise en main de la POO
- Modélisation simple d'un univers Pokemon
- Manipulation de Pokemons

---

## 1. Un peu d'objet

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

Avant de développer, il est important de modéliser l'univers.  
Ici, le cahier des charges est le suivant :

- Chaque Pokemon est d'une certaine espèce (ex : Bulbizarre, Salamèche ...). Une espèce possède les caractéristiques suivantes : un numéro, un nom et un type (EAU, FEU, PLANTE)
- Il peut y avoir plusieurs Pokemons de la même espèce (ex : plusieurs Bulbizarres). Chaque Pokemon possède des caractéristiques propres : un identifiant unique (entier commençant à 0 et augmentant de 1 pour chaque Pokemon), un surnom (immutable), une taille (en mètres, pas forcément entier), un niveau. Un Pokemon commence niveau 1. Le niveau maximum est fixé à 5.

<br />
=> Créer un diagramme de classe respectant ce cahier des charges

---

## 2. Place au code

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

- Créer un package "core" qui contiendra les objets correspondantes à votre modélisation
- Créer les classes correspondantes à votre modélisation et respectant le cahier des charges
- Dans la classe Main, instancier votre premier Pokemon. Lui attribuer les caractéristiques que vous voulez
- Afficher ses caractéristiques dans la console
- Ecrire la méthode [toString()](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html) de Pokemon pour générer une représentation sous forme de chaine de caractères de l'état du Pokemon

---

## 3. Un peu d'action

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

Les Pokemons vont maintenant pouvoir gagner de l'expérience

- Ajouter aux Pokemons un score d'expérience
- Ajouter aux pokemons une méthode gagnerExperience(). Cette méthode prendra en paramètre l'expérience gagnée
- Modifier la fonction main pour faire gagner 100 fois de l'expérience à votre Pokemon et afficher son état à chaque étape
- Les Pokemons peuvent maintenant manger des bonbons pour gagner plus d'expérience. Le gain en expérience varie en fonction du bonbon. Un bonbon contiendra un nom et le montant d'expérience qu'il fait gagner
- Ecrire la fonction mangerBonbon(). Elle prendra en paramètre le bonbon à manger et mettra à jour l'expérience du Pokemon.
- Adapter la méthode main pour faire manger votre Pokemon
- Un bonbon ne peut être consommé qu'une seule fois. Si un bonbon est reconsommé, il n'apporte pas d'expérience

---

## 4. Plus d'action

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

Les Pokemons sont maintenant capables de se battre !

- Ajouter aux Pokemons un nombre de points de vie ainsi qu'un score de force. Le nombre de points de vie et la force de départ d'un Pokemon dépendent uniquement de son espèce mais peuvent ensuite évoluer au cours du temps
- Ajouter aux Pokemons une fonction attaquer(). Cette fonction prendra en paramètre le Pokemon à attaquer. Quand un Pokemon attaque, il inflige autant de blessures que sa force à l'autre Pokemon
- Dans la fonction main, simuler un combat entre 2 Pokemons. Les règles du combat sont simples : les Pokemons attaquent l'un après l'autre jusqu'à ce que l'un des deux soit KO (points de vie <= 0)
- Un Pokemon qui gagne un combat reçoit 5 points d'expérience

---

## 5. Passer au niveau supérieur

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

Actuellement, les Pokemons ont un nombre de points d'expérience mais il n'y a pas de notion de niveau.

- Ajouter dans le code, à l'endroit qui vous parait le plus opportun, les seuils d'expérience requis pour passer les niveaux :

| Niveau | XP requis |
| :----- | --------: |
| 1      |         0 |
| 2      |         5 |
| 3      |        15 |
| 4      |        30 |
| 5      |       100 |

- Ecrire une fonction qui, à partir d'un nombre de points d'expérience, renvoie le niveau correspondant
- Lorsqu'un pokemon gagne de l'expérience, prendre en compte un éventuel changement de niveau

---

## 6. Un peu d'aléa

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

Pour l'instant, notre code est déterministe et les combats peu passionnants. Il est temps de changer un peu tout ça

- En utilisant la classe [Random](https://docs.oracle.com/javase/8/docs/api/java/util/Random.html), générer des caractéristiques aléatoires aux Pokemons et rendre les combats plus intéressants

---

## Aller plus loin

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

- En utilisant la classe [Scanner](https://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html), permettre à l'utilisateur de choisir les caractéristiques des pokemons
- En utilisant la fonctionnalité export de votre IDE, générer un jar exécutable de l'application. Le lancer via la commande suivante :

```
java -jar jeanmichel.jar
```

Félicitations, vous avez maintenant une application Pokemon fonctionnelle
