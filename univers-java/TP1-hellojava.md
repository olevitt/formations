## MiniTP 1 : Hello Java

<!-- .slide: class="slide" -->

Ce premier petit TP a pour objet de découvrir Java.

---

### 1.1 : Hello world

<!-- .slide: class="slide" -->

- Créer un nouveau projet Java
- Créer un nouveau package (dossier) main
- Dans ce package, créer une classe Main contenant un Hello world :

```Java
public class Main {

  public static void main(String[] args) {
    System.out.println("Hello world");
    }
}
```

- Exécuter le code

---

### Le dessous des cartes

<!-- .slide: class="slide" -->

- En utilisant javac, compiler la classe Main :

```sh
javac Main.java
```

- En utilisant java, exécuter cette classe :

```sh
java Main Main.class
```

---

### 1.2 : Une nouvelle classe

<!-- .slide: class="slide" -->

- Créer une classe Pokemon, y mettre quelques attributs
- Instancier, dans la méthode `main`, différentes instances de Pokemon
- Afficher dans la console les attributs des Pokemon
