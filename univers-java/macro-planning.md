## Macro planning
Macro-planning initial de la formation  
* 1 journée sur les bases du langage Java et l'environnement de dev
* 1 journée sur le web en Java (JEE / Reactive apps, clients HTTP)
* 1 journée de stabilisation et de découverte de différentes extensions / frameworks : JDBC, MongoDB, ORM, Spring ...
* 2 journées sur le développement mobile avec application sous Android (et découverte de Kotlin)
* 2 journées consacrées aux projets avec coaching actif  

Formation très pratique et dense avec alternance forte entre cours et TP  
TP fil rouge : Ecriture d'une application de gestion d'animalerie (Pet store)  
Evaluation ? Soutenances des projets en mode hackathon ?

----

### Jour 1 : Basics
<!-- .slide: class="slide" -->  
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Cours | Concept, historique, syntaxe, objets, IDE, packaging                         |
| 2              | TP | Hello IN'TECH                                  |
| 3       | Cours | Ingénierie de dev : Dépendances, Maven, Gradle, Debug, Tests, Build                                                         |
| 4      | TP |  Pet store, initialisation et premières features                                                         |

----

### Jour 2 : Web & JEE
<!-- .slide: class="slide" -->
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Cours | Servlets, JSP, JAX-RS, WAR, servlet containers                         |
| 2              | TP | Déploiement sur tomcat et IHM JEE                              |
| 3       | Cours |  Reactive apps (Vert.x), clients HTTP                                                        |
| 4      | TP |  Webservice                                                        |

----

### Jour 3 : Stabilisation & foire aux frameworks
<!-- .slide: class="slide" -->
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Cours | Persistance : JDBC, MongoDB, ORM (JPA, hibernate), fichiers, flux                          |
| 2              | TP | Persistance                                  |
| 3       | Cours | Galaxie Spring                                                          |
| 4      | TP |   Spring-core, spring-boot, spring-mvc                                                         |

----

### Jour 4 : Android
<!-- .slide: class="slide" -->
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Cours | Spécificités du dev mobile                         |
| 2              | Cours | Dev Android                                  |
| 3       | TP |   Pet store sur Android                                                      |
| 4      | TP |    Pet store sur Android                                                        |

----

### Jour 5 : Android
<!-- .slide: class="slide" -->
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Cours | Concepts avancés : services, broadcasts, persistance, HTTP        |
| 2              | TP | Pet store persistant et connécté aux webservices                           |
| 3       | Cours |    Kotlin                                                       |
| 4      | TP |     Migration du pet store vers Kotlin & interopérabilité                                                    |

----

### Jour 6 : Projet
<!-- .slide: class="slide" -->
Pré-requis de la journée : validation ou pré-validation des sujets de projet  
  
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Projet |                          |
| 2              | Projet |                                   |
| 3       | Projet |                                                           |
| 4      | Projet |                                                            |

----

### Jour 7 : Projet
<!-- .slide: class="slide" -->
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Projet |                          |
| 2              | Projet |                                   |
| 3       | Projet |                                                           |
| 4      | Projet |                                                            |
