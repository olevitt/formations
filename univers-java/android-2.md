## Android : aller plus loin
<!-- .slide: class="slide" -->  

----

### Précédemment dans Android
<!-- .slide: class="slide" -->

----

### Précédemment : les prérequis
<!-- .slide: class="slide" -->
* OS au choix  
* IDE : Android Studio
* SDK : adb

----

### Précédemment : un projet Android
<!-- .slide: class="slide" -->
* src/main/java
* src/main/res : la classe R  
* AndroidManifest.xml  
* build.gradle

----

### Précédemment : une appli
<!-- .slide: class="slide" -->
* Un ensemble de composants  
* Définis dans le manifest  
* Composant : Activity

----

### Précédemment : une Activity 1
<!-- .slide: class="slide" -->  
* Dans src/main/java
```Java
public class MainActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }
}
```

----

### Précédemment : une Activity 2
<!-- .slide: class="slide" -->
* Dans AndroidManifest.xml
```XML
<activity android:name="fully.qualified.package.name.MainActivity"
  android:label="Accueil">
  <intent-filter>
      <action android:name="android.intent.action.MAIN" />
      <category android:name="android.intent.category.LAUNCHER" />
  </intent-filter>
</activity>
```

----

### Précédemment : une Activity 3
<!-- .slide: class="slide" -->
* src/main/res/layout/activity_main.xml
```XML
<LinearLayout
  xmlns:android="http://schemas.android.com/apk/res/android"
  android:layout_width="matchparent"
  android:layout_height="matchparent"
  android:id="@+id/monlayout">

  <Button
    android:layout_width="wrapcontent"
    android:layout_height="wrapcontent"
    android:text="@string/helloworld"
    android:id="@+id/monbouton" />
</LinearLayout>
```

----

### Autre type de composants : les services
<!-- .slide: class="slide" -->
* Tâche en arrière plan
* Cycle de vie différent de celui d’une activity
* Pas d’interface graphique (sauf si une activity communique
avec le service)
* Exemples : lecteur MP3, client torrent, système de mise à
jour, taskkiller . . .

----

### Autre type de composants : les broadcast receivers
<!-- .slide: class="slide" -->
* Composant recevant les annonces système et les annonces des
autres applications
* Permet de réagir à certains événements
* Possibilité de lancer des activités ou des services depuis le
broadcast receiver
* Exemples : indicateur de batterie, antivirus, lancement au
démarrage, intercepter un appel ou un SMS reçu . . .

----

### Autre type de composants : les content providers
<!-- .slide: class="slide" -->
* Composant servant à distribuer les données
* Peu importe comment les données sont stockées (sqlite,
préférences, fichier . . . )
* Respecte une interface d’utilisation des données
* Peut permettre la récupération, la modification et/ou la
suppression de données
* Principalement destiné à une utilisation entre applications
* Gestion de la sécurité et des droits (permissions)
* Exemple : accès aux données système (SMS, contacts . . . )

----

### Les intents
<!-- .slide: class="slide" -->
* Messages asynchrones échangés entre applications ou entre
composants
* On déclare son intention, android réagit en conséquence
* Intent implicite / Intent explicite

----

### Intent implicite
<!-- .slide: class="slide" -->
* “Je veux envoyer un mail à toto@gmail.com avec comme objet "Salut""

```Java
public void envoyerMail() {
    Intent i = new Intent(Intent.ACTION_SEND);
    i.setType("message/rfc822");
    i.putExtra(Intent.EXTRA_EMAIL,"toto@gmail.com");
    i.putExtra(Intent.EXTRA_SUBJECT, "Salut");
    i.putExtra(Intent.EXTRA_TEXT, "Contenu pré-rempli du mail");

    try {
      startActivity(i);
    }
    catch (ActivityNotFoundException e) {
      Log.e(TAG,"Pas d'activity permettant de gérer les emails");
    }
}
```

----

### Intent explicite
<!-- .slide: class="slide" -->
* "Je veux lancer tel composant"
* Usecase de base : lancer une Activity de l'application

```Java
public void visualiserProfil() {
  Intent intent = new Intent(context, ProfilActivity.class);
  intent.putExtra("id",42);
  startActivity(intent);
}

//Et dans la classe ProfilActivity
protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  Intent intent = getIntent();
  int id = intent.getIntExtra("id",-1);
}
```

----

### Filter les intents, exemple
<!-- .slide: class="slide" -->
```XML
<activity
  android:name="com.toto.app"
  android:label="@string/app_name">
  <intent-filter>
    <data android:host="twitter.com"
      android:scheme="http" />
      <category android:name="android.intent.category.DEFAULT" />
      <category android:name="android.intent.category.BROWSABLE" />
      <action android:name="android.intent.action.VIEW" />
  </intent-filter>
</activity>
```

----

### Il existe plusieurs façons de stocker les données sur un appareil Android
<!-- .slide: class="slide" -->
* [SharedPreferences](https://developer.android.com/reference/android/content/SharedPreferences.html) : stockage de données primitives
* Fichiers : contenus sur la mémoire interne ou externe
* Bases de données : [SQLite](https://developer.android.com/training/data-storage/sqlite.html)
* Stockage distant : Webservice / cloud

Documentation android officielle sur le stockage de données :
http://developer.android.com/guide/topics/data/data-storage.html

----

### ListView
<!-- .slide: class="slide" -->
* Pattern ultra classique des applications mobile
* Affichage d’un nombre variable d’éléments avec, si besoin, une
scroll bar
* Récupération des events (press, longpress . . . ) sur les
éléments de la liste
* Chaque élement est affiché dans une view différente à
l’intérieur de la listview
* Pattern [Adapter](https://developer.android.com/reference/android/widget/ListAdapter.html)  

![](images/listview.jpg)

----

### Multithreading
<!-- .slide: class="slide" -->
* Thread main / UI
* ANR
* [StrictMode](https://developer.android.com/reference/android/os/StrictMode.html)
* [AsyncTaskLoader](https://developer.android.com/reference/android/content/AsyncTaskLoader.html)
* [AsyncTask](https://developer.android.com/reference/android/os/AsyncTask.html)
* [Activity::runOnUiThread](https://developer.android.com/reference/android/app/Activity.html#runOnUiThread(java.lang.Runnable))


----

### Stetho, la vie
<!-- .slide: class="slide" -->
* http://facebook.github.io/stetho/
* chrome://inspect

----

### Firebase, le mode facile
<!-- .slide: class="slide" -->
* https://firebase.google.com/

