## Java : syntaxe

----

### Types primitifs
<!-- .slide: class="slide" -->  
```Java
public static void main(String[] args) {
  int nbJoueurs = 5;
  boolean ok = false;
  char a = 'a';
  float virgule = 0.1f;
  long gros = 1000L;
  ...
}
```
[https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html)

----

### Equivalence primitif <> classe
<!-- .slide: class="slide" -->  
```Java
public static void main(String[] args) {
  Integer nbJoueurs = 5;
  Boolean ok = false;
  Character a = 'a';
  Float virgule = 0.1f;
  Long gros = 1000L;
  ...
}
```

----

### Autres types : classes
<!-- .slide: class="slide" -->
```Java
public class Personne {
    private int age;
    private String nom;
    private Personne pote;
    //getters & setters
}
```
```
Personne personne = new Personne();
Personne personne2 = new Personne();
personne.setPote(personne2);
personne.getPote().getNom();
```


----


### Conteneurs
<!-- .slide: class="slide" -->  
Array (ordonné, taille fixe) :  
```Java
String[] mots = new String[5];
mots[0]; //null
mots[2] = ":)";
```
List (ordonné, taille variable) :  
```Java
List<String> mots = new ArrayList<>();
mots.add(":)");
mots.size();
System.out.println(mots.get(0));
```

----

### Conteneurs, suite
<!-- .slide: class="slide" -->  
Set (non ordonné, éléments "uniques") :
```Java
Set<String> mots = new HashSet<>();
mots.add(":)");
mots.add(":)");
mots.size(); //1
```
Map (clé / valeur) :
```Java
Map<String, Integer> scores = new HashMap<>();
scores.put("bob",42);
scores.put("bobette",43);
scores.containsKey("bob");
scores.get("bob");
```

----

### Boucles
<!-- .slide: class="slide" -->
For :
```Java
for (int i = 0; i < mots.size(); i++) {
  System.out.println(mots.get(i));
}
```
"Foreach" :
```Java
for (String mot : mots) {
  //mots implements Iterable
  System.out.println(mot);
}
```
While :
```Java
while (condition) {

}
```

----

### Exceptions
<!-- .slide: class="slide" -->
Flag throws :
```
public String lireFichier() throws IOException, IllegalStateException {

}
```
Lever une exception :
```
public String lireFichier() throws IOException, IllegalStateException {
  throw new IllegalStateException();
}
```
Catcher une exception :
```
public String batch() {
  try {
    lireFichier();
  }
  catch (IllegalStateException e) {
    e.printStackTrace();
  }
}
```

----

### Commentaires, Javadoc
<!-- .slide: class="slide" -->
```Java
// Commentaire monoligne
```
```Java
/*
Commentaire multiligne
:)
*/
```
```Java
/**
     * Fait l'addition de deux entiers
     * @param a
     * @param b
     * @return l'addition a+b
     */
public static int addition(int a,int b) {
    return a+b;
}
```

----

### Conventions de nommage
<!-- .slide: class="slide" -->
```Java
package package.en.lowercase;

public class MaClasseEnCamelCase {

  private String attributEnLowerCamelCase;
  public static final int CONSTANTE_EN_CAPS_LOCK = 42;

  public void nomDeMethodeEnLowerCamelCase() {
    String variableEnLowerCamelCase;
  }
}

public interface InterfaceCommeLesClasses {

}
```
