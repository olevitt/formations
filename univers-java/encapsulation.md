# Encapsulation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* L’encapsulation désigne le principe de regrouper des données brutes avec un ensemble de routines permettant de les lire ou de les manipuler.  
<!-- .element: class="fragment" -->
* Ce principe est souvent accompagné du masquage de ces données brutes afin de s’assurer que l’utilisateur ne contourne pas l’interface qui lui est destinée.  
<!-- .element: class="fragment" -->
* L’ensemble se considère alors comme une boîte noire ayant un comportement et des propriétés spécifiés.  
<!-- .element: class="fragment" -->  

[https://fr.wikipedia.org/wiki/Encapsulation_(programmation)](https://fr.wikipedia.org/wiki/Encapsulation_(programmation%29)
<!-- .element: class="fragment" --> 

----

## Encapsulation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Regrouper les attributs au sein d'un objet
* Cacher l'implémentation => abstraction
* Accès uniquement par les accès publics
* Préserver l'intégrité

Distinction public / privé

----

## Encapsulation, Java
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Visibilité
* public / private (protected / default)  
* classe, méthode, attribut  

[https://docs.oracle.com/javase/tutorial/java/javaOO/accesscontrol.html](https://docs.oracle.com/javase/tutorial/java/javaOO/accesscontrol.html)

----

## Visibilité public : problématique
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Pokemon {
    public int xp = 0;
    public int niveau = 1;
}
```
```Java
Pokemon pokemon = new Pokemon();
pokemon.xp = 9999;
// pokemon.niveau est toujours à 1
```
* Incohérence des données : problème d'intégrité  

----

## Visibilité private : intégrité respectée
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Pokemon {
    private int xp = 0;
    private int niveau = 1;

    public int getXp() {
        return xp;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setXp(int xp) {
        this.xp = xp;
        this.niveau = Niveau.niveauCorrespondant(xp);
    }
}
```
```Java
Pokemon pokemon = new Pokemon();
pokemon.setXp(9999);
// pokemon.niveau est à jour
```
* Intégrité respéctée

----

## Accesseurs / mutateurs : getters / setters
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public class Pokemon {
    private int xp = 0;
    private int niveau = 1;

    public int getXp() {
        return xp;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setXp(int xp) {
        this.xp = xp;
        this.niveau = Niveau.niveauCorrespondant(xp);
    }
}
```
* getXxx(), setXxx(Type x)  
* Pour les booléens : isActif(), setActif(boolean actif)  
* Eclipse : menu source

