# TP 6 : Menu bestof

<!-- .slide: class="slide" -->

Ce TP consiste à exploiter tout ce qui a été vu précédemment pour faire une application complète.  
Le sujet choisi est une TODO-list (liste de choses à faire).  
Les fonctionnalités que l'on souhaite implémenter sont les suiventes :  
* Affichage de la liste des tâches  
* Ajout d'une tâche  
* Persistance des données  
* Possibilité de prendre une photo  

Une démo est disponible ici : https://demo.levitt.fr/levitt/todolist (cliquer sur tab1)
Et chaque étape correspond à un commit dans le dépôt formations.  
Vous pouvez donc accéder à la correction de chaque étape en faisant :  

```
git checkout idducommit
```

----

## 1. Le point de départ

<!-- .slide: class="slide" -->

Notre application se voulant être multiplateforme et exploiter les spécificités du mobile, on utilisera `Ionic` :  
```
ionic start --type react todolist tabs
```  

Commit correspondant : [ed375ef](https://bitbucket.org/olevitt/formations/commits/ed375ef13a43b6283346dfa9087e1d7dd9043ebf)

----

## 2. Modèle de données

<!-- .slide: class="slide" -->

* Créer un fichier `Tache.ts` dans un dossier `src/model` qui contiendra la définition de ce qu'est une tâche  
* Une tâche contiendra au moins un nom et une description  

```typescript
export interface Tache {
  titre: string;
  description: string;
}
```  

* Créer, dans un fichier `taches.json` dans un dossier `src/model/data` un tableau de tâches

Commit correspondant : [476e56e](https://bitbucket.org/olevitt/formations/commits/476e56ed25fbcab77c58755a38b08e490fe33b16)

----

## 3. Affichage

<!-- .slide: class="slide" -->

* Créer un fichier `Todolist.tsx` dans `src/components`  
* Y définir un composant React affichant une liste de Taches  
* Afficher, dans l'application, les taches du fichier `taches.json`  

Commit correspondant : [f1a22a3](https://bitbucket.org/olevitt/formations/commits/f1a22a33337c9d96e708e2c263e4c087e3a8a3cc)

----

## 4. Interactions

<!-- .slide: class="slide" -->

* Créer un formulaire contenant 2 champs de texte : `titre` et `description` et un bouton `ajouter`  
* Lors du click sur le bouton `ajouter`, créer une nouvelle tâche et l'ajouter à la liste des tâches de la `todolist` (l'ajouter à la liste en mémoire, il n'est pas possible de modifier `taches.json` à l'exécution)  

Commit correspondant : [f1f18e7](https://bitbucket.org/olevitt/formations/commits/f1f18e7acd2b05b07fd03282f342cea7d5d99cfe)

----

## 5. Persistance

<!-- .slide: class="slide" -->

* Ajouter https://ionicframework.com/docs/native/sqlite à notre application. Vérifier les plateformes supportées. Quelles sont les conséquences ?  
* A l'aide de la documentation, créer une table `tache` avec les 2 colonnes `titre` et `description`.  
* Lors d'un click sur le bouton `ajouter`, sauvegarder toutes les tâches en base de données.  
* Lors du chargement de l'application, charger la liste des `taches` depuis la base de données.  

Commit correspondant : [7e7a239](https://bitbucket.org/olevitt/formations/commits/7e7a2394584c494c8e8a141b8a2e6f635a44a387)

----

## 6. Prise de photo

<!-- .slide: class="slide" -->

* Modifier les structures et composants précédents pour ajouter une nouvelle propriété à `tâche` : `photo`. Il s'agira d'une `string` correspondant à l'image encodée en [`base64`](https://en.wikipedia.org/wiki/Base64)  
* Ajouter un bouton `prendre une photo`. On pourra par exemple utiliser une `IonIcon` en s'inspirant du code suivant :

```javascript
import { trash } from 'ionicons/icons'; // A choisir ici : https://ionicons.com/
import { IonIcon } from '@ionic/react';  

<IonIcon icon={trash}></IonIcon>
```  

* En utilisant le `hook` `useCamera` de https://github.com/ionic-team/ionic-react-hooks , permettra à l'utilisateur de prendre une photo et afficher le résultat (base64) dans la console  
* Lors d'un click sur le bouton, déclencher le code précédent  

Commit correspondant : [16d9320](https://bitbucket.org/olevitt/formations/commits/16d93207b7cab75597992fdaa77b13b6bf6bd3bd)

----

## Aller plus loin

<!-- .slide: class="slide" -->  
* Ajouter un bouton de suppression à chaque élément de la liste
* Utiliser l'OCR (Optical Object Recognition) d'ionic native : https://ionicframework.com/docs/native/ocr pour reconnaitre le texte de la photo  
* Utiliser la géolocalisation Ionic pour permettre à l'utilisateur d'ajouter les informations de localisation aux tâches : https://ionicframework.com/docs/native/geolocation  
* Utiliser [File](https://ionicframework.com/docs/native/file) ou `fetch` pour permettre à l'utilisateur d'importer / exporter ses tâches