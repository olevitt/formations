## Multiplateforme : React native
<!-- .slide: class="slide" -->  

https://reactnative.dev/  

"Learn once, write anywhere"

----

### Principe
<!-- .slide: class="slide" -->
* Applications cross-plateformes (Web, PWA, Android, iOS, Electron)
* Technologies web (HTML / JS / CSS + React)  

----

### Expo ?
<!-- .slide: class="slide" -->

https://expo.io/  

Choix pour ce cours : **ne pas utiliser expo**

----

### Hello world
<!-- .slide: class="slide" -->

https://reactnative.dev/docs/environment-setup  

(React native CLI Quickstart)

```shell
npx react-native init AwesomeProject
```

Dans un premier terminal, lancer le serveur métro :  

```shell
cd AwesomeProject
npx react-native start
```

et dans un autre terminal :  

```shell
npx react-native run-android
```

----

### Anatomie du projet
<!-- .slide: class="slide" -->

* `package.json`  
* Equivalent de `create-react-app`  
* Dossiers `android` et `iOS`  

----

### Et maintenant ?  
<!-- .slide: class="slide" -->

Le kiff : https://reactnative.dev/docs/getting-started  

`Components` & `API`  

----

### Support du web
<!-- .slide: class="slide" -->

https://github.com/necolas/react-native-web  

Exemple de squelette de projet avec support web : https://github.com/olevitt/demo-react-native-web