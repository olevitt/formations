const Score = ({score}) => {
    return <div>Score : {score}</div>
  }
  
  const ameliorations = [
    {nom : 'Supercookie', prix: 50},
    {nom : 'Supercookie 2', prix: 55},
    {nom : 'Megacookie', prix: 100},
  ]
  console.log(ameliorations)
  
  const Magasin = ({score}) => {
    
    return <div>Ameliorations : {ameliorations.map(amelioration => <Amelioration amelioration={amelioration} />)}</div>
  }
  
  const Amelioration = ({amelioration}) => {
    return <button>{amelioration.nom} {amelioration.prix}</button>
  } 
  
  //const unClick = () => 
    //console.log("toto")
  const Cookie = ({onClick}) => {
    return <div onClick={onClick}><img width ="200" height="200" src="https://www.cookingclassy.com/wp-content/uploads/2014/06/chocolate-chip-cookie-16-600x868.jpg"></img></div>
  }
  
  const Jeu = () => {
    const [score, setScore] = React.useState(45)
    return <><Score score={score} />
      <Cookie onClick={() => setScore(score+1)} />
      <Magasin score={score}/>
    </>
  }
  
  
  ReactDOM.render(
    <Jeu />,
    document.getElementById('root')
  );
  