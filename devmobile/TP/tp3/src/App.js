import React, { useState, useEffect } from "react";
import "./App.css";
import Liste from "./Liste";
//import data from "./data/fakedata.json";
import TextField from "@material-ui/core/TextField";

const Header = () => {
  return <div>Ma super appli</div>;
};

const ComposantFiltre = ({ onChange }) => {
  return (
    <>
      <TextField
        id="standard-basic"
        label="Filtre à appliquer"
        onChange={(event) => onChange(event.target.value)}
      />
    </>
  );
};

const Body = () => {
  const [data, setData] = useState(undefined);
  const [filtre, setFiltre] = useState(undefined);

  useEffect(() => {
    fetch("https://api.covid19api.com/summary")
      .then((resp) => resp.json())
      .then((json) => {
        setData(json);
        console.log(json);
      });
  }, []);

  return (
    <>
      <ComposantFiltre onChange={(newValue) => setFiltre(newValue)} />
      <Liste data={data ? data.Countries : undefined} filtre={filtre} />
    </>
  );
};

function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Footer />
    </div>
  );
}

const Footer = () => {
  return (
    <p>
      Application de qualité, conçue avec {"<"}3<br />
      Code source sur{" "}
      <a href="https://bitbucket.org/olevitt/formations">
        https://bitbucket.org/olevitt/formations
      </a>
    </p>
  );
};

export default App;
