import React from "react";

const Liste = ({ data, filtre }) => (
  <>
    {data ? (
      <ol>
        {data
          .filter((element) => !filtre || element.Country.includes(filtre))
          .map((element) => (
            <li>
              {element.Country} ({element.TotalConfirmed})
            </li>
          ))}
      </ol>
    ) : (
      <div>Chargement en cours</div>
    )}
  </>
);

export default Liste;
