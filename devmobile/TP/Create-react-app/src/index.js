import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

const Score = (props) => {
  console.log("Render de score");
  return (
    <div>
      Score : {props.score} {props.toto}
    </div>
  );
};

const objetsDisponibles = [
  {
    nom: "Four",
    cout: 100,
  },
  {
    nom: "Chocolat",
    cout: 10,
  },
  {
    nom: "Rouleau à patisserie",
    cout: 50,
  },
];

const Achetable = ({ objet, scoreActuel, onAchat }) => {
  return (
    <button onClick={() => onAchat(objet)} disabled={scoreActuel < objet.cout}>
      {objet.nom} ({objet.cout})
    </button>
  );
};

const PagePrincipale = () => {
  const [score, setScore] = React.useState(50);

  const handleClick = () => {
    setScore(score + 1);
  };

  const handleAchat = (objet) => {
    setScore(score - objet.cout);
  };

  return (
    <>
      <Score score={score} />
      {objetsDisponibles.map((objet) => (
        <Achetable objet={objet} scoreActuel={score} onAchat={handleAchat} />
      ))}
      <input
        onClick={handleClick}
        type="image"
        src="https://image.flaticon.com/icons/svg/808/808749.svg"
        alt="click"
      />
    </>
  );
};

ReactDOM.render(
  <React.StrictMode>
    <PagePrincipale />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
