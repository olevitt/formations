const MonChat = (props) => {
  console.log(props);
  return <img src={props.adresse} />;
};

var adresses = [
  "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg",
  "https://i.redd.it/z7sm56clyjo61.jpg",
];

const MonComposant = () => {
  return (
    <div>
      <MonChat adresse={adresses[0]} />
      <MonChat adresse={adresses[1]} />
    </div>
  );
};

const CookieClicker = (props) => {
  const [score, setScore] = React.useState(props.scoreInitial);
  const clickSurLeBouton = () => {
    setScore(score + 1);
  };

  return <button onClick={clickSurLeBouton}>{score}</button>;
};

ReactDOM.render(
  <div>
    <CookieClicker scoreInitial={100} />
    <MonComposant />
  </div>,
  document.getElementById("root")
);
