import React, { useState, useEffect } from "react";
import "./App.css";
import Liste from "./Liste";
//import localData from "./data.json";

function App() {
  const [filter, setFilter] = useState("");
  const [data, setData] = useState(undefined);

  useEffect(() => {
    // Si on n'a pas encore les données
    if (!data) {
      fetch("https://api.covid19api.com/summary")
        .then((resp) => resp.json())
        .then((json) => setData(json));
    }
  });

  return (
    <div className="App">
      <header className="App-header">
        <input
          type="text"
          placeholder="Filtrer"
          onChange={(event) => setFilter(event.target.value)}
        ></input>
        {data ? (
          <Liste data={data} filter={filter} />
        ) : (
          <div>Chargement en cours ...</div>
        )}
      </header>
    </div>
  );
}

export default App;
