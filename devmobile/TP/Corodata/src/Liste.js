import React from "react";

const Liste = ({ data, filter }) => {
  return (
    <ol>
      {data.Countries.filter(
        (country) => !filter || country.Country.includes(filter)
      ).map((country) => (
        <li>
          {country.Country} ({country.TotalConfirmed})
        </li>
      ))}
    </ol>
  );
};

export default Liste;
