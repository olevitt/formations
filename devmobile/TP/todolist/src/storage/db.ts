import { SQLite } from "@ionic-native/sqlite";
import { Tache } from "../model/Tache";
import { isPlatform } from "@ionic/react";
import defaultTasks from "../model/data/taches.json";

var inMemoryTasks = defaultTasks;

const initDBIfNeeded = async () => {
  const db = await SQLite.create({
    name: "data2.db",
    location: "default",
  });
  await db.executeSql(
    "CREATE TABLE IF NOT EXISTS taches(identifiant INTEGER PRIMARY KEY AUTOINCREMENT, titre TEXT, description TEXT, photo TEXT)",
    []
  );
  return db;
};

export const getTasks = async () => {
  if (!isPlatform("android") && !isPlatform("ios")) {
    // Pas sur mobile, comportement dégradé
    return inMemoryTasks;
  }

  const data = await (await initDBIfNeeded()).executeSql(
    "SELECT * FROM taches",
    []
  );
  const retour: Tache[] = [];
  for (let index = 0; index < data.rows.length; index++) {
    const element = data.rows.item(index);
    retour.push(element);
  }
  return retour;
};

export const addTask = async (tache: Tache) => {
  if (!isPlatform("android") && !isPlatform("ios")) {
    // Pas sur mobile, comportement dégradé
    inMemoryTasks = [...inMemoryTasks, tache];
    return inMemoryTasks;
  }

  await (
    await initDBIfNeeded()
  ).executeSql("INSERT INTO taches(titre,description,photo) VALUES(?,?,?)", [
    tache.titre,
    tache.description,
    tache.photo,
  ]);

  return getTasks();
};
