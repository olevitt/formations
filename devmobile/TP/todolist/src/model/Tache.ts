export interface Tache {
  titre: string;
  description: string;
  photo?: string;
}
