const Liste = ({ data }) => {
  if (!data) {
    return <div>Chargement en cours ...</div>;
  }
  return (
    <ol>
      {data.Countries.map((country) => (
        <li>
          {country.Country} : {country.TotalConfirmed}
        </li>
      ))}
    </ol>
  );
};

export default Liste;
