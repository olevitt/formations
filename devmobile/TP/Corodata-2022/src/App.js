import "./App.css";
import Liste from "./Liste";
import fakeData from "./data/fakedata.json";
import { useEffect, useState } from "react";

function App() {
  const [data, setData] = useState();
  useEffect(() => {
    fetch("https://api.covid19api.com/summary")
      .then((rep) => rep.json())
      .then((json) => setData(json));
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <Liste data={data} />
      </header>
    </div>
  );
}

export default App;
