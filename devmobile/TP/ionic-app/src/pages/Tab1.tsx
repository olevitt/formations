import React from "react";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import ExploreContainer from "../components/ExploreContainer";
import "./Tab1.css";
import Liste from "../components/Liste";
import fakeData from "../data/fakedata.json";
import Geoloc from "../components/Geoloc";

const Tab1: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 11</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 111</IonTitle>
          </IonToolbar>
        </IonHeader>
        <Geoloc />
        <Liste data={fakeData.Countries} filtre="" />
        <ExploreContainer name="Tab 6 page" />
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
