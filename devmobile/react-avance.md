# React 2
<!-- .slide: class="slide" -->  

----

## Précédemment dans le dev web
<!-- .slide: class="slide" -->  

* HTML
* Javascript
* CSS

----

## Professionnalisme
<!-- .slide: class="slide" -->  

* NodeJS : Javascript en dehors du navigateur  
* NPM : Node Package Manager
* `package.json` : configuration, dépendances du projet

----

## React S01E01
<!-- .slide: class="slide" -->  

* Bibliothèque Javascript pour construire des interfaces utilisateur  

```Javascript
const Hello = () => <div>Hello world</div>;
```  

----

## Dans les épisodes précédents 
<!-- .slide: class="slide" -->  

* Props

```Javascript
const Hello = ({pseudo}) => <div>Bonjour {pseudo}</div>;
```  

* State

```Javascript
const Hello = () => {
    const [compteur,setCompteur] = React.useState(0);
    return <><div>{compteur} clicks</div>
    <button onClick={() => setCompteur(compteur+1)}>+1</button>
    </>;
}
```  

----

## Cycle de vie React
<!-- .slide: class="slide" -->  

![Lifecycle](img/lifecycle.png)  

----

## Import / export
<!-- .slide: class="slide" -->  

Exports nommés :  

`Composant.js`

```Javascript
export const MonComposant = () => <div>Hello</div>;
export const maVariable = 42;
```  

Import nommé :  

`AutreFichier.js`

```Javascript
import { MonComposant, maVariable } from './Composant.js';
```  

----

## Imports / exports par défaut
<!-- .slide: class="slide" -->  

Exports nommés :  

`Composant.js`

```Javascript
export const MonComposant = () => <div>Hello</div>;
const maVariable = 42;
export default maVariable;
```  

Import nommé :  

`AutreFichier.js`

```Javascript
import leNombreMystere, { MonComposant } from './Composant.js';
```

----

## Javascript : opérations sur les listes / objets
<!-- .slide: class="slide" -->  

```Javascript
const utilisateurs = [{
    nom : "Bob"
}, {
    nom : "Bobette"
}];
```  

```Javascript
utilisateurs.forEach(utilisateur => console.log(utilisateur.nom));
const noms = utilisateurs.map(utilisateur => utilisateur.nom);
const utilisateursAuxNomsLongs = utilisateurs.filter(utilisateur => utilisateur.nom.length > 6);
const nbDeLettres = utilisateurs.reduce((total,utilisateur) => total + utilisateur.nom.length,0);
```  

----

## Javascript : requête HTTP
<!-- .slide: class="slide" -->  

[Fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)

```Javascript
fetch('https://catfact.ninja/fact');
```  

Promesse ?  

----

## Javascript : HTTP / promesses
<!-- .slide: class="slide" -->  

```Javascript
fetch('https://catfact.ninja/fact')
        .then(resp => resp.json())
        .then(json => console.log(json));
```