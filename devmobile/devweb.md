# Développment Web : fondamentaux
<!-- .slide: class="slide" --> 

----

## C'est quoi ?
<!-- .slide: class="slide" -->  

* Web : protocole HTTP(s)
* Dev web : applications pour le navigateur

----

### Le trio gagnant  
<!-- .slide: class="slide" -->  

* HTML : définition de l'interface
* Javascript : code dynamique  
* CSS : design  

----

### HTML : prérequis  
<!-- .slide: class="slide" -->  

Avant de commencer, info / rappel sur `XML / JSON` :  

<iframe width="560" height="315" src="https://www.youtube.com/embed/utz49fxDBiM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

----

### HTML  
<!-- .slide: class="slide" -->   

* Implémentation de `XML` pour définir des interfaces  
* Les balises correspondent à des composants  
* Principalement à destination des navigateurs  

```HTML
<html>
<body>
<div>Hello world</div>
</body>
</html>
```  

Le navigateur affichera :  
<div>Hello world</div>  

----

### HTML : exemples
<!-- .slide: class="slide" -->   

```HTML
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg" />
```  
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg" />

```HTML
<input type="text" placeholder="Votre nom ici" />
```

<input type="text" placeholder="Votre nom ici" />

```HTML
<Button>Clique ici</Button>
```

<Button>Clique ici</Button>  

----

### HTML : vidéo
<!-- .slide: class="slide" -->  

<iframe width="560" height="315" src="https://www.youtube.com/embed/ce92DnVSPjM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

----

### Javascript : du dynamisme
<!-- .slide: class="slide" -->  

* HTML = définition de l'interface  
* Javascript = action !  

```Javascript
console.log('Hello world');
var score = 2;
alert(score * 2);
```  

----

### Javascript : inclure dans la page
<!-- .slide: class="slide" -->  

```HTML
<script>
   console.log(':)');
</script>
```  

ou (préféré)

```HTML
<head>
<script src="monfichier.js"></script>
</head>
```  

----

### Javascript : exemples
<!-- .slide: class="slide" -->  
```Javascript
console.log('Hello world');
```
```Javascript
const afficherMessage = (message) => console.log(message);
afficherMessage('Hello world');
```

Utilisation HTML :
```HTML
<button onClick="alert('cool !')">Popup</button>
<button onClick="afficherMessage('cool !')">Message</button>
```
<button onClick="alert('cool !')">Popup</button>
<button onClick="afficherMessage('cool !')">Message</button>

----

### Javascript : sélécteurs
<!-- .slide: class="slide" --> 
```HTML
<div id="nombreAleatoire">0</div>
```

```Javascript
// https://xkcd.com/221/
document.getElementById('nombreAleatoire').innerHTML = 4;
```  

En action :  

```Javascript
const tirerAuSort = () => document.getElementById('nombreAleatoire').innerHTML = Math.random();
```
```HTML
<div id="nombreAleatoire">0</div>
<button onClick="tirerAuSort()">Nouveau tirage</button>
```
<div id="nombreAleatoire">0</div>
<button onClick="tirerAuSort()">Nouveau tirage</button>

----

### Javascript : vidéo
<!-- .slide: class="slide" -->  

<iframe width="560" height="315" src="https://www.youtube.com/embed/20CbuxrRxWM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


----

### CSS : du style !
<!-- .slide: class="slide" -->  
Utilisation directe en HTML
```HTML
<div style="color:red">Caméléon</div>
```  

<div style="color:red">Caméléon</div>

Ou (préféré), css à part :
```HTML
<div id="zonespeciale">Hello World!</p>
```
```CSS
<style>
#zonespeciale {
  text-align: center;
  color: red;
}
</style>
```
<div id="zonespeciale">Hello World!</p>
<style>
#zonespeciale {
  text-align: center;
  color: red;
}
</style>  

----

### CSS : vidéo
<!-- .slide: class="slide" -->  

<iframe width="560" height="315" src="https://www.youtube.com/embed/LAW26kbazlE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>