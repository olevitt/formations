# TP 4 : Hello React Native

<!-- .slide: class="slide" -->

Ce TP consiste à prendre en main React Native pour réaliser et déployer une petite application utilisant quelques fonctionnalités spécifiques du mobile (Vibration, partage)    

----

## 1. Le point de départ

<!-- .slide: class="slide" -->

Pour ce TP, on se propose de repartir de ce projet : https://github.com/olevitt/demo-react-native-web qui intègre le support de `react-native-web`.  

Comme à chaque récupération de projet utilisant NPM, il convient de télécharger les dépendances définies dans le `package.json` :  
```
npm install
```

Pour info, ce projet est globalement équivalent à `npx react-native init ReactNativeWebApp` plus l'ajout de `react-native-web`. Une explication approfondie est disponible ici : https://medium.com/@aureliomerenda/create-a-native-web-app-with-react-native-web-419acac86b82

----

## 2. Hello

<!-- .slide: class="slide" -->

Commençons par lancer l'application.  
Pour lancer le serveur métro :  

```
npm run start
```  

Pour lancer l'application sur Android :  

```
npm run android
```  

Pour lancer sur le navigateur :  

```
npm run web
```

Modifier une partie du code, constater :)

----

## 3. Spécificités mobile

<!-- .slide: class="slide" -->

On se propose d'ajouter 2 fonctionnalités classiques et spécifiques au mobile : les vibrations et le partage de contenu (partager via whatsapp / sms / mail ...).  

A l'aide de la documentation React native sur le vibreur : https://reactnative.dev/docs/vibration , mettre en place un bouton qui, lors d'un click, fait vibrer le téléphone.  

```
import {Button, Vibration} from 'react-native';
```
```
<Button title="Vibrate once" onPress={() => Vibration.vibrate()} />
```  

Note : pour Android, il est nécessaire de rajouter une demande de permission au `manifest.xml` : https://developer.android.com/guide/topics/manifest/uses-permission-element  

`android/app/src/main/AndroidManifest.xml`  

Faire de même pour implémenter un bouton de partage : https://reactnative.dev/docs/share