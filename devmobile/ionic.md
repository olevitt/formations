## Multiplateforme : Ionic
<!-- .slide: class="slide" -->  

https://ionicframework.com/

----

### Principe
<!-- .slide: class="slide" -->
* Applications cross-plateformes (Web, PWA, Android, iOS, Electron)
* Technologies web (React, Angular ...)  
* Web first

----

### Composants
<!-- .slide: class="slide" -->
* Ionic CLI : outil ligne de commande pour le développement Ionic
* Ionic Framework : composants UI  
* [Capacitor](https://capacitor.ionicframework.com) : bridge avec les API des plateformes (remplace `cordova`)

----

### Hello world
<!-- .slide: class="slide" -->

https://ionicframework.com/getting-started  

```shell
npm install -g @ionic/cli
```

```shell
ionic start --type react ionic-app tabs
```

```shell
cd ionic-app
ionic serve
```

----

### Anatomie du projet
<!-- .slide: class="slide" -->

* `package.json`  
* Surcouche à `create-react-app`  
* `.ts`, `.tsx` ?
* Composants `ionic`  
* React router

----

#### Web : run & build  
<!-- .slide: class="slide" -->

```shell
ionic serve
ionic build
```

équivalent de  

```shell
npm run start
npm run build
```  

Différence avec `create-react-app` :   
`package.json` (`create-react-app`)
```JSON
"homepage":"."
```  

`index.html` (`ionic`)
```HTML
<base href="." />
```
