# TP 4 : Hello Ionic

<!-- .slide: class="slide" -->

Ce TP consiste à prendre en main Ionic.  
On ne s'intéresse dans ce TP qu'à la plateforme Web. La partie mobile sera vue dans le TP suivant.

----

## 1. Le point de départ

<!-- .slide: class="slide" -->

Pour ce TP, on va créer une application Ionic :

```bash
npm install -g @ionic/cli
ionic start --type react ionic-app tabs
cd ionic-app
ionic serve
```

----

## 2. Build

<!-- .slide: class="slide" -->

Afin de confirmer le support web d'Ionic, on va builder et déployer l'application sur un serveur.  

```shell
ionic build
```  

* Déployer l'application sur le serveur `FTP`

----

## 3. Développements

<!-- .slide: class="slide" -->

`Ionic` combine les concepts vus précédemment.  
Les principaux changements portent sur l'utilisation des composants graphiques `Ionic`.  

* Adapter le code du TP précédent pour Ionic  
* En utilisant les outils de développement du navigateur (ctrl + shift + M sur chrome), visualiser le rendu dans différentes tailles d'écran