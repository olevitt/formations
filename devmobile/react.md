# React
<!-- .slide: class="slide" -->  

https://reactjs.org/

----

## C'est quoi ?
<!-- .slide: class="slide" -->  

* Bibliothèque Javascript pour construire des interfaces utilisateur  
* Opensource, made in Facebook

----

## Alternatives
<!-- .slide: class="slide" -->  

* [Vue.js](https://vuejs.org/), [Angular](https://angular.io), [Svelte](https://svelte.dev/)  
* Le (difficile) choix du framework front : https://2019.stateofjs.com/front-end-frameworks/  

----

## Préalable : le DOM

<!-- .slide: class="slide" -->  

* [Document Object Model](https://www.w3.org/TR/WD-DOM/introduction.html).  
* L'arbre vivant du HTML du navigateur  
* Manipuler le DOM en Javascript :  

```Javascript
document.getElementById('nom'); // Parcourt le DOM à la recherche de l'élément nom  
document.getElementById('nom').innerHTML = ':)';
```

----

## React : principes  

<!-- .slide: class="slide" --> 

* On confie toute la gestion du DOM à React
* On confie toute la gestion du DOM à React
* On confie toute la gestion du DOM à React
* On confie toute la gestion du DOM à React
* On confie toute la gestion du DOM à React
* On confie toute la gestion du DOM à React
* On confie toute la gestion du DOM à React
* On confie toute la gestion du DOM à React

----

## React : fonctionnement  

<!-- .slide: class="slide" -->  

Une zone du DOM réservée à React :  
```HTML
<div id="root"></div>
```  

React y dessine (render) les composants :  
```Javascript
ReactDOM.render(
  <ComposantPrincipal />,
  document.getElementById('root')
);
```

----

## React : utilisation

<!-- .slide: class="slide" -->  

On créé des composants :  

```Javascript
const MonComposant = () => {
    return <div>Hello world</div>
}
```  

Ces composants sont des fonctions qui renvoient du `JSX`.  

----

## JSX

<!-- .slide: class="slide" -->  

* `JSX` = Javascript eXtension  
* En gros : du HTML avec 2 différences  

Possibilité d'inclure des composants :
```HTML
<div>
    <MonSousComposant />
    <AutreComposant />
</div>
```  

Possibilité d'exécuter du Javascript :  
```HTML
<div>
    {pseudo}
</div>
```

----

## Props  

<!-- .slide: class="slide" -->  

* Propriétés (lecture seule) passées de père en fils

```Javascript
const Meteo = (props) => {
    return <div>Il fait {props.temperature} degrés</div>
}
```

```HTML
<div>
    <Meteo temperature="20" />
</div>
```  

----

## State

<!-- .slide: class="slide" -->  

* Etat interne du composant

```Javascript
const Meteo = (props) => {
    const [unite, setUnite] = React.useState("celsius")
    return <div>
    Il fait {props.temperature} degrés {unite}
    <button onClick={() => setUnite('fahrenheit')}>Changer unité</button>
    </div>
}
```

```HTML
<div>
    <Meteo temperature="20" />
</div>
```