# Un projet react
<!-- .slide: class="slide" -->  

https://github.com/facebook/create-react-app  

----

## Create react app
<!-- .slide: class="slide" -->  

* Utilitaire permettant de créér un projet React préconfiguré  
* Utilisation via `NPM` (`npx`)  
* "Opinionated" (nombreux choix faits)

----

## Let's go
<!-- .slide: class="slide" -->  

```
npx create-react-app monapp
```

* Structure du projet  
* `package.json`  
* React préconfiguré  

----

## CRA : utilisation

<!-- .slide: class="slide" -->  

```
npm run start
npm run test
npm run build
```

