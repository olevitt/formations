## Développement mobile : spécificités
<!-- .slide: class="slide" -->  

----

### Des appareils suréquipés
<!-- .slide: class="slide" -->
* Téléphonie (SMS, MMS, appels)
* Internet (GPRS, EDGE, 3G, 4G, 5G, WiFi)
* Réseaux locaux (Bluetooth, WiFi direct, tethering)
* Capteurs (Luminosité, proximité, gyroscope, NFC)
* Localisation (GPS, triangulation, SSID WiFi)
* Notifications (Vibreur, haut-parleurs, LED)
* Photo / vidéo / visio / 3D
* Stockage de données (Mémoire flash, SD externe, SQLite)
* Interactions (Ecran tactile, gestures, boutons physique, reconnaissance vocale, lecteur d’empreintes)  

Et bien d’autres ...  
Et des API pour utiliser tout ¸ca !

----

### Des contraintes techniques importantes
<!-- .slide: class="slide" -->
* Processeur
* Mémoire RAM
* Stockage de données
* Gestion de la batterie
* Présence, stabilité, quotas et débit de la connexion internet
* Cycle de vie de l’application (appels entrants, veille . . . )
* Taille / orientation d'ecran
* Inputs atypiques (clavier virtuel, gestures, peu de boutons . . . )

Contraintes à garder en tête en permanence.

----

### La fragmentation
<!-- .slide: class="slide" -->
* Une application publiée sur le google playstore cible plus de 9000
appareils différents !
* “Write once, run everywhere” ?
* Comment tester / débugger pour tous ces appareils ?
* Eviter de géner l’utilisateur (versions HD, appareils non
compatibles)
* S’adapter quand une fonctionnalité n’est pas disponible

----

### La fragmentation : taille d'écran
<!-- .slide: class="slide" -->

* Comment gérer toutes les tailles d’´ecran ?
* Montres connectées : de 1 à 2 pouces
* Smartphones lowcost : 3 pouces (Galaxy pocket, Galaxy Y, Wiko)
* Smartphones high-end : 4 à 5 pouces (iPhone 5, HTC 8X,
Nexus 5)
* Phablets : 5 à 6 pouces (Galaxy note, Oneplus 5, iPhone 9)
* Tablettes : 7 pouces (Nexus 7, iPad mini), 8 pouces, 10 pouces (Nexus 10, iPad)

----

### De nombreuses autres sources de fragmentation
<!-- .slide: class="slide" -->

* Versions de l’OS
* Résolutions d'écran
* Eléments hardware présents
* Puissance
* Modifications constructeur / “rom custom”

----

### Un monde qui évolue très vite
<!-- .slide: class="slide" -->

* Evolution technologique permanente (nouveaux appareils,
nouvelles versions des OS)
* Evolution du marché (parts de marché des OS)
* Nouveaux OS
* Concurrence sur les stores
* Développement des réseaux et nouveaux usages (4G, 5G)

----

### Des écosystèmes forts
<!-- .slide: class="slide" -->

* Obligation d’utiliser le SDK fourni
* Suivre les guidelines
* Restrictions liées à la plateforme
* Utilisation des services de la plateforme
* Processus de déploiement des applications
* Règles des “store” (validation, monétisation . . . )

Alternatives cross-platform ?

----

## Présentation des OS
<!-- .slide: class="slide" -->

----

### iOS
<!-- .slide: class="slide" -->

* Soutenu par Apple
* Présenté le 9 janvier 2007 : [https://www.youtube.com/watch?v=VKpaK670U7s](https://www.youtube.com/watch?v=VKpaK670U7s)
* Réservé aux produits Apple (iPhone, iPad, iPod, iWatch)
* Programmation en objective-C (+ swift), sur mac OS X uniquement
* Appstore : validation + 100$ / an


----

### Android
<!-- .slide: class="slide" -->

* Soutenu par Google
* Version 1.0 en septembre 2008, commerciale 1.5 en avril 2009 ([https://www.youtube.com/watch?v=1FJHYqE0RDg](https://www.youtube.com/watch?v=1FJHYqE0RDg))
* Plus de 9000 appareils officiellement supportés, plus de 50
constructeurs
* Programmation en Java (+Kotlin), sur Windows / OS X /
Linux
* Noyau open-source (Apache 2.0)
* Google playstore : “pas de validation” + 25$

----

### Les autres
<!-- .slide: class="slide" -->

----

### Les autres
<!-- .slide: class="slide" -->
* Tizen (Samsung)
* HarmonyOS (Huawei)
* Windows 10 (Microsoft) : RIP
* Firefox OS (Mozilla) : RIP
* Blackberry 10 (RIM) : RIP
* Ubuntu touch (Canonical) : RIP
