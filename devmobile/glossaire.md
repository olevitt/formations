# Glossaire

Ce glossaire a pour objectif de centraliser les notions traitées dans ce cours.  
C'est un bon moyen de savoir quoi chercher pour de l'autoformation.

### XML (eXtensible Markup Language)

Format de données structuré par des balises. XML défini simplement la syntaxe à respecter par les balises mais ne défini pas les balises utilisables ni leur signification.

https://www.youtube.com/watch?v=utz49fxDBiM

### HTML (HyperText Markup Language)

Implémentation de XML pour définir des interfaces graphiques.

https://youtu.be/ce92DnVSPjM

### Javascript

Langage de programmation utilisé par les navigateurs. Sur le web, c'est la partie dynamique de l'interface.

https://youtu.be/20CbuxrRxWM

### CSS (Cascading Style Sheets)

Format permettant de définir des feuilles de style qui s'appliqueront aux éléments HTML de nos pages.

https://youtu.be/LAW26kbazlE

### Node.js

Node.js est un moteur Javascript utilisable hors du navigateur. Il va nous permettre d'utiliser des outils de développement comme `NPM`.

https://youtu.be/8PyMQTP5uf0

### NPM (Node Package Manager)

Gestionnaire de paquet de `Node.js`, `NPM` nous permet de définir les dépendances (bibliothèques) utilisées par notre projet ainsi que la façon de le packager. Il s'appuye sur le fichier `package.json`.  
C'est l'équivalent de `maven` pour le monde `Java`

https://youtu.be/yNhK9MLSdqI

### Yarn (optionnel)

Alternative (surcouche) à `NPM`. Vous pouvez utiliser `yarn` à la place de `NPM` si vous le souhaitez.

### React

Bibliothèque javascript de composants. React nous permet de structurer la partie graphique de notre application sous forme de composants. Chaque composant produisant in-fine du HTML en fonction de ses paramètres.

### JSON (JavaScript Object Notation)

Format de données alternatif à XML. Plus léger et moins verbeux que XML, le JSON est le format le plus populaire pour les échanges avec les webservices (API). Il est géré nativement dans Javascript (tout objet Javascript est un JSON potentiel et inversement).

https://www.youtube.com/watch?v=utz49fxDBiM

### React native

Un framework réutilisant les concepts React afin de développer des applications natives pour mobile : Android et iOS.

https://reactnative.dev/