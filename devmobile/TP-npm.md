## TP : utilisation de NPM

<!-- .slide: class="slide" -->

Ce petit TP a pour but d'illustrer l'utilisation de NPM, le gestionnaire de package de Node.  
Pour cela, on va voir la faciliter de prise en main d'un projet développé par d'autres personnes.

----

### 1. Le plan

<!-- .slide: class="slide" -->

Le ministère de l'intérieur a publié, dans le cadre de la lutte contre la pandémie de COVID-19, le code source du générateur d'attestation de déplacement dérogatoire.  
Il s'agit du code source du site https://media.interieur.gouv.fr/attestation-deplacement-derogatoire-covid-19/  
Ce code est disponible sur le github du lab du ministère de l'intérieur : https://github.com/LAB-MI/attestation-deplacement-derogatoire-covid-19

- Prendre un peu de temps pour parcourir le dépôt
- Prendre connaissance de la licence associée à ce code
- Constater la présence d'un fichier `package.json`

----

### 2. Prise en main de l'application

<!-- .slide: class="slide" -->

Cette application semble utiliser `NPM` (présence de `package.json`) et ce de façon standard (présence de scripts `start` et `build` dans le `package.json`). Il devrait donc être aisé de faire tourner cette application.

- Cloner le dépôt
- A l'aide de vos connaissances sur `NPM` et / ou des instructions présentes dans le `README.md`, lancer l'application sur votre machine
- A l'aide de la log du terminal, déterminer l'adresse de notre application. L'ouvrir dans un navigateur.
- Modifier un élément de la page pour expliciter le fait qu'il s'agit de votre version personnelle
- Recharger la page et admirer votre modification

----

### 3. Publication

<!-- .slide: class="slide" -->

On va maintenant publier cette application.

- Quelle commande permet de construire le livrable de l'application ?
- L'exécuter et déposer le livrable sur votre espace personnel FTP.
- Accéder à votre site. Pourquoi ces problèmes d'affichage ?
- En utilisant les outils de développement du navigateur, débugger le problème.
- Corriger (indice : remplacer $PUBLIC_URL) et republier l'application.  

----

### Aller plus loin  

<!-- .slide: class="slide" -->

* Les slides de cette formation utilisent elle-même des technologies web et le projet utilise aussi `NPM`. A partir du code source disponible ici : https://bitbucket.org/olevitt/formations/src/master/ , faire tourner sur votre machine les slides de ces formations  

