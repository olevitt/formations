## SDK Android

<!-- .slide: class="slide" -->  

Pack tout en un : https://developer.android.com/studio  
Uniquement les outils du SDK (sans IDE) : https://developer.android.com/studio/releases/platform-tools

---

### Késako ?

<!-- .slide: class="slide" -->

SDK = Software Development Kit

![](img/sdk.png)

Ensemble d'outils

---

<!-- .slide: class="slide" -->

### IDE

<!-- .slide: class="slide" -->

- Android studio
- Développement en Java (ou en Kotlin)
- Tools => AVD Manager / SDK Manager

---

<!-- .slide: class="slide" -->

### Communication avec l'appareil

<!-- .slide: class="slide" -->

- Emulateur (AVD Manager) ou [débug USB](https://www.youtube.com/watch?v=UbSejX7mppo)
- Utilitaire `adb` (Android debugging bridge).
- `adb devices` / `adb install toto.apk` / `adb shell` / `adb reboot bootloader` ...

![](img/bureau-adb.png)
