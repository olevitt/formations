## TP : Hello react

<!-- .slide: class="slide" -->

Ce petit TP a pour but de commencer, dans un environnement minimal, à prendre en main React.

---

### 1. L'environnement

<!-- .slide: class="slide" -->

Pour ce TP, nous allons utiliser un environnement de développement en ligne afin de se concentrer sur l'utilisation de React. La mise en place de React dans un vrai projet sera abordée dans le chapitre suivant.  
On se propose d'utiliser Codepen (mais vous êtes libres d'utiliser l'une des multiples alternatives).  
L'environnement est accessible sur https://reactjs.org/redirect-to-codepen/hello-world

- Prendre le temps de regarder le code des 3 parties : `HTML` / `CSS` / `JS`
- Dans les paramètres (settings => JS), vérifier la présence des dépendances `react` et `react-dom`
- Prendre le temps de comprendre les intéractions `HTML` / `JS`

Sidequest : suivre ce tutoriel pour créer une page web utilisant React à partir de zéro : https://reactjs.org/docs/add-react-to-a-website.html  
Lors de la prochaine séance, on verra comment industrialiser (gestion des dépendances, gestion du build / packaging) cette installation.

---

### 2. Mon premier composant

<!-- .slide: class="slide" -->

Créér votre premier composant React :

```Javascript
const MonComposant = () => {
    return <div>Hello world</div>
}
```

- Afficher ce composant à l'écran

---

### 3. Séquence souvenirs

<!-- .slide: class="slide" -->

Le 8 août 2013, la productivité mondiale s'est effondrée suite à la sortie du jeu `Cookie clicker` (https://en.wikipedia.org/wiki/Cookie_Clicker).
Ce jeu, très simple, est considéré comme l'un des fondateurs du genre des `idle games`, jeux consistant à accumuler des ressources, soit en attendant, soit en clickant frénétiquement sur un bouton.  
On se propose ici d'en recréer une version simpliste.

- Afficher un score, initialisé à `100`.
- Afficher un bouton `click`.
- Lors d'un click sur le bouton, ajouter `1` au score.

---

### Aller plus loin

<!-- .slide: class="slide" -->

- Ajouter des boutons permettant d'acheter des améliorations augmentant le rendement des clicks. Ces améliorations coûtent des points.
- En utilisant le hook `useEffect` et la fonction javascript `useTimeout`, ajouter un timer augmentant régulièrement le score.
