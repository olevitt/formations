# TP 5 : Ionic mobile

<!-- .slide: class="slide" -->

Ce TP consiste à déployer l'application du TP précédent sur Android

----

## 1. Le point de départ

<!-- .slide: class="slide" -->

On repart du code du TP précédent.  

----

## 2. Ajout d'Android

<!-- .slide: class="slide" -->

* Ajouter le support d'Android à l'application  
* Ouvrir Android studio et s'assurer que les composants sont à jour
* Importer le projet  
* Lancer le projet sur un émulateur ou un appareil Android connécté en `USB`

----

## 3. Le kiff  
<!-- .slide: class="slide" -->

Il est temps de profiter de la puissance d'Ionic et de ses plugins  
* Ajouter la géolocalisation à partir de cette documentation : https://ionicframework.com/docs/native/geolocation  
* Tester dans le navigateur  
* Tester sur un appareil Android ou un émulateur  
* Transformer le code pour utiliser les hooks reacts : https://github.com/ionic-team/ionic-react-hooks

----

## 4. Le kiff
<!-- .slide: class="slide" -->

Vous avez maintenant la possibilité d'ajouter les plugins de votre choix à partir de la liste disponible sur https://ionicframework.com/docs/native.  
Par exemple :  
* Vibration  
* Camera (via hooks)  
* OCR (Android / iOS uniquement)  
* Text to speech  

...