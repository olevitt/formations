# Utilisation du serveur (FTP)

Pendant la durée de ce cours (et après, sur demande), je vous met à disposition un espace personnel sur mon serveur. Il vous servira à déployer votre application parce que c'est quand même beaucoup plus sympa de partager les créations :)  
Le contenu que vous y déposerez sera disponible publiquement sur https://demo.levitt.fr/votreidentifiant  
Pour modifier le contenu (uploader / supprimer des fichiers), vous devrez utiliser `FTP` (File Transfer Protocol). Ce document documente l'installation et l'utilisation simple d'un client `FTP`.  
Vous aurez besoin des identifiants (credentials) qui vous ont été transmis par mail. Si vous n'avez pas reçu vos identifiants, venez les demander sur `teams`.

## Disclaimer

Avant de commencer, un disclaimer est nécessaire :

- **Cet espace vous est mis à disposition dans le cadre du cours et il ne doit être utilisé que dans ce cadre. Ce n'est pas un espace de partage ou de stockage de données.**
- **Vous êtes responsables du contenu que vous déposez sur cet espace. N'y déposez que du contenu dont vous détenez les droits et n'y mettez pas de données personnelles**

En utilisant le service, vous acceptez les points ci-dessus.

## Un poil de théorie

`FTP` est un protocole `client-serveur`.  
Un serveur `FTP` a été installé sur la machine hébergeant `levitt.fr`. Il s'agit d'un logiciel qui écoute en permanence sur le port `21` et attend qu'un client vienne se connecter.  
Après authentification du client (`login / password`), le client peut uploader, modifier et supprimer les fichiers des répertoires auxquels il a accès en utilisant les commandes du protocole `FTP` (si vous êtes curieux : https://www.cs.colostate.edu/helpdocs/ftp.html et https://tools.ietf.org/html/rfc959).

## Logiciel FTP

N'importe quel client `FTP` devrait faire l'affaire.  
Le plus connu est probablement Filezilla et c'est celui qui sera illustré dans ce guide.  
Filezilla est téléchargeable ici : https://filezilla-project.org/ (attention à bien prendre le client, pas le serveur).

## Configuration du serveur

Une fois Filezilla lancé, aller dans le gestionnaire de sites (fichier => gestionnaire de sites ou ctrl + s ou le bouton en haut à gauche).  
Créez un nouveau site et entrez les informations suivantes :

- Hôte : `levitt.fr`
- Port : `21` (ou laisser blanc)
- Protocol : `FTP - File Transfer Protocol`
- Chiffrement : `Connexion FTP explicite sur TLS si disponible` (ou, `Connexion FTP simple (non sécurisé)`)
- Type d'authentification : `normal`
- Identifiant : `votre identifiant` (envoyé par mail. Si vous ne l'avez pas reçu, venez demander sur teams).
- Mot de passe : `le mot de passe qui vous a été généré` (envoyé par mail. Si vous ne l'avez pas reçu, venez demander sur teams).

![Filezilla](img/filezilla.png)

Laissez les autres options par défaut.  
Vous pouvez maintenant vous connecter. Vous n'avez accès qu'à un seul dossier (mais vous pouvez créer des sous-dossiers).  
Le contenu de ce dossier est exposé **publiquement** sur https://demo.levitt.fr/votreidentifiant
