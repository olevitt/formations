## Multiplateforme : Framework7
<!-- .slide: class="slide" -->  

https://framework7.io/  

Alternatives : [React native](https://reactnative.dev/), [Ionic](https://ionicframework.com/) ... 

----

### Principe
<!-- .slide: class="slide" -->
* Applications cross-plateformes (Web, PWA, Android, iOS, Electron)
* Technologies web (HTML / JS / CSS + React)  

----

### Hello world
<!-- .slide: class="slide" -->

https://framework7.io/#get-started

(React native CLI Quickstart)

----

### Anatomie du projet
<!-- .slide: class="slide" -->

* `package.json`  
* Equivalent de `create-react-app`  
* Dossiers `public` et `src`  

----

### Et maintenant ?     
<!-- .slide: class="slide" -->

Le kiff : https://framework7.io/react/ 

`React components`  