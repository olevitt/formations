## Ionic mobile
<!-- .slide: class="slide" -->  

----

### Principe
<!-- .slide: class="slide" -->
* Build web  
* Projet surcouche Android  
* Webview  

----

### Let's go (Android)
<!-- .slide: class="slide" -->

```shell
ionic capacitor add android  
```  

----

### Import sous Android studio  
<!-- .slide: class="slide" -->

* Lancer Android Studio  
* Ouvrir le dossier `android` du projet  
* Attendre (dépendances `gradle`)  

----

### Lancer sur Android ?
<!-- .slide: class="slide" -->

* Appareil vs Emulateur 
* ADB 

----

### Ionic : au quotidien
<!-- .slide: class="slide" -->

```shell
ionic capacitor copy
```  

= `ionic build` + copie du build dans le projet `Android`  

----

### Ionic : plugins
<!-- .slide: class="slide" -->

* Ionic Native, des tonnes de plugins : https://ionicframework.com/docs/native  
* Exemple : [Vibration](https://ionicframework.com/docs/native/vibration)  
* `Supported Platforms`  

----

### Special react : React-hooks
<!-- .slide: class="slide" -->

* https://github.com/ionic-team/ionic-react-hooks