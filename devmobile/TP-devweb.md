## TP : développement web
<!-- .slide: class="slide" -->  
Ce TP a pour but de prendre en main les bases des 3 composants principaux du web : HTML, Javascript, CSS.  
Rappel : les outils de dev c'est la vie. [Chrome](https://developer.chrome.com/docs/devtools/overview/) et [Firefox](https://developer.mozilla.org/en-US/docs/Tools)  
Vous êtes libres d'utiliser vos accès FTP [(guide d'utilisation)](https://bitbucket.org/olevitt/formations/src/master/devmobile/ftp.md) pour publier vos créations :)  
Attention spoiler : [exemple de code solution](https://bitbucket.org/olevitt/formations/src/master/devmobile/TP/tp1/)

----

### 1. Hello HTML
<!-- .slide: class="slide" -->  
* Créer un premier fichier `index.html`  
* Y mettre le code minimal suivant :  

```HTML
<html>
<body>
  <div>Hello world</div>
</body>
</html>
```  
* Ouvrir le fichier dans un navigateur  
* Se balader dans les outils de dev pour voir le dessous des cartes

Sidequest : déployer le fichier sur le serveur FTP et aller sur https://votreidentifiant.levitt.fr

----

### 2. Une première interface  
<!-- .slide: class="slide" -->  
On veut écrire une application de conversion de devises  
* Modifier le fichier HTML pour réaliser une interface semblable à celle-ci :  
![](img/devises.png)  

(Vous êtes libres de modifier cette interface à votre convenance)

----

### 3. Let's move !
<!-- .slide: class="slide" -->  
On va maintenant mettre en place l'action de notre convertisseur.  
```text
Faire en sorte qu'un click sur le bouton convertisse le montant
(avec la logique 1€ = 1.1$) 
et affiche le résultat (à la place du 20$)  
```  
Principe, 3 parties :  
* La logique (fonction de conversion multipliant par 1.1)  
* L'intéraction graphique (récupérer le montant à convertir, modifier le résultat)  
* Le déclencheur (lors d'un click sur le bouton)

----

### 3-2. Guide pas à pas
<!-- .slide: class="slide" -->  

Pas à pas :  
* Créer un fichier `devises.js` et l'inclure dans votre page `html`  
* Dans ce fichier `devises.js`, ecrire une fonction de conversion x => x * 1.1 (logique du code). 
* Ecrire une fonction renvoyant la valeur contenue dans le champ montant à convertir (intéraction graphique)  
* Ecrire une fonction modifiant le contenu du champ résultat (intéraction graphique)  
* Ecrire une fonction utilisant les 3 fonctions précédentes (controlleur)
* Lors d'un click sur le bouton, déclencher la méthode précédente (déclencheur)

----

### 4. Concours de style
<!-- .slide: class="slide" -->  
En utilisant du CSS (ne pas appliquer directement le style dans le HTML), faire une interface plus classe.  
Une fois votre convertisseur fonctionnel et stylisé, uploadez le sur le serveur et partagez le lien sur le canal `Concours de style (TP1)` de teams :)