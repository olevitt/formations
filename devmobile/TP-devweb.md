## TP : développement web
<!-- .slide: class="slide" -->  
Ce TP a pour but de prendre en main les bases des 3 composants principaux du web : HTML, Javascript, CSS.  
Cet outil en ligne peut être pratique pour tester rapidement ou pour partager un exemple : https://jsfiddle.net/  
Vous êtes libres d'utiliser vos accès FTP [(guide d'utilisation)](https://bitbucket.org/olevitt/formations/src/master/devmobile/ftp.md) pour publier vos créations :)

----

### 1. Hello HTML
<!-- .slide: class="slide" -->  
* Créer un premier fichier `index.html`  
* Y mettre le code minimal suivant :  

```HTML
<html>
<body>
  <div>Hello world</div>
</body>
</html>
```  
* Ouvrir le fichier dans un navigateur  

Sidequest : déployer le fichier sur le serveur FTP et aller sur https://demo.levitt.fr/votreidentifiant/index.html  

----

### 2. Une première interface  
<!-- .slide: class="slide" -->  
On veut écrire une application de conversion de devises  
* Modifier le fichier HTML pour réaliser une interface semblable à celle-ci :  
![](img/devises.png)  

(Vous êtes libres de modifier cette interface à votre convenance)

----

### 3. Let's move !
<!-- .slide: class="slide" -->  
On va maintenant mettre en place la logique du convertisseur.  
```text
Faire en sorte qu'un click sur le bouton convertisse le montant
(avec la logique 1€ = 1.1$) 
et affiche le résultat (à la place du 20$)  
```  
Pas à pas :
* Créer un fichier `devises.js` et l'inclure dans votre page `html`  
* Dans ce fichier `devises.js`, afficher "Hello" dans la log. Testez.  
* Ecrire une fonction qui modifie le texte du champ "20$"  
* Lors d'un click sur le bouton, afficher "Hello" dans la log.  
* Lors d'un click sur le bouton, modifier le texte du champ "20$"  
* Ecrire une fonction qui multiplie un nombre par 1.1
* Lors d'un click sur le bouton, afficher le contenu du champ montant  
* Mettre tout bout à bout :)  

----

### 4. Concours de style
<!-- .slide: class="slide" -->  
En utilisant du CSS (ne pas appliquer directement le style dans le HTML), faire une interface plus classe.  
Une fois votre convertisseur fonctionnel et stylisé, uploadez le sur le serveur et partagez le lien sur le canal `Concours de style (TP1)` de teams :)