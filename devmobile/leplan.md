## Objectif

<!-- .slide: class="slide" -->

Développer une application mobile

---

## Le choix

<!-- .slide: class="slide" -->

Pour développer sur mobile :

- Utilisation du SDK natif de chaque plateforme
- Alternatives multi-plateformes : web

---

## SDK natif

<!-- .slide: class="slide" -->

Pour Android : Java (ou Kotlin)  
Pour iOS : Objective-C (ou Swift)

Cours d'Android en utilisant le SDK natif : https://bitbucket.org/olevitt/technologies-mobiles/src/master/

---

## Multi-plateforme

<!-- .slide: class="slide" -->

Utilisation des technologies web (navigateur)  
Avantages :

- Technologies plus standards (web, multi-plateforme)
- Développement facilité (test dans le navigateur)
- "Write once, run anywhere"

Inconvénients :

- Des limitations techniques
- Utilisation de frameworks non officiels

---

## Plan du cours

<!-- .slide: class="slide" -->

1. Fondamentaux du développment web (HTML, Javascript, CSS)
2. Le web moderne (NodeJS, NPM, React)
3. Spécificités du mobile (Etat de l'art, possibilités, contraintes)
4. Application mobile avec [Framework7](https://framework7.io/)
