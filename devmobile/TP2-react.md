# TP 3 : Suivi du coronavirus

<!-- .slide: class="slide" -->

Ce TP consiste à écrire une application de suivi de la propagation du Coronavirus.  
Les notions suivantes rentreront en jeu :

- Afficher une liste d'informations
- Manipuler des données JSON
- Consommer une API (https://covid19api.com/)
- Partager les informations entre les composants  

Un exemple de réalisation de ce TP est disponible ici : https://demo.levitt.fr/levitt/tp3/  
Le code source correspondant est disponible ici : https://bitbucket.org/olevitt/formations/src/master/devmobile/TP/Corodata/ (dans l'intérêt de l'exercice il vous est conseillé de ne pas le regarder avant la fin pour ne pas vous divulgâcher)

----

## 1. Le point de départ

<!-- .slide: class="slide" -->

Pour ce TP, vous pouvez au choix reprendre un projet existant ou créer un nouveau projet `create-react-app`

```bash
npx create-react-app tp-coronavirus
```

----

## 2. Un premier composant

<!-- .slide: class="slide" -->

Créer un composant `Liste` dans un fichier `Liste.js`.  
Pour l'instant, ce composant affichera une liste HTML simple :

```HTML
<ol>
    <li>Element A</li>
    <li>Element B</li>
    <li>Element C</li>
</ol>
```

Exporter ce composant et l'importer dans `App.js`. Tester.

----

## 3. Des données

<!-- .slide: class="slide" -->

Pour l'instant, nous allons simuler les données de notre application. La récupération des données depuis le serveur (API) interviendra dans la prochaine partie.

- Créer un fichier `fakedata.json`
- Y copier les données de la sortie de l'API : https://api.covid19api.com/summary
- Ce fichier est importable tel quel en utilisant le code suivant :

```Javascript
import data from "./data.json";
```

- Afficher dans la console les données

----

## 4. Afficher les données

<!-- .slide: class="slide" -->

On va combiner les 2 parties précédentes afin d'afficher dans la liste les données du fichier `JSON`.  

* Rajouter une props `data` à notre liste  
* Transmettre, depuis `App.js`, les données au composant `Liste`  
* Itérer, dans `Liste`, sur les données afin de les afficher (nom du pays, nombre de cas confirmés par exemple)

----

## 5. Des vraies données

<!-- .slide: class="slide" -->

* En utilisant `fetch` (ou un client alternatif), faire une requête `HTTP` vers l'API (https://api.covid19api.com/summary)  
* Afficher le résultat dans la console  
* Transmettre ces données fraiches au composant `Liste`

----

## 6. Filter les données

<!-- .slide: class="slide" -->

On veut maintenant pouvoir filter les données  
* Ajouter une props `filter` au composant `Liste`  
* Filter la liste au moment de l'affichage pour ne garder que les pays contenant le filtre  
* Ajouter, sur la page principale, un champ de texte modifiable (filtre)  
* Transmettre à la `Liste` le contenu du champ de texte à chaque fois qu'il change

----

## Aller plus loin

<!-- .slide: class="slide" -->

- Modifier le style de l'application en utilisant [`material-ui`](https://material-ui.com/)  
- Utiliser [React-leaflet](https://react-leaflet.js.org/) pour afficher les données sous forme de carte du monde
- Mettre en place un musée de nos composants en utilisant [Storybook](https://storybook.js.org/docs/guides/guide-react/)
