# Développement Web moderne
<!-- .slide: class="slide" --> 

----

## NodeJS
<!-- .slide: class="slide" -->  

* Javascript en dehors du navigateur  
* Du Javascript artisanal au Javascript industriel

<iframe width="560" height="315" src="https://www.youtube.com/embed/8PyMQTP5uf0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

----

## NPM
<!-- .slide: class="slide" -->  

* Node Package Manager  
* Gestionnaire de projet, structure, dépendances, build ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/yNhK9MLSdqI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>