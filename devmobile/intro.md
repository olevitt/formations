<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

# Technologies mobile

<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->

[Olivier Levitt](https://bitbucket.org/olevitt) (olivier.levitt@gmail.com)

Online : [https://formations.levitt.fr/devmobile](http://formations.levitt.fr/devmobile)  
Source : [https://bitbucket.org/olevitt/formations](https://bitbucket.org/olevitt/formations)

[Prérequis à installer](https://bitbucket.org/olevitt/formations/src/master/devmobile/setup.md)

[Glossaire (avec liens vers les vidéos)](https://bitbucket.org/olevitt/formations/src/master/devmobile/glossaire.md)

[Utilisation du serveur FTP](https://bitbucket.org/olevitt/formations/src/master/devmobile/ftp.md)

Ecrit en [markdown][1] et propulsé par [reveal.js](http://lab.hakim.se/reveal-js/)

[1]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
