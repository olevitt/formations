# Pré-requis

Liste des outils qui seront utilisés pendant ce cours.  
Il est demandé de les avoir pré-installé et testé (des exemples de tests sont proposés dans les blocs de code) avant le début du cours.  
En cas de question / problèmes, n'hésitez pas à venir demander de l'aide sur teams.

## Système d'exploitation

Le système d'exploitation n'est pas imposé, le développement Web / Android est possible sur les 3 OS majeurs :

- Linux
- Windows
- Mac OS

Les versions doivent être "relativement" récentes.

## GIT

Git doit être installé sur la machine. 
Pour Windows, on pourra utiliser `https://gitforwindows.org/`.

```
git clone https://bitbucket.org/olevitt/formations.git
```

## IDE

L'IDE n'est pas imposé, vous êtes libres de votre choix d'environnement.  
Il est cependant conseillé d'utiliser VSCode https://code.visualstudio.com/ .
Les plugins conseillés sont :

- ESLint
- npm Intellisense
- Prettier - Code formatter

Et les plugins secondaires :

- GitLens

```
L'IDE se lance bien.
```

## Navigateur

Notre application va utiliser principalement des technologies du web.  
Il sera donc pratique de la tester sur un navigateur.  
Tous les navigateurs modernes (chrome, firefox, edge) feront l'affaire.  
Dans le doute, chrome est conseillé.

Lors de ces tests, l'extension react developer tools sera bien pratique :

- Pour chrome (et edge ?) : https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en
- Pour firefox : https://addons.mozilla.org/en-US/firefox/addon/react-devtools/

## Node 

Node va nous permettre d'exécuter du code Javascript en dehors du navigateur.  
On en aura besoin lors du développement pour tout ce qui touche à la compilation, au build et au packaging de notre application.  
Il vient avec `NPM` qui sera notre gestionnaire de dépendances.

- NodeJS : https://nodejs.org/en/download/ , prendre la version LTS

```
node -v
```

depuis le terminal doit renvoyer une version supérieure ou égale à 16.

```
npm -v
```

depuis le terminal doit renvoyer une version supérieure ou égale à 8.

Validation complète de l'installation (faire tourner un projet démo) :

```
git clone https://github.com/olevitt/demo-framework-7.git
cd demo-framework-7
npm install
npm run start
```

Si tout va bien, vous devriez avoir une "jolie" application qui s'ouvre dans le navigateur (les étapes peuvent être longues, en particulier sur une connexion faible. Soyez patients :)).  
En cas de problèmes, n'hésitez pas à venir demander de l'aide sur teams.

## Transferts FTP

Il sera intéressant de publier notre application.  
Pour cela, je vous ai créé des comptes sur mon serveur personnel.  
Un mail vous sera envoyé avec vos identifiants pour vous y connecter.  
Vous aurez besoin d'un client `FTP` (File Transfer Protocol).  
Le plus connu est probablement `Filezilla` : https://filezilla-project.org/ (prendre `Filezilla Client`) mais il existe des tonnes d'alternatives. N'importe quel client `FTP` fera l'affaire.

## Android

L'utilisation des outils spécifiques Android arrivera lors de la troisième session de cours.  
Il n'est donc pas obligatoire de les avoir pour la première séance.  
Il s'agit cependant de l'installation la plus lourde en terme de téléchargement (plus de `1Go`), il est donc important de bien l'anticiper.

Il s'agit de télécharger et d'installer Android studio :  

https://developer.android.com/studio
