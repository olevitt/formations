# TP : React

<!-- .slide: class="slide" -->

Ce TP a pour but d'écrire notre première application complète en React

----

## 1. Le point de départ

<!-- .slide: class="slide" -->

Pour ce TP, on va partir d'une application Create React App  

```bash
npx create-react-app tp-react
```  

* Reprendre votre code du TP précédent et l'inclure dans cette application.

----

## 2. Du style

<!-- .slide: class="slide" -->

`NPM` et `React` nous permettent de facilement récupérer les composants créés par d'autres personnes.  
On se propose d'utiliser, dans cette application, la bibliothèque graphique `material-ui` (https://material-ui.com/).  

* A partir de la documentation sur le site, ajouter `material-ui` au projet pour donner du style  
* Parcourir les composants disponibles et intégrer ceux qui vous plaisent particulièrement

----

## 3. Publication (sidequest)

<!-- .slide: class="slide" -->

Même si notre application est loin d'être complète, on peut déjà la publier.  

* Constater que le fichier `.gitignore` a déjà été précréé.  
* Versionner le code sur GIT.
* Builder l'application et la déployer sur le serveur FTP

----

## 4. Tests

<!-- .slide: class="slide" -->

* En s'inspirant du test créé par défaut par `create react app`, créér un test pour chacun de nos composants.  

----

## Aller plus loin

<!-- .slide: class="slide" -->

* Mettre en place un musée de nos composants en utilisant [Storybook](https://storybook.js.org/docs/guides/guide-react/)  
